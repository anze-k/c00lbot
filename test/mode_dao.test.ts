import { Mode, getAllModes, getModeById, getModeByName, insertMode } from '../src/db/dao/mode'
import * as _ from 'lodash'

test('insert and get modes', async () => {
    const newMode: Mode = { name: 'ctf', team_size: 5 }

    await insertMode(newMode)

    const modes = await getAllModes()
    const modeById = await getModeById('1')
    const modeByName = await getModeByName('ctf')

    expect(modes[0]).toMatchObject({ ...newMode, id: 1 })
    expect(modeById).toMatchObject({ ...newMode, id: 1 })
    expect(modeByName).toMatchObject({ ...newMode, id: 1 })
})
