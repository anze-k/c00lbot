import { getLastPlayedMaps, getPlayerLastPlayedMaps, UtMap } from '../src/db/dao/map'
import { calculateOdds, pickMap } from '../src/pug/pick_map'


describe('pickMap', () => {
    it('should return a map from the input array', async () => {
        const maps: UtMap[] = [
            { id: 'map1', current_weight: 10, default_weight: 10, name: 'map1' },
            { id: 'map2', current_weight: 20, default_weight: 20, name: 'map2' },
        ]
        const players = ['player1']

        const result = await pickMap(maps, players)

        expect(maps).toContainEqual(result)
    })

    it('should be more likely to pick maps with higher default weights', async () => {
        const maps: UtMap[] = [
            { id: '1', name: 'Map 1', default_weight: 5, current_weight: 5 },
            { id: '2', name: 'Map 2', default_weight: 10, current_weight: 10 },
            { id: '3', name: 'Map 3', default_weight: 20, current_weight: 20 },
        ]

        const players = ['player1', 'player2']

        const mapCounts: { [key: string]: number } = {
            'Map 1': 0,
            'Map 2': 0,
            'Map 3': 0
        }
        for (let i = 0; i < 1000; i++) {
            const map = await pickMap(maps, players)
            mapCounts[map!.name]++
        }

        expect(mapCounts['Map 3']).toBeGreaterThan(mapCounts['Map 2'])
        expect(mapCounts['Map 2']).toBeGreaterThan(mapCounts['Map 1'])
    })

    // it('should increase the current weight of maps that have not been played recently', async () => {
    //     const maps: UtMap[] = [
    //         { id: 'map1', current_weight: 10, default_weight: 10, name: 'map1' },
    //         { id: 'map2', current_weight: 20, default_weight: 20, name: 'map2' },
    //     ]
    //     const players = ['player1']
    //     jest.spyOn(getPlayerLastPlayedMaps, 'getPlayerLastPlayedMaps').mockResolvedValue([])
    //     const result = await pickMap(maps, players)
    //     expect(result!.current_weight).toBeGreaterThan(maps[0].current_weight)
    // })

    // it('should reduce the current weight of maps that have been played recently', async () => {
    //     const maps: UtMap[] = [
    //         { id: 'map1', current_weight: 10, default_weight: 10, name: 'map1' },
    //         { id: 'map2', current_weight: 20, default_weight: 20, name: 'map2' },
    //     ]
    //     const players = ['player1']
    //     jest.spyOn(getPlayerLastPlayedMaps, 'getPlayerLastPlayedMaps').mockResolvedValue([{ id: 'map1', current_weight: 10, default_weight: 10, name: 'map1' }])

    //     const result = await pickMap(maps, players)
    //     expect(result!.current_weight).toBeLessThan(result!.default_weight)
    // })

    // it('should reduce the chance of maps that a player has recently played getting picked', async () => {
    //     const maps: UtMap[] = [
    //         { id: 'map1', current_weight: 10, default_weight: 10, name: 'map1' },
    //         { id: 'map2', current_weight: 20, default_weight: 20, name: 'map2' },
    //     ]
    //     const players = ['player1']
    //     jest.spyOn(getPlayerLastPlayedMaps, 'getPlayerLastPlayedMaps').mockResolvedValue([{ id: 'map1', current_weight: 10, default_weight: 10, name: 'map1' }])
    //     let map1Count = 0
    //     let map2Count = 0
    //     for (let i = 0; i < 1000; i++) {
    //         const result = await pickMap(maps, players)
    //         if (result!.name === 'map1') map1Count++
    //         if (result!.name === 'map2') map2Count++
    //     }
    //     expect(map1Count).toBeLessThan(map2Count)
    // })

})

describe('calculateOdds', () => {
    it('should return a map with the odds for each map in the input array', async () => {
        const maps: UtMap[] = [
            { id: 'map1', current_weight: 10, default_weight: 10, name: 'map1' },
            { id: 'map2', current_weight: 20, default_weight: 20, name: 'map2' },
        ]

        const result = await calculateOdds(maps)

        expect(result).toEqual(new Map([['map2', 66.66666666666666], ['map1', 33.33333333333333]]))
    })
})
