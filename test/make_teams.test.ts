import { fetchEligiblePlayers } from '../src/db/dao/map_stats'
import { Player } from '../src/db/dao/player'
import { makeTeams } from '../src/pug/make_teams'
import config from 'config'

const TEAM_SIZE = 3

jest.mock('config', () => ({ get: jest.fn() }))
jest.mock('../src/db/dao/map_stats', () => ({
    fetchEligiblePlayers: jest.fn()
}))

describe('makeTeams', () => {

    beforeEach(() => {
        (config.get as jest.MockedFunction<typeof config.get>).mockReturnValue(true);
        (fetchEligiblePlayers as jest.Mock).mockClear();
    })

    test('should create two teams with equal ratings when possible', async () => {
        const players: Player[] = [
            { discord_id: '123', nickname: 'Anže', rating: 500 },
            { discord_id: '456', nickname: 'Anžej', rating: 500 },
            { discord_id: '789', nickname: 'Anžek', rating: 500 },
            { discord_id: '321', nickname: 'Anžel', rating: 500 },
            { discord_id: '654', nickname: 'Anžem', rating: 500 },
            { discord_id: '987', nickname: 'Anžen', rating: 500 },
        ];

        (fetchEligiblePlayers as jest.Mock).mockResolvedValue([])
        const result = await makeTeams(players, 'EternalDragonBridge', TEAM_SIZE)
        expect(result.red.rating + result.blue.rating).toBe(3000)
        expect(result.red.rating).toEqual(result.blue.rating)
        expect(result.red.players.length).toEqual(TEAM_SIZE)
        expect(result.blue.players.length).toEqual(TEAM_SIZE)
    })

    test('should potentially create different teams after multiple calls with the same input', async () => {
        const players: Player[] = [
            { discord_id: '258', nickname: 'Sirène', rating: 479 },
            { discord_id: '679', nickname: 'Sirena', rating: 998 },
            { discord_id: '921', nickname: 'Serrure', rating: 756 },
            { discord_id: '362', nickname: 'Mermaid', rating: 688 },
            { discord_id: '514', nickname: 'Lockpick', rating: 227 },
            { discord_id: '986', nickname: 'Verrou', rating: 100 },
        ];

        const teamsSet = new Set()
        for (let i = 0; i < 10; i++) {
            const result = await makeTeams(players, 'FaceTripleBridge', TEAM_SIZE)
            teamsSet.add(JSON.stringify(result))
        }
        expect(teamsSet.size).toBeGreaterThan(1)
    })

    test('should select valid combinations of players based on rating threshold and experts', async () => {
        const players: Player[] = [
            { discord_id: '123', nickname: 'Lockprick', rating: 800 },
            { discord_id: '456', nickname: 'Lockprickster', rating: 700 },
            { discord_id: '789', nickname: 'Lockprickly', rating: 500 },
            { discord_id: '321', nickname: 'Lockprickled', rating: 300 },
            { discord_id: '654', nickname: 'Lockprickish', rating: 100 },
            { discord_id: '987', nickname: 'Lockick', rating: 200 },
        ];

        (fetchEligiblePlayers as jest.Mock).mockResolvedValue([{ player_id: '321' }, { player_id: '987' }])
        const result = await makeTeams(players, 'CoretTivoli', TEAM_SIZE)

        const allExpertPlayerIds = result.red.players.concat(result.blue.players).map(p => p.discord_id)
        expect(allExpertPlayerIds).toContain('321')
        expect(allExpertPlayerIds).toContain('987')

        const ratingDifference = Math.abs(result.red.rating - result.blue.rating)
        expect(ratingDifference).toBeLessThanOrEqual(50)
    })

    test('should prioritize map experts and distribute them across teams', async () => {
        const players: Player[] = [
            { discord_id: '1', nickname: 'Franco Pressure', rating: 1000 },
            { discord_id: '2', nickname: 'Melony Trombone', rating: 950 },
            { discord_id: '3', nickname: 'Sammy Foguar', rating: 400 },
            { discord_id: '4', nickname: 'Lucky Dunkčić', rating: 960 },
            { discord_id: '5', nickname: 'Teeny Mazerunner', rating: 100 },
            { discord_id: '6', nickname: 'Slavy Whizcheck', rating: 800 },
        ];

        (fetchEligiblePlayers as jest.Mock).mockResolvedValue([{ player_id: '1' }, { player_id: '2' }])

        const iterations = 100
        let expertsOnSameTeam = false

        for (let i = 0; i < iterations; i++) {
            const result = await makeTeams(players, 'EternalDragonBridge', TEAM_SIZE)

            const redTeamExperts = result.red.players.filter(p => ['1', '2'].includes(p.discord_id)).length
            const blueTeamExperts = result.blue.players.filter(p => ['1', '2'].includes(p.discord_id)).length

            if (redTeamExperts === 2 || blueTeamExperts === 2) {
                expertsOnSameTeam = true;
                break
            }
        }
        expect(expertsOnSameTeam).toBe(false)
    })

    test('should prioritize team balance over map expertise when rating difference would be too large', async () => {
        const players: Player[] = [
            { discord_id: '1', nickname: 'Joey Plexinick', rating: 1500 },
            { discord_id: '2', nickname: 'Bo Paymore', rating: 1500 },
            { discord_id: '3', nickname: 'Hermy Potluck', rating: 10000 },
            { discord_id: '4', nickname: 'Mikey Seerare', rating: 9 },
            { discord_id: '5', nickname: 'Anzy Copytar', rating: 8 },
            { discord_id: '6', nickname: 'Alex Doubleyolk', rating: 7 },
        ];

        (fetchEligiblePlayers as jest.Mock).mockResolvedValue([{ player_id: '1' }, { player_id: '2' }])

        const result = await makeTeams(players, 'NivenMetelkova', TEAM_SIZE)

        const redRating = result.red.players.reduce((acc, player) => acc + player.rating, 0)
        const blueRating = result.blue.players.reduce((acc, player) => acc + player.rating, 0)

        expect(Math.abs(redRating - blueRating)).toBe(7006)

        const redExpertIds = result.red.players.filter(p => ['1', '2'].includes(p.discord_id)).length
        const blueExpertIds = result.blue.players.filter(p => ['1', '2'].includes(p.discord_id)).length

        expect(redExpertIds === 2 || blueExpertIds === 2).toBe(true)
    })

    test('should not have duplicated players across teams', async () => {
        const players: Player[] = [
            { discord_id: '312', nickname: 'SandyStorm', rating: 1350 },
            { discord_id: '489', nickname: 'Saradune', rating: 1100 },
            { discord_id: '521', nickname: 'Sandalous', rating: 1450 },
            { discord_id: '765', nickname: 'Santricity', rating: 900 },
            { discord_id: '234', nickname: 'CassandraNova', rating: 780 },
            { discord_id: '672', nickname: 'Sanshiro', rating: 620 },
        ];

        const result = await makeTeams(players, 'FacingLjubljana', TEAM_SIZE)
        const allPlayers = result.red.players.concat(result.blue.players)
        const uniquePlayers = new Set(allPlayers.map(p => p.discord_id))
        expect(allPlayers.length).toBe(uniquePlayers.size)
    })

    test('should ensure all input players are in one of the teams', async () => {
        const players: Player[] = [
            { discord_id: '158', nickname: 'Sanknight', rating: 1200 },
            { discord_id: '234', nickname: 'SansaraRift', rating: 800 },
            { discord_id: '894', nickname: 'AlessandraPrime', rating: 990 },
            { discord_id: '345', nickname: 'Santara', rating: 1020 },
            { discord_id: '431', nickname: 'Santhology', rating: 660 },
            { discord_id: '876', nickname: 'CelestrialSandra', rating: 750 },
        ];

        const result = await makeTeams(players, 'DeckBelgrade', TEAM_SIZE)
        const allPlayers = result.red.players.concat(result.blue.players).map(p => p.discord_id)
        players.forEach(player => {
            expect(allPlayers).toContain(player.discord_id)
        })
    })

    test('should always divide even number of players equally between teams', async () => {
        const skewedPlayers: Player[] = [
            { discord_id: '321', nickname: 'ShadyWader', rating: 100 },
            { discord_id: '276', nickname: 'lockdrop', rating: 150 },
            { discord_id: '341', nickname: 'ninTomatoes', rating: 200 },
            { discord_id: '482', nickname: 'candyCorney', rating: 2270 },
            { discord_id: '838', nickname: 'binThomas', rating: 2100 },
            { discord_id: '921', nickname: 'popcorny', rating: 2150 },
        ];

        const result = await makeTeams(skewedPlayers, 'SignoSnore', TEAM_SIZE)
        expect(result.red.players.length).toBe(result.blue.players.length)
        expect(result.red.players.length).toEqual(TEAM_SIZE)
        expect(result.blue.players.length).toEqual(TEAM_SIZE)
    })

    test('should handle scenarios where all players are experts', async () => {
        const players: Player[] = [
            { discord_id: '1', nickname: 'BurntToast', rating: 1000 },
            { discord_id: '2', nickname: 'HyperPadawan', rating: 1100 },
            { discord_id: '3', nickname: 'Chyburger', rating: 1200 },
            { discord_id: '4', nickname: 'BigShotBradley', rating: 1000 },
            { discord_id: '5', nickname: 'SirSandwich', rating: 900 },
            { discord_id: '6', nickname: 'JustJoshinYa', rating: 800 },
        ];

        (fetchEligiblePlayers as jest.Mock).mockResolvedValue(players.map(player => ({ player_id: player.discord_id })))

        const result = await makeTeams(players, 'GlacialGone', TEAM_SIZE)

        expect(result.red.rating).toEqual(result.blue.rating)
        expect(result.red.players.length).toEqual(TEAM_SIZE)
        expect(result.blue.players.length).toEqual(TEAM_SIZE)
    })

    test('should handle scenarios where no players are experts', async () => {
        const players: Player[] = [
            { discord_id: '1', nickname: 'ChillChief', rating: 1000 },
            { discord_id: '2', nickname: 'MellowMastah', rating: 1100 },
            { discord_id: '3', nickname: 'PickyPica', rating: 1200 },
            { discord_id: '4', nickname: 'sirCake', rating: 1000 },
            { discord_id: '5', nickname: 'LiteBrite', rating: 900 },
            { discord_id: '6', nickname: 'SockTick', rating: 800 },
        ];

        (fetchEligiblePlayers as jest.Mock).mockResolvedValue([])

        const result = await makeTeams(players, 'OrbitBall', TEAM_SIZE)

        expect(result.red.rating).toEqual(result.blue.rating)
        expect(result.red.players.length).toEqual(TEAM_SIZE)
        expect(result.blue.players.length).toEqual(TEAM_SIZE)
    })

    test('should handle scenarios where multiple players have the same rating', async () => {
        const players: Player[] = [
            { discord_id: '1', nickname: 'Wader', rating: 1000 },
            { discord_id: '2', nickname: 'TinThomas', rating: 1000 },
            { discord_id: '3', nickname: 'CornFlakey', rating: 1000 },
            { discord_id: '4', nickname: 'Devarew', rating: 800 },
            { discord_id: '5', nickname: 'Redefine', rating: 800 },
            { discord_id: '6', nickname: 'Zapstick', rating: 800 },
        ];

        (fetchEligiblePlayers as jest.Mock).mockResolvedValue([])

        const result = await makeTeams(players, 'FaultyCity', TEAM_SIZE)

        const ratingDifference = Math.abs(result.red.rating - result.blue.rating)
        expect(ratingDifference).toBe(200)
        expect(result.red.players.length).toEqual(TEAM_SIZE)
        expect(result.blue.players.length).toEqual(TEAM_SIZE)
    })

    it('should balance teams only by rating when enableMapExperts is false', async () => {
        config.get = jest.fn().mockReturnValue(false)

        const players: Player[] = [
            { discord_id: '1', nickname: 'Wader', rating: 1000 },
            { discord_id: '2', nickname: 'TinThomas', rating: 1000 },
            { discord_id: '3', nickname: 'CornFlakey', rating: 1000 },
            { discord_id: '4', nickname: 'Devarew', rating: 800 },
            { discord_id: '5', nickname: 'Redefine', rating: 800 },
            { discord_id: '6', nickname: 'Zapstick', rating: 800 },
        ];

        const result = await makeTeams(players, 'testMap', TEAM_SIZE)

        const ratingDifference = Math.abs(result.red.rating - result.blue.rating)
        expect(ratingDifference).toBeLessThanOrEqual(200)

        const redTeamExperts = result.red.players.filter(p => ['1', '2'].includes(p.discord_id)).length
        const blueTeamExperts = result.blue.players.filter(p => ['1', '2'].includes(p.discord_id)).length
        expect(redTeamExperts + blueTeamExperts).toBe(2)
    })

    it('should balance teams only by rating when enableMapExperts is false, even across multiple iterations', async () => {
        const players: Player[] = [
            { discord_id: '1', nickname: 'Wader', rating: 1000 },
            { discord_id: '2', nickname: 'TinThomas', rating: 1000 },
            { discord_id: '3', nickname: 'CornFlakey', rating: 1000 },
            { discord_id: '4', nickname: 'Devarew', rating: 1000 },
            { discord_id: '5', nickname: 'Redefine', rating: 800 },
            { discord_id: '6', nickname: 'Zapstick', rating: 800 },
        ];

        (fetchEligiblePlayers as jest.Mock).mockResolvedValue([{ player_id: '1' }, { player_id: '2' }])
        config.get = jest.fn().mockReturnValue(false)
        let expertsWereTogether = 0

        for (let i = 0; i < 100; i++) {
            const result = await makeTeams(players, 'testMap', 3)

            const ratingDifference = Math.abs(result.red.rating - result.blue.rating)
            expect(ratingDifference).toBeLessThanOrEqual(200)

            const redTeamHasBothExperts = result.red.players.filter(p => ['1', '2'].includes(p.discord_id)).length === 2
            const blueTeamHasBothExperts = result.blue.players.filter(p => ['1', '2'].includes(p.discord_id)).length === 2

            if (redTeamHasBothExperts || blueTeamHasBothExperts) {
                expertsWereTogether++
            }
        }

        expect(expertsWereTogether).toBeGreaterThan(1)
    })

    it('should separate map experts when enableMapExperts is true, even across multiple iterations', async () => {
        const players: Player[] = [
            { discord_id: '1', nickname: 'Wader', rating: 1000 },
            { discord_id: '2', nickname: 'TinThomas', rating: 1000 },
            { discord_id: '3', nickname: 'CornFlakey', rating: 1000 },
            { discord_id: '4', nickname: 'Devarew', rating: 1000 },
            { discord_id: '5', nickname: 'Redefine', rating: 800 },
            { discord_id: '6', nickname: 'Zapstick', rating: 800 },
        ];

        (fetchEligiblePlayers as jest.Mock).mockResolvedValue([{ player_id: '1' }, { player_id: '2' }])
        config.get = jest.fn().mockReturnValue(true)
        let expertsWereTogether = 0

        for (let i = 0; i < 100; i++) {
            const result = await makeTeams(players, 'testMap', 3)

            const ratingDifference = Math.abs(result.red.rating - result.blue.rating)
            expect(ratingDifference).toBeLessThanOrEqual(200)

            const redTeamHasBothExperts = result.red.players.filter(p => ['1', '2'].includes(p.discord_id)).length === 2
            const blueTeamHasBothExperts = result.blue.players.filter(p => ['1', '2'].includes(p.discord_id)).length === 2

            expect(redTeamHasBothExperts && blueTeamHasBothExperts).toBeFalsy()

            if (redTeamHasBothExperts || blueTeamHasBothExperts) {
                expertsWereTogether++
            }
        }

        expect(expertsWereTogether).toBe(0)
    })
})
