import { calculateElo } from '../src/pug/elo_calculation'

describe('calculateElo', () => {
    it('should calculate the ELO rating change for red and blue teams', async () => {
        const redRating = 1500
        const blueRating = 1600
        const scoreRed = 10
        const scoreBlue = 5

        const ratingChange = await calculateElo(redRating, blueRating, scoreRed, scoreBlue)
        expect(ratingChange).toEqual({
            teamRed: {
                delta: 8,
                newRating: 1508,
            },
            teamBlue: {
                delta: -8,
                newRating: 1592,
            },
        })
    })

    it('should return 1 delta for the lower rated team and -1 delta for the higher rated team if the scores are equal', async () => {
        const redRating = 1500
        const blueRating = 1600
        const scoreRed = 10
        const scoreBlue = 10

        const ratingChange = await calculateElo(redRating, blueRating, scoreRed, scoreBlue)
        expect(ratingChange).toEqual({
            teamRed: {
                delta: redRating > blueRating ? -1 : 1,
                newRating: redRating > blueRating ? 1499 : 1501,
            },
            teamBlue: {
                delta: blueRating > redRating ? -1 : 1,
                newRating: blueRating > redRating ? 1599 : 1601,
            },
        })
    })

    it('should handle negative ratings', async () => {
        const redRating = -500
        const blueRating = -400
        const scoreRed = 10
        const scoreBlue = 5

        const ratingChange = await calculateElo(redRating, blueRating, scoreRed, scoreBlue)
        expect(ratingChange).toEqual({
            teamRed: {
                delta: 8,
                newRating: -492,
            },
            teamBlue: {
                delta: -8,
                newRating: -408,
            },
        })
    })

    it('should handle a large margin of victory for the blue team and give 0 rating change if the score is not high enough when the rating difference is high', async () => {
        const redRating = 10000
        const blueRating = 20000
        const scoreRed = 50
        const scoreBlue = 100

        const ratingChange = await calculateElo(redRating, blueRating, scoreRed, scoreBlue)
        expect(ratingChange).toEqual({
            teamRed: {
                delta: 0,
                newRating: 10000,
            },
            teamBlue: {
                delta: 0,
                newRating: 20000,
            },
        })
    })

    it('should handle a significantly higher rating for the red team', async () => {
        const redRating = 20000
        const blueRating = 10000
        const scoreRed = 50
        const scoreBlue = 100

        const ratingChange = await calculateElo(redRating, blueRating, scoreRed, scoreBlue)
        expect(ratingChange).toEqual({
            teamRed: {
                delta: -60,
                newRating: 19940,
            },
            teamBlue: {
                delta: 60,
                newRating: 10060,
            },
        })
    })

    it('should handle a rating of 0', async () => {
        const redRating = 0
        const blueRating = 0
        const scoreRed = 100
        const scoreBlue = 50

        const ratingChange = await calculateElo(redRating, blueRating, scoreRed, scoreBlue)
        expect(ratingChange).toEqual({
            teamRed: {
                delta: 30,
                newRating: 30,
            },
            teamBlue: {
                delta: -30,
                newRating: -30,
            }
        })
    })

    it('should handle a tie score and a tie rating', async () => {
        const redRating = 2000
        const blueRating = 2000
        const scoreRed = 5
        const scoreBlue = 5

        const ratingChange = await calculateElo(redRating, blueRating, scoreRed, scoreBlue)
        expect(ratingChange).toEqual({
            teamRed: {
                delta: 0,
                newRating: 2000,
            },
            teamBlue: {
                delta: 0,
                newRating: 2000,
            },
        })
    })

})
