import { getMapExperts } from '../src/pug/get_map_experts'
import { fetchEligiblePlayers } from '../src/db/dao/map_stats'

jest.mock('../src/db/dao/map_stats', () => ({
    fetchEligiblePlayers: jest.fn()
}))

describe('getMapExperts', () => {
    beforeEach(() => {
        jest.clearAllMocks()
    })

    it('returns the correct player IDs when eligible players are found', async () => {
        (fetchEligiblePlayers as jest.Mock).mockResolvedValue([
            { player_id: '1' },
            { player_id: '3' }
        ])

        const players = [
            { discord_id: '1', nickname: 'SirLouisM', rating: 800 },
            { discord_id: '2', nickname: 'Quacktor Waluigi', rating: 700 },
            { discord_id: '3', nickname: 'MagikWizard', rating: 650 },
        ]

        const result = await getMapExperts('AnarchyZone', players)
        expect(result).toEqual(['1', '3'])
    })

    it('returns an empty array when no eligible players are found', async () => {
        (fetchEligiblePlayers as jest.Mock).mockResolvedValue([])

        const players = [
            { discord_id: '4', nickname: 'MightyMighty', rating: 600 },
            { discord_id: '5', nickname: 'ChillinHodor', rating: 550 },
            { discord_id: '6', nickname: 'NotSoSmoothie', rating: 500 },
        ]

        const result = await getMapExperts('VaultedCity', players)
        expect(result).toEqual([])
    })
})
