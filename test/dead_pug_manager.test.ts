import { DeadPugger } from '../src/pug/dead_pug_manager'

jest.mock('config', () => ({
    get: jest.fn(() => 3),
}))

describe('DeadPugger', () => {
    let deadPugger: DeadPugger

    beforeEach(() => {
        deadPugger = new DeadPugger()
    })

    it('should return the correct result for various input scenarios', () => {
        const testCases = [
            { pugId: 'pug1', discordId: 'discord1', team: 'RED', expectedResult: { dead: false } },
            { pugId: 'pug1', discordId: 'discord1', team: 'RED', expectedResult: { dead: false } },
            { pugId: '', discordId: 'discord1', team: 'RED', expectedResult: { dead: false } },
            { pugId: 'pug1', discordId: '', expectedResult: { dead: false } },
            { pugId: 'pug1', discordId: 'discord1', team: 'INVALID_TEAM', expectedResult: { dead: false } },
        ]

        testCases.forEach(({ pugId, discordId, team, expectedResult }) => {
            if (team === 'RED' || team === 'BLUE') {
                const result = deadPugger.deadpug(pugId, discordId, team)
                expect(result).toEqual(expectedResult)
            }
        })

    })

    it('should return dead as false if the user has already voted', () => {
        const result = deadPugger.deadpug('pug1', 'discord1', 'RED')
        expect(result).toEqual({ dead: false })
        const result2 = deadPugger.deadpug('pug1', 'discord1', 'RED')
        expect(result2).toEqual({ dead: false })
    })

    it('should return dead as false if the number of votes for a team is less than the required votes', () => {
        const result = deadPugger.deadpug('pug1', 'discord1', 'RED')
        expect(result).toEqual({ dead: false })
        const result2 = deadPugger.deadpug('pug1', 'discord2', 'RED')
        expect(result2).toEqual({ dead: false })
    })

    it('should return dead as true if the number of votes for both teams is equal to or greater than the required votes', () => {
        const result = deadPugger.deadpug('pug1', 'discord1', 'RED')
        expect(result).toEqual({ dead: false })
        const result2 = deadPugger.deadpug('pug1', 'discord2', 'RED')
        expect(result2).toEqual({ dead: false })
        const result3 = deadPugger.deadpug('pug1', 'discord3', 'RED')
        expect(result3).toEqual({ dead: false })
        const result4 = deadPugger.deadpug('pug1', 'discord4', 'RED')
        expect(result4).toEqual({ dead: false })
        const result5 = deadPugger.deadpug('pug1', 'discord5', 'RED')
        expect(result5).toEqual({ dead: false })
        const result6 = deadPugger.deadpug('pug1', 'discord6', 'BLUE')
        expect(result6).toEqual({ dead: false })
        const result7 = deadPugger.deadpug('pug1', 'discord7', 'BLUE')
        expect(result7).toEqual({ dead: false })
        const result8 = deadPugger.deadpug('pug1', 'discord8', 'BLUE')
        expect(result8).toEqual({ dead: true, voted: 'discord1,discord2,discord3,discord4,discord5,discord6,discord7,discord8' })
    })

    it('should return dead as false if the pugId argument is not in the userVoted map', () => {
        const result = deadPugger.deadpug('pug1', 'discord1', 'RED')
        expect(result).toEqual({ dead: false })
    })

    it('should return dead as true if both teams have cast the required number of votes, but one team has cast more votes', () => {
        const result = deadPugger.deadpug('pug1', 'discord1', 'RED')
        expect(result).toEqual({ dead: false })
        const result2 = deadPugger.deadpug('pug1', 'discord2', 'RED')
        expect(result2).toEqual({ dead: false })
        const result3 = deadPugger.deadpug('pug1', 'discord3', 'RED')
        expect(result3).toEqual({ dead: false })
        const result4 = deadPugger.deadpug('pug1', 'discord4', 'BLUE')
        expect(result4).toEqual({ dead: false })
        const result5 = deadPugger.deadpug('pug1', 'discord5', 'BLUE')
        expect(result5).toEqual({ dead: false })
        const result6 = deadPugger.deadpug('pug1', 'discord6', 'BLUE')
        expect(result6).toEqual({ dead: true, voted: 'discord1,discord2,discord3,discord4,discord5,discord6' })
        const result7 = deadPugger.deadpug('pug1', 'discord7', 'RED')
        expect(result7).toEqual({ dead: false })
    })

    it('should return dead as false if one team has cast the required number of votes, but the other team has not', () => {
        const result = deadPugger.deadpug('pug1', 'discord1', 'RED')
        expect(result).toEqual({ dead: false })
        const result2 = deadPugger.deadpug('pug1', 'discord2', 'RED')
        expect(result2).toEqual({ dead: false })
        const result3 = deadPugger.deadpug('pug1', 'discord3', 'RED')
        expect(result3).toEqual({ dead: false })
        const result4 = deadPugger.deadpug('pug1', 'discord4', 'BLUE')
        expect(result4).toEqual({ dead: false })
        const result5 = deadPugger.deadpug('pug1', 'discord5', 'BLUE')
        expect(result5).toEqual({ dead: false })
    })

})
