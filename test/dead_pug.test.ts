import { processLiveScore } from '../src/pug/server/process_server_scores'
import { finishPug, insertModeAndEmptyPug, startPug } from './setup'

beforeAll(async () => {
  const pugId = <number>await insertModeAndEmptyPug()

  await startPug(pugId)

  const now = new Date()
  await finishPug(pugId, 2, 1, new Date(now.getTime() - 1000 * 60 * 60 * 24 * 2).toISOString()) // finish 1h ago
})

jest.mock('../src/bot/bot', () => {
  return {
    getMsgReactions: () => undefined,
  };
});

test('must be able to detect a deadpug if timeout and no reactions', async () => {
  await processLiveScore()
})

jest.mock('../src/bot/bot', () => {
  return {
    getMsgReactions: () => ({ approve: [], reject: [], deadPug: [1, 2] }),
  };
});

test('must be able to detect a deadpug if timeout and deadpug reactions', async () => {
  await processLiveScore()
})
