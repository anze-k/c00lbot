import { format } from '../../src/bot/embeds/utils/format_rating'

describe('format', () => {
    it('should return the input number as a string without a suffix if the number is less than 1000', () => {
        expect(format(999)).toBe('999')
        expect(format(123)).toBe('123')
    })

    it('should return the input number as a string with a suffix if the number is greater than or equal to 1000', () => {
        expect(format(1000)).toBe('1k')
        expect(format(2000)).toBe('2k')
    })

    it('should return the input number as a string with a suffix rounded to 2 decimal places if the number is greater than or equal to 1000', () => {
        expect(format(1234)).toBe('1.23k')
        expect(format(12345)).toBe('12.35k')
        expect(format(123456)).toBe('123.46k')
    })

    it('should return the input number as a string with the correct suffix based on its magnitude', () => {
        expect(format(999)).toBe('999')
        expect(format(1000)).toBe('1k')
        expect(format(1000000)).toBe('1m')
    })
})
