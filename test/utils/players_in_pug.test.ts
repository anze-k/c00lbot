import * as formatUtils from '../../src/bot/embeds/utils/format_rating'
import * as utils from '../../src/bot/embeds/utils/get_nickname'
import { getPlayersInPug } from '../../src/bot/embeds/utils/players_in_pug'
import { Player } from '../../src/db/dao/player'

jest.mock('../../src/bot/embeds/utils/get_display_name', () => ({
    getnickname: jest.fn()
}))

jest.mock('../../src/bot/embeds/utils/format_rating', () => ({
    format: jest.fn()
}))

describe('getPlayersInPug', () => {
    beforeEach(() => {
        jest.clearAllMocks()
    })

    it('returns "No one joined yet" when given an empty array', async () => {
        const players: Player[] = []
        const result = await getPlayersInPug(players)
        expect(result).toBe('No one joined yet')
    })

    it('returns a list of players sorted by rating', async () => {
        const players: Player[] = [
            { discord_id: '1234567890', nickname: 'player1', rating: 851 },
            { discord_id: '1234567891', nickname: 'player2', rating: 2000 },
            { discord_id: '1234567892', nickname: 'player3', rating: 1500 },
        ];

        (utils.getNickname as jest.Mock)
            .mockResolvedValueOnce('player2')
            .mockResolvedValueOnce('player3')
            .mockResolvedValueOnce('player1');

        (formatUtils.format as jest.Mock)
            .mockReturnValueOnce('2k')
            .mockReturnValueOnce('1.5k')
            .mockReturnValueOnce('851');

        const result = await getPlayersInPug(players)
        expect(result).toBe(
            ':small_orange_diamond: **player2** (2k) :small_orange_diamond: **player3** (1.5k) :small_orange_diamond: **player1** (851)'
        )
    })

    it('handles async display name fetching', async () => {
        const players: Player[] = [
            { discord_id: '1', nickname: 'Alice', rating: 1000 },
            { discord_id: '2', nickname: 'Bob', rating: 900 }
        ];

        (utils.getNickname as jest.Mock)
            .mockResolvedValueOnce('Alice')
            .mockResolvedValueOnce('Bob');
        (formatUtils.format as jest.Mock)
            .mockReturnValueOnce('1k')
            .mockReturnValueOnce('900');

        const result = await getPlayersInPug(players)
        expect(result).toBe(
            ':small_orange_diamond: **Alice** (1k) :small_orange_diamond: **Bob** (900)'
        )
    })
})
