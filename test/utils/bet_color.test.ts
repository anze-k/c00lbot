import { getBetColor } from '../../src/bot/embeds/utils/bet_color'
import { blueDot, orangebox, redDot } from '../../src/bot/embeds/utils/constants'
import { BetOption } from '../../src/pug/bet/bet'

describe('getBetColor', () => {
    it('should return redDot for BetOption.Red', () => {
        expect(getBetColor(BetOption.Red)).toEqual(redDot)
    })

    it('should return blueDot for BetOption.Blue', () => {
        expect(getBetColor(BetOption.Blue)).toEqual(blueDot)
    })

    it('should return orangebox for BetOption.Draw', () => {
        expect(getBetColor(BetOption.Draw)).toEqual(orangebox)
    })
})
