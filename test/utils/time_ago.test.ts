import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import utc from 'dayjs/plugin/utc'

import { timeAgo } from '../../src/bot/embeds/utils/time_ago'

dayjs.extend(relativeTime)
dayjs.extend(utc)

describe('timeAgo', () => {
    test('returns "Invalid date" for invalid input', () => {
        const invalidInput = 'invalid input'
        const result = timeAgo(invalidInput)
        expect(result).toBe('Invalid date')
    })

    test('returns time remaining for future date input', () => {
        const futureDate = dayjs().add(2, 'hour')
        const result = timeAgo(futureDate.toISOString())
        expect(result).toMatch(/in 2 hours/)
    })

    test('returns time remaining for positive number input', () => {
        const futureSeconds = dayjs().unix() + (3600 * 24)
        const result = timeAgo(futureSeconds)
        expect(result).toMatch(/in a day/)
    })

    test('returns time remaining for future string object input', () => {
        const currentDate = new Date()
        const futureDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 5)
        const result = timeAgo(futureDate.toISOString())
        expect(result).toMatch(/in (4|5) days/)
    })

    test('returns time ago for past date input', () => {
        const pastDate = dayjs().subtract(3, 'weeks')
        const result = timeAgo(pastDate.toISOString())
        expect(result).toMatch(/21 days ago/)
    })

    test('returns time ago for negative number input', () => {
        const pastSeconds = -45
        const result = timeAgo(pastSeconds)
        expect(result).toMatch(/a minute ago/)
    })

    test('returns time ago for past string input', () => {
        const currentDate = new Date()
        const pastDate = new Date(currentDate.getFullYear(), currentDate.getMonth() - 2, currentDate.getDate(), currentDate.getHours(), currentDate.getMinutes())
        const result = timeAgo(pastDate.toISOString())
        expect(result).toMatch(/2 months ago/)
    })

    test('returns time ago for date string with missing leading zeros', () => {
        const futureDate = dayjs().add(5, 'days')
        const futureDateString = futureDate.format('ddd MMM D YYYY HH:mm:ss')
        const result = timeAgo(futureDateString)
        expect(result).toMatch(/in (4|5) days/)
    })

    test('returns time ago for date string in different timezone', () => {
        const futureDate = dayjs().add(4, 'hours')
        const futureDateString = futureDate.utc().format('ddd MMM DD YYYY HH:mm:ss') + ' GMT+0100 (Central European Time)'
        const result = timeAgo(futureDateString)
        expect(result).toMatch(/in (3|4) hours/)
    })

    test('returns time remaining for current date and time', () => {
        const currentDate = new Date()
        const result = timeAgo(currentDate)
        expect(result).toMatch(/a few seconds ago/)
    })

    test('returns time ago for past date more than a year old', () => {
        const pastDate = dayjs().subtract(2, 'years')
        const result = timeAgo(pastDate.toDate())
        expect(result).toMatch(/2 years ago/)
    })
})
