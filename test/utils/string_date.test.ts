import { getStringDate } from '../../src/bot/embeds/utils/string_date'

describe('getStringDate', () => {
    it('should return the current date in Swedish when passed "today"', () => {
        const today = new Date()
        const expectedDateString = today.toLocaleDateString('sv-SE')

        expect(getStringDate('today')).toEqual(expectedDateString)
    })

    it('should return yesterday\'s date in Swedish when passed "yesterday"', () => {
        const yesterday = new Date()
        yesterday.setDate(yesterday.getDate() - 1)
        const expectedDateString = yesterday.toLocaleDateString('sv-SE')

        expect(getStringDate('yesterday')).toEqual(expectedDateString)
    })

    it('should return the date three days ago in Swedish when passed "three"', () => {
        const threeDaysAgo = new Date()
        threeDaysAgo.setDate(threeDaysAgo.getDate() - 3)
        const expectedDateString = threeDaysAgo.toLocaleDateString('sv-SE')

        expect(getStringDate('three')).toEqual(expectedDateString)
    })

    it('should return the start of the current week in Swedish when passed "week"', () => {
        const currentWeekStart = new Date()
        const dayOfWeek = currentWeekStart.getDay()
        currentWeekStart.setDate(currentWeekStart.getDate() - dayOfWeek + (dayOfWeek === 0 ? -6 : 1))
        const expectedDateString = currentWeekStart.toLocaleDateString('sv-SE')

        expect(getStringDate('week')).toEqual(expectedDateString)
    })

    it('should return the start of the current month in Swedish when passed "month"', () => {
        const currentMonthStart = new Date()
        currentMonthStart.setMonth(currentMonthStart.getMonth())
        currentMonthStart.setDate(1)
        const expectedDateString = currentMonthStart.toLocaleDateString('sv-SE')

        expect(getStringDate('month')).toEqual(expectedDateString)
    })

    it('should return the start of the current year in Swedish when passed "year"', () => {
        const currentYearStart = new Date()
        currentYearStart.setFullYear(currentYearStart.getFullYear())
        currentYearStart.setMonth(0)
        currentYearStart.setDate(1)
        const expectedDateString = currentYearStart.toLocaleDateString('sv-SE')

        expect(getStringDate('year')).toEqual(expectedDateString)
    })

    it('should throw an error when passed an invalid time parameter', () => {
        expect(() => getStringDate('invalid')).toThrowError('Invalid time parameter')
    })
})
