import config from 'config'

import { getAccumulatedBanDuration } from '../../src/bot/embeds/utils/ban_valuation'
import { getLastBanForUserWithReason } from '../../src/db/dao/ban'

jest.mock('../../src/db/dao/ban')
jest.mock('config')

describe('getAccumulatedBanDuration', () => {
    const INITIAL_DURATION = 24
    const LAST_BAN_DURATION = 48
    const USER_ID = "1231"

    const mockedGetLastBanForUserWithReason = getLastBanForUserWithReason as jest.MockedFunction<typeof getLastBanForUserWithReason>
    const mockedConfigGet = config.get as jest.Mock

    let defaultConfigValues: {
        'ban.types': Array<{
            name: string
            duration: number
            mode: 'ADDITIVE' | 'MULTIPLICATIVE' | 'STATIC' | 'PROGRESSIVE'
        }>
        'ban.repeatBanTimeframeDays': number
        'ban.progressiveDurations': number[]
        [key: string]: any
    }

    beforeEach(() => {
        jest.resetAllMocks()

        defaultConfigValues = {
            'ban.types': [
                {
                    "name": "Used comic sans in a meme",
                    "duration": 3,
                    "mode": "ADDITIVE"
                },
                {
                    "name": "Used health potion for personal lubrication",
                    "duration": 24,
                    "mode": "STATIC"
                },
                {
                    "name": "Wore socks with sandals",
                    "duration": 24,
                    "mode": "PROGRESSIVE"
                },
                {
                    "name": "Asked NPCs on a date",
                    "duration": 24,
                    "mode": "MULTIPLICATIVE"
                },
            ],
            'ban.repeatBanTimeframeDays': 30,
            'ban.progressiveDurations': [24, 48, 72, 168]
        }

        mockedConfigGet.mockImplementation((key: string) => defaultConfigValues[key])
    })

    describe('ban reasons', () => {
        it('returns initial duration if reason is not stackable', async () => {
            mockedGetLastBanForUserWithReason.mockResolvedValueOnce({
                reason: 'Asked NPCs on a date',
                end_time: new Date().toISOString(),
                duration: LAST_BAN_DURATION,
                discord_id: USER_ID,
                admin_id: "321312",
                admin_name: "c00lb0t2"
            })

            const result = await getAccumulatedBanDuration(USER_ID, 'Asked the mermaids for their OnlyFans link', INITIAL_DURATION)
            expect(result).toBe(INITIAL_DURATION)
        })
    })

    describe('ban stackModes', () => {
        it('returns initial duration for a STATIC ban, even if there is a previous ban', async () => {
            mockedGetLastBanForUserWithReason.mockResolvedValueOnce({
                reason: 'Used health potion for personal lubrication',
                end_time: new Date().toISOString(),
                duration: LAST_BAN_DURATION,
                discord_id: USER_ID,
                admin_id: "321312",
                admin_name: "c00lb0t2"
            })

            const result = await getAccumulatedBanDuration(USER_ID, 'Used health potion for personal lubrication', INITIAL_DURATION)
            expect(result).toBe(INITIAL_DURATION)
        })

        it('add the previous ban duration on the new duration for a repeat offense in ADDITIVE mode', async () => {
            mockedGetLastBanForUserWithReason.mockResolvedValueOnce({
                reason: 'Used comic sans in a meme',
                end_time: new Date().toISOString(),
                duration: LAST_BAN_DURATION,
                discord_id: USER_ID,
                admin_id: "321312",
                admin_name: "c00lb0t2"
            })

            const result = await getAccumulatedBanDuration(USER_ID, 'Used comic sans in a meme', INITIAL_DURATION)
            expect(result).toBe(INITIAL_DURATION + LAST_BAN_DURATION)
        })

        it('doubles the ban duration for a repeat offense in MULTIPLICATIVE mode', async () => {
            mockedGetLastBanForUserWithReason.mockResolvedValueOnce({
                reason: 'Asked NPCs on a date',
                end_time: new Date().toISOString(),
                duration: LAST_BAN_DURATION,
                discord_id: USER_ID,
                admin_id: "321312",
                admin_name: "c00lb0t2"
            })

            const result = await getAccumulatedBanDuration(USER_ID, 'Asked NPCs on a date', INITIAL_DURATION)
            expect(result).toBe(LAST_BAN_DURATION * 2)
        })

        it('uses the progressive durations for a repeat offense in PROGRESSIVE mode', async () => {
            mockedGetLastBanForUserWithReason.mockResolvedValueOnce({
                reason: 'Wore socks with sandals',
                end_time: new Date().toISOString(),
                duration: LAST_BAN_DURATION,
                discord_id: USER_ID,
                admin_id: "321312",
                admin_name: "c00lb0t2"
            })

            const result = await getAccumulatedBanDuration(USER_ID, 'Wore socks with sandals', INITIAL_DURATION)
            expect(result).toBe(72)
        })

        it('uses the first step in progressive durations if the last ban duration is not in the list but matches ban type duration', async () => {
            mockedGetLastBanForUserWithReason.mockResolvedValueOnce({
                reason: 'Wore socks with sandals',
                end_time: new Date().toISOString(),
                duration: 3,
                discord_id: USER_ID,
                admin_id: "321312",
                admin_name: "c00lb0t2"
            })

            const result = await getAccumulatedBanDuration(USER_ID, 'Wore socks with sandals', INITIAL_DURATION)
            expect(result).toBe(24)
        })

        it('remains at the longest duration in progressive durations if the last ban was already at this duration', async () => {
            mockedGetLastBanForUserWithReason.mockResolvedValueOnce({
                reason: 'Wore socks with sandals',
                end_time: new Date().toISOString(),
                duration: 168,
                discord_id: USER_ID,
                admin_id: "321312",
                admin_name: "c00lb0t2"
            })

            const result = await getAccumulatedBanDuration(USER_ID, 'Wore socks with sandals', INITIAL_DURATION)
            expect(result).toBe(168)
        })
    })

    describe('ban history', () => {
        it('returns initial duration if no previous ban exists', async () => {
            mockedGetLastBanForUserWithReason.mockResolvedValueOnce(null as any)

            const result = await getAccumulatedBanDuration(USER_ID, 'Used comic sans in a meme', INITIAL_DURATION)
            expect(result).toBe(INITIAL_DURATION)
        })

        it('returns initial duration if last ban was a long time ago', async () => {
            const oldDate = new Date()
            oldDate.setDate(oldDate.getDate() - 31)

            mockedGetLastBanForUserWithReason.mockResolvedValueOnce({
                reason: 'Used comic sans in a meme',
                end_time: oldDate.toISOString(),
                duration: LAST_BAN_DURATION,
                discord_id: USER_ID,
                admin_id: "321312",
                admin_name: "c00lb0t2"
            })

            const result = await getAccumulatedBanDuration(USER_ID, 'Used comic sans in a meme', INITIAL_DURATION)
            expect(result).toBe(INITIAL_DURATION)
        })

        it('should add ban durations even if close to limit', async () => {
            const oldDate = new Date()
            oldDate.setDate(oldDate.getDate() - 29)

            mockedGetLastBanForUserWithReason.mockResolvedValueOnce({
                reason: 'Used comic sans in a meme',
                end_time: oldDate.toISOString(),
                duration: LAST_BAN_DURATION,
                discord_id: USER_ID,
                admin_id: "321312",
                admin_name: "c00lb0t2"
            })

            const result = await getAccumulatedBanDuration(USER_ID, 'Used comic sans in a meme', INITIAL_DURATION)
            expect(result).toBe(LAST_BAN_DURATION + INITIAL_DURATION)
        })

        it('increases ban duration only if the last ban was for the same reason', async () => {
            mockedGetLastBanForUserWithReason.mockResolvedValueOnce({
                reason: 'Used comic sans in a meme',
                end_time: new Date().toISOString(),
                duration: LAST_BAN_DURATION,
                discord_id: USER_ID,
                admin_id: "321312",
                admin_name: "c00lb0t2"
            })

            let result = await getAccumulatedBanDuration(USER_ID, 'Used comic sans in a meme', INITIAL_DURATION);
            expect(result).toBe(LAST_BAN_DURATION + INITIAL_DURATION)

            mockedGetLastBanForUserWithReason.mockResolvedValueOnce({
                reason: 'Used comic sans in a meme',
                end_time: new Date().toISOString(),
                duration: LAST_BAN_DURATION,
                discord_id: USER_ID,
                admin_id: "321312",
                admin_name: "c00lb0t2"
            })

            result = await getAccumulatedBanDuration(USER_ID, 'Asked NPCs on a date', INITIAL_DURATION);
            expect(result).toBe(INITIAL_DURATION)
        })
    })
})
