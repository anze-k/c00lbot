import { getSuffixOf } from '../../src/bot/embeds/utils/number_suffix'

describe('getSuffixOf', () => {
    it('should return the correct suffix for a given number', () => {
        expect(getSuffixOf(1)).toBe('1st')
        expect(getSuffixOf(2)).toBe('2nd')
        expect(getSuffixOf(3)).toBe('3rd')
        expect(getSuffixOf(4)).toBe('4th')
        expect(getSuffixOf(11)).toBe('11th')
        expect(getSuffixOf(12)).toBe('12th')
        expect(getSuffixOf(13)).toBe('13th')
        expect(getSuffixOf(21)).toBe('21st')
        expect(getSuffixOf(22)).toBe('22nd')
        expect(getSuffixOf(23)).toBe('23rd')
        expect(getSuffixOf(101)).toBe('101st')

    })
})
