import Database from 'better-sqlite3'
import config from 'config'
import * as fs from 'fs'
import { parse } from 'csv-parse'


const db = new Database(config.get('dbConfig.dbName'), {});

const load = async (): Promise<void> => {
    await db.prepare(`update maps set status = 'DISABLED', current_weight = default_weight;`).run()

    fs.createReadStream("./resources/maps.csv")
    .pipe(parse({ delimiter: ",", from_line: 1 }))
    .on("data", function (rows) {

        for (const row of rows) {
            console.log('Updating map ' + row + ' to ENABLED')
            db.prepare(`update maps set status = 'ENABLED' where name = ?`).run(row)
        }
    })
}

load()
