import Database from 'better-sqlite3'
import config from 'config'
import * as fs from 'fs'
import { parse } from 'csv-parse'


const db = new Database(config.get('dbConfig.dbName'), {});

const insert = db.prepare('INSERT INTO players (discord_id, nickname, rating, starting_rating) VALUES (?, ?, ?, ?)');
const insertMany = db.transaction((rows) => {
    for (const row of rows) insert.run(row);
});


const load = async (): Promise<void> => {
    const rows: Array<string>[] = []

    fs.createReadStream("./resources/wages.csv")
    .pipe(parse({ delimiter: ",", from_line: 2 }))
    .on("data", function (row) {
        rows.push([row[2], row[0], row[3], row[3]])
    })
    .on("end", function () {
        insertMany(rows)
    })
}

load()
