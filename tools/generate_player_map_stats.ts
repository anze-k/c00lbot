import Database from 'better-sqlite3'
import config from 'config'

const db = new Database(config.get('dbConfig.dbName'), {})

interface MapStats {
    played: number
    preference: number
}

interface PlayerStats {
    id: string
    loved: number
    lovedPlayed: number
    liked: number
    likedPlayed: number
    sometimes: number
    sometimesPlayed: number
    hated: number
    hatedPlayed: number
    total: number
}

type MapPreference = {
    map_name: string
    score: number
}

interface PugData {
    id: string
    players: string
    map_name: string
}

const generate = async (): Promise<void> => {
    const pugs: PugData[] = db.prepare(`
        SELECT p.id, p.players, m.name as map_name FROM pugs p
        JOIN scores s ON p.id = s.pug_id
        JOIN maps m ON p.map_id = m.id
        WHERE p.started > '2023-04-02'
    `).all() as PugData[]

    const playerMaps = new Map<string, Map<string, MapStats>>()

    for (const pug of pugs) {
        const pugPlayers = pug.players.split(',')
        for (const player of pugPlayers) {
            if (!playerMaps.has(player)) {
                playerMaps.set(player, new Map<string, MapStats>())
            }
            const playerMap = playerMaps.get(player)!
            if (!playerMap.has(pug.map_name)) {
                playerMap.set(pug.map_name, { played: 1, preference: -1 })
            } else {
                const stats = playerMap.get(pug.map_name)!
                playerMap.set(pug.map_name, { played: stats.played + 1, preference: stats.preference })
            }
        }
    }

    const preferences: MapPreference[] = db.prepare(`
        SELECT player_id, map_name, score FROM map_preferences
    `).all() as MapPreference[]

    for (const pref of preferences) {
        const playerMap = playerMaps.get(pref.map_name)
        if (playerMap) {
            const mapStats = playerMap.get(pref.map_name) || { played: 0, preference: pref.score }
            playerMap.set(pref.map_name, mapStats)
        }
    }

    const playerObjs = []

    for (const player of playerMaps) {
        const playerObj = new Object({
            id: player[0],
            loved: 0,
            lovedPlayed: 0,
            liked: 0,
            likedPlayed: 0,
            sometimes: 0,
            sometimesPlayed: 0,
            hated: 0,
            hatedPlayed: 0,
            total: 0,
        }) as PlayerStats

        for (const p of player[1]) {
            playerObj.total += 1

            if (p[1].preference === -10) {
                playerObj.hated += 1
                playerObj.hatedPlayed += p[1].played
            }
            if (p[1].preference === 0) {
                playerObj.sometimes += 1
                playerObj.sometimesPlayed += p[1].played
            }
            if (p[1].preference === 5) {
                playerObj.liked += 1
                playerObj.likedPlayed += p[1].played
            }
            if (p[1].preference === 10) {
                playerObj.loved += 1
                playerObj.lovedPlayed += p[1].played
            }
        }

        playerObjs.push(playerObj)

    }

    const total = new Object({
        id: 'total',
        loved: 0,
        lovedPlayed: 0,
        liked: 0,
        likedPlayed: 0,
        sometimes: 0,
        sometimesPlayed: 0,
        hated: 0,
        hatedPlayed: 0,
        total: 0,
    }) as PlayerStats

    for (const playerObj of playerObjs) {
        total.loved += playerObj.loved
        total.lovedPlayed += playerObj.lovedPlayed
        total.liked += playerObj.liked
        total.likedPlayed += playerObj.likedPlayed
        total.sometimes += playerObj.sometimes
        total.sometimesPlayed += playerObj.sometimesPlayed
        total.hated += playerObj.hated
        total.hatedPlayed += playerObj.hatedPlayed
        total.total += playerObj.total
    }

    console.log(total)
}

// generate()

//4887
// 37.3%
// 26.3%
// 20.5%
// 15.8%
