import fs from 'fs'

import { getAllEnabledMaps } from '../src/db/dao/map'
import { getModeById } from '../src/db/dao/mode'
import { getAllActivePlayers, getPlayersByDiscordId, Player } from '../src/db/dao/player'
import { fetchAllPugsWithScores, PugWithScore } from '../src/db/dao/pug'
import { getMapExperts } from '../src/pug/get_map_experts'
import { makeTeams } from '../src/pug/make_teams'
import { pickMap } from '../src/pug/pick_map'

function getRandomPlayers(players: Player[], count: number): string[] {
    const selectedPlayers = players.sort(() => 0.5 - Math.random()).slice(0, count)
    return selectedPlayers.map(player => player.discord_id)
}

async function getWinRateForPlayerOnMap(playerId: string, mapName: string): Promise<number> {
    const allPugs: PugWithScore[] = await fetchAllPugsWithScores()
    const relevantPugs = allPugs.filter(pug => pug.map_name === mapName)

    let played = 0
    let won = 0

    for (const pug of relevantPugs) {
        if (pug.team_red.includes(playerId) || pug.team_blue.includes(playerId)) {
            played++

            if ((pug.team_red.includes(playerId) && pug.score_red > pug.score_blue) ||
                (pug.team_blue.includes(playerId) && pug.score_blue > pug.score_red)) {
                won++
            }
        }
    }

    return (played > 0) ? (won / played) * 100 : 0
}

async function simulatePUG() {
    const activePlayers = await getAllActivePlayers()
    const mockPlayerIds = getRandomPlayers(activePlayers, 10)
    const mode = await getModeById(2)

    const maps = await getAllEnabledMaps()
    const map = await pickMap(maps, mockPlayerIds)

    const mockPlayers = await getPlayersByDiscordId(mockPlayerIds)
    const teams = await makeTeams(mockPlayers, map.name, mode.team_size)

    const redTeamTotalRating = teams.red.players.reduce((sum, player) => sum + player.rating, 0)
    const blueTeamTotalRating = teams.blue.players.reduce((sum, player) => sum + player.rating, 0)
    const percentDifference = (Math.abs((redTeamTotalRating - blueTeamTotalRating) / ((redTeamTotalRating + blueTeamTotalRating) / 2)) * 100).toFixed(2)

    const mapExpertsIds = await getMapExperts(map.name, mockPlayers)

    const formatPlayer = async (player: Player) => {
        const isExpert = mapExpertsIds.includes(player.discord_id)
        const winRateValue = isExpert ? await getWinRateForPlayerOnMap(player.discord_id, map.name) : null
        const winRate = winRateValue !== null ? ` (${winRateValue.toFixed(2)}%)` : ''
        const formatted = `${player.nickname} (${player.rating})${winRate}`
        return isExpert ? `**${formatted}**` : formatted
    }

    const redTeamDataPromises = teams.red.players.map(formatPlayer)
    const blueTeamDataPromises = teams.blue.players.map(formatPlayer)

    const redTeamData = await Promise.all(redTeamDataPromises)
    const blueTeamData = await Promise.all(blueTeamDataPromises)

    if (mapExpertsIds.length === 0) {
        return null  // Remove this if you want pugs without map experts too
    }

    return `"${redTeamData.join(', ')}","${blueTeamData.join(', ')}",${redTeamTotalRating},${blueTeamTotalRating},${percentDifference},${map.name},"${mapExpertsIds.map(id => mockPlayers.find(p => p.discord_id === id)?.nickname).join(', ')}"\n`
}

async function generateCSV(simulations: number) {
    let csvData = "Red Team,Blue Team,Red Team Rating,Blue Team Rating,Rating Percent Difference,Map,Map Experts\n"

    for (let i = 0; i < simulations; i++) {
        console.log(`Simulating PUG ${i + 1}/${simulations}...`)
        const pugData = await simulatePUG()
        if (pugData) {
            csvData += pugData
        }
    }

    fs.writeFileSync('pug_simulations2.csv', csvData, 'utf8')
}

generateCSV(10)
