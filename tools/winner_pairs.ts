import fs from 'fs'

import { getPlayerName } from '../src/db/dao/player'
import { fetchAllPugsWithScores, PugWithScore } from '../src/db/dao/pug'

const winRateParameter = 50
const gamePlayedParameter = 5

async function main() {
    const allPugs: PugWithScore[] = await fetchAllPugsWithScores()

    const uniquePlayerIds = new Set<string>()
    allPugs.forEach(pug => {
        pug.team_red.forEach(playerId => uniquePlayerIds.add(playerId))
        pug.team_blue.forEach(playerId => uniquePlayerIds.add(playerId))
    })

    const players: Record<string, string> = {}
    for (const playerId of uniquePlayerIds) {
        const playerNameObj = await getPlayerName(playerId)
        if (playerNameObj && playerNameObj.nickname) {
            players[playerId] = playerNameObj.nickname
        }
    }

    type PairStats = {
        played: number
        won: number
        winRate?: number
    }

    const pairStats: Record<string, PairStats> = {}
    const teamColors: (keyof PugWithScore)[] = ['team_red', 'team_blue']

    allPugs.forEach(pug => {
        teamColors.forEach(teamColor => {
            const teamPlayers = pug[teamColor]
            if (Array.isArray(teamPlayers)) {
                const pairs = combinations(teamPlayers, 2)
                pairs.forEach(pair => {
                    const player1Name = players[pair[0]] || pair[0]
                    const player2Name = players[pair[1]] || pair[1]
                    const mapName = pug.map_name.replace("CTF-", "")
                    const key = `${player1Name},${player2Name},${mapName}`

                    if (!pairStats[key]) {
                        pairStats[key] = { played: 0, won: 0 }
                    }

                    pairStats[key].played++

                    if (teamColor === 'team_red' && pug.score_red > pug.score_blue) {
                        pairStats[key].won++
                    } else if (teamColor === 'team_blue' && pug.score_blue > pug.score_red) {
                        pairStats[key].won++
                    }
                })
            }
        })
    })

    const filteredStats = Object.entries(pairStats).filter(([key, stats]) => {
        stats.winRate = (stats.won / stats.played) * 100
        return stats.played >= gamePlayedParameter && stats.winRate > winRateParameter
    }).sort((a, b) => b[1].winRate! - a[1].winRate!)

    let csvContent = "Player1,Player2,Map,Played,Won,WinRate\n"

    filteredStats.forEach(([key, stats]) => {
        const [player1, player2, map] = key.split(',').map(wrapInQuotes)
        csvContent += `${player1},${player2},${map},${stats.played},${stats.won},${stats.winRate}\n`
    })

    fs.writeFileSync(`results#${allPugs.length}.csv`, csvContent)
    console.log(`Total number of pugs processed: ${allPugs.length}`)
}

function combinations<T>(array: T[], size: number): T[][] {
    const results: T[][] = []
    function* combination(arr: T[], size: number): Generator<T[]> {
        const length = arr.length
        if (size > length) return
        if (!size) {
            yield []
            return
        }
        for (let i = 0; i < length; i++) {
            const current = arr.slice(i, i + 1)
            const remaining = arr.slice(i + 1)
            for (const sub of combination(remaining, size - 1)) {
                yield current.concat(sub)
            }
        }
    }
    for (const comb of combination(array, size)) {
        results.push(comb)
    }
    return results
}

function wrapInQuotes(value: string): string {
    return `"${value}"`
}

main()
