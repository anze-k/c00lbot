import fs from 'fs'

import { getPlayerName } from '../src/db/dao/player'
import { fetchAllPugsWithScores, PugWithScore } from '../src/db/dao/pug'

const winRateParameter = 65
const gamePlayedParameter = 5

async function main() {
    const allPugs: PugWithScore[] = await fetchAllPugsWithScores()

    type PlayerStats = {
        played: number
        won: number
        winRate?: number
    }

    const playerStats: Record<string, PlayerStats> = {}
    const teamColors: (keyof PugWithScore)[] = ['team_red', 'team_blue']

    allPugs.forEach(pug => {
        teamColors.forEach(teamColor => {
            const teamPlayers = pug[teamColor]
            if (Array.isArray(teamPlayers)) {
                const mapName = pug.map_name.replace("CTF-", "")
                teamPlayers.forEach(playerId => {
                    const key = `${playerId},${mapName}`

                    if (!playerStats[key]) {
                        playerStats[key] = { played: 0, won: 0 }
                    }

                    playerStats[key].played++

                    if (teamColor === 'team_red' && pug.score_red > pug.score_blue) {
                        playerStats[key].won++
                    } else if (teamColor === 'team_blue' && pug.score_blue > pug.score_red) {
                        playerStats[key].won++
                    }
                })
            }
        })
    })

    const filteredStats = Object.entries(playerStats).filter(([key, stats]) => {
        stats.winRate = (stats.won / stats.played) * 100
        return stats.played >= gamePlayedParameter && stats.winRate > winRateParameter
    }).sort((a, b) => b[1].winRate! - a[1].winRate!)

    let csvContent = "Player,Map,Played,Won,WinRate\n"

    await Promise.all(
        filteredStats.map(async ([key, stats]) => {
            const [playerId, map] = key.split(',')
            const playerNameObj = await getPlayerName(playerId)
            const playerName = (playerNameObj && playerNameObj.nickname) || playerId
            csvContent += `${wrapInQuotes(playerName)},${wrapInQuotes(map)},${stats.played},${stats.won},${stats.winRate}\n`
        })
    )

    fs.writeFileSync(`individual_results#${allPugs.length}.csv`, csvContent)
    console.log(`Total number of pugs processed: ${allPugs.length}`)
}

function wrapInQuotes(value: string): string {
    return `"${value}"`
}

main()
