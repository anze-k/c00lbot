import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

export const clearPasswords = async (): Promise<void> => {
    await db.prepare(`delete from passwords`).run()
}
