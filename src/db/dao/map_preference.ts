import sqlite3 from 'better-sqlite3'
import config from 'config'
import { MapStatus } from '../../bot/embeds/utils/constants'

const db = sqlite3(config.get('dbConfig.dbName'))

export enum MapPreferenceScore {
    Constantly = 10,
    Often = 5,
    Sometimes = 0,
    AlmostNever = -10
}
export interface MapPreference {
    map_name: string
    map_id: string
    player_id: string
    score: number
}

export const getAllMapPreferencesForPlayers = async (playerIds: string[]): Promise<MapPreference[]> => {
    return db.prepare(`SELECT * FROM map_preferences WHERE player_id IN (${playerIds.map(() => '?').join(',')}) ORDER BY map_name ASC`).all(...playerIds) as MapPreference[]
}

export const getEnabledMapPreferencesForPlayers = async (playerIds: string[]): Promise<MapPreference[]> => {
    return db.prepare(`SELECT * FROM map_preferences mp 
        join maps m on m.id = mp.map_id  WHERE mp.player_id IN (${playerIds.map(() => '?').join(',')})
        and m.status = ? ORDER BY mp.map_name ASC`).all(...playerIds, MapStatus.Enabled) as MapPreference[]
}

export const getMapPreferencesForPlayers = async (playerIds: string[], mapId: string): Promise<MapPreference[]> => {
    const placeholders = playerIds.map(() => '?').join(',')
    return (db.prepare(`SELECT * FROM map_preferences WHERE player_id IN (${placeholders}) AND map_id = ? ORDER BY map_name ASC`)
        .all(...playerIds, mapId)) as MapPreference[]
}

export const insertMapPreference = async (playerId: string, mapName: string, mapId: string, score: number): Promise<void> => {
    db.prepare(`INSERT INTO map_preferences (player_id, map_name, map_id, score) VALUES (?, ?, ?, ?) ON CONFLICT (player_id, map_id) DO UPDATE SET score = EXCLUDED.score`)
        .run(playerId, mapName, mapId, score)
}
