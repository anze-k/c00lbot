import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

interface EligiblePlayer {
    player_id: string
}

export const fetchEligiblePlayers = async (mapId: string, playerIds: string[], gamesPlayedParam: number, winRateParam: number): Promise<EligiblePlayer[]> => {
    const sql = `
        SELECT player_id
        FROM map_stats
        WHERE map_id = ?
        AND player_id IN (${new Array(playerIds.length).fill('?').join(', ')})
        AND games_played >= ?
        AND (games_won * 1.0 / games_played * 1.0) * 100 > ?
    `
    return db.prepare(sql).all(mapId, ...playerIds, gamesPlayedParam, winRateParam) as EligiblePlayer[]
}
