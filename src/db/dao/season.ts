import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface Season {
    id: string
    name: string
    start_date: Date
    end_date: Date
    archived: boolean
    stats_msg_id?: string
    leaderboard_msg_id?: string
}

export const getCurrentSeason = async (): Promise<Season> => {
    const season: Season | undefined = await db.prepare(`SELECT * from seasons WHERE archived = 0 and start_date <= CURRENT_DATE and end_date >= CURRENT_DATE`).get() as Season | undefined
    if (!season) {
        throw new Error("No current season is active or matches the current date criteria.")
    }
    return season
}

export const setCurrentSeasonStatsMsgId = async (statsMsgId: string, id: string): Promise<void> => {
    const info = db.prepare(`UPDATE seasons SET stats_msg_id = ? WHERE id = ?`).run(statsMsgId, id)
    if (info.changes === 0) {
        throw new Error("No season was updated.")
    }
}

export const setCurrentLeaderboardMsgId = async (statsMsgId: string, id: string): Promise<void> => {
    const info = db.prepare(`UPDATE seasons SET leaderboard_msg_id = ? WHERE id = ?`).run(statsMsgId, id)
    if (info.changes === 0) {
        throw new Error("No season was updated.")
    }
}
