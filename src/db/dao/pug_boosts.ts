import sqlite3 from 'better-sqlite3'
import config from 'config'

import { pugMode } from '../../bot/embeds/utils/constants'
import { getCurrentPug } from './pug'

const db = sqlite3(config.get('dbConfig.dbName'))

enum BoostType {
    ELO = 'elo'
}

export interface PugBoostFinished extends PugBoost {
    pug_id: number
}

export interface PugBoost {
    id: number
    boost_type: BoostType
    value: number
    pug_id?: number
}

// Gets
export const getActiveEloBoost = async (): Promise<PugBoost> => {
    let boost: PugBoost | undefined = await db.prepare(`SELECT * FROM pug_boosts WHERE pug_id is null AND boost_type = 'elo'`).get() as PugBoost | undefined
    if (!boost) {
        await insertEmptEloBoost()
        boost = await db.prepare(`SELECT * FROM pug_boosts WHERE pug_id is null AND boost_type = 'elo'`).get() as PugBoost | undefined
        if (!boost) {
            throw new Error("Failed to create AND retrieve new Elo boost.")
        }
    }
    return boost
}

export const isPugDoubleElo = async (pugId: number): Promise<boolean> => {
    const boost: PugBoost | undefined = await db.prepare(`SELECT * FROM pug_boosts WHERE pug_id = ? AND boost_type = 'elo'`).get(pugId) as PugBoost | undefined
    return !!boost
}

// Updates
export const updateValueForBoost = async (value: number, id: number): Promise<void> => {
    const info = db.prepare(`UPDATE pug_boosts SET value = value + ? WHERE id = ?`).run(value, id)
    if (info.changes === 0) {
        throw new Error("No boost updated, check the ID.")
    }
}

export const updateValueForBoostFinish = async (id: number): Promise<void> => {
    const info = db.prepare(`UPDATE pug_boosts SET value = 10000 WHERE id = ?`).run(id)
    if (info.changes === 0) {
        throw new Error("No boost finished, check the ID.")
    }
}

export const updatePugForBoost = async (pugId: number, id: number): Promise<void> => {
    const info = db.prepare(`UPDATE pug_boosts SET pug_id = ? WHERE id = ?`).run(pugId, id)
    if (info.changes === 0) {
        throw new Error("No boost updated with new Pug ID, check the boost AND Pug ID.")
    }
}

export const resolveBoostedDeadPug = async (pugId: number): Promise<void> => {
    const existingBoost: PugBoost | undefined = await db.prepare(`SELECT * FROM pug_boosts WHERE pug_id = ?`).get(pugId) as PugBoost | undefined
    if (existingBoost) {
        const newPug = await getCurrentPug(pugMode)
        if (!newPug.id) throw new Error("No current Pug ID found.")
        await updatePugForBoost(parseInt(newPug.id), existingBoost.id)
    }
}

// Inserts
const insertEmptEloBoost = async (): Promise<void> => {
    const info = db.prepare(`INSERT INTO pug_boosts (boost_type) VALUES (?)`).run('elo')
    if (info.changes === 0) {
        throw new Error("Failed to insert a new Elo boost.")
    }
}
