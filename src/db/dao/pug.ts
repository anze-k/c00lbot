import sqlite3 from 'better-sqlite3'
import config from 'config'

import { ScoreStatus } from '../../bot/embeds/utils/constants'
import { Server } from './server'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface LivePug {
    id?: string
    players: string[]
    teams: string[]
    mode_id: number
    created: Date
    started?: Date
    finished?: Date
    score_red?: number
    score_blue?: number
    rating_change_red?: number
    rating_change_blue?: number
    str_red?: number
    str_blue?: number
    score_id: string
    map_id?: string
}

export interface LastPug {
    id: string
    team_red: string[]
    team_blue: string[]
    mode_id: number
    created: Date
    started: Date
    finished: string
    score_red: number
    score_blue: number
    name: string
    str_red: number
    str_blue: number
    map_id?: string
    status?: string
}

interface LasttPug {
    id: string
    team_red: string[]
    team_blue: string[]
    mode_id: number
    created: Date
    started: Date
    finished: string
    score_red: number
    score_blue: number
    name: string
    str_red: number
    str_blue: number
    map_id?: string
}

export interface HasId {
    id: number
}

export interface InsertedScore {
    score_red: number
    score_blue: number
    score_msg_id: string
}

export interface LivePugDto extends Omit<LivePug, 'players'> {
    players: string
    team_red: string[]
    team_blue: string[]
}

interface LastPugDto extends Omit<LastPug, 'team_red' | 'team_blue'> {
    team_red: string
    team_blue: string
}

interface LasttPugDto extends Omit<LasttPug, 'team_red' | 'team_blue'> {
    team_red: string
    team_blue: string
}

export interface LivePugFinishedDto extends Omit<LivePug, 'players'> {
    id: string
    players: string
    started: Date
    team_red: string
    team_blue: string
    finished: Date
    server_id: string
}

export interface LivePugScoredDto extends LivePugFinishedDto {
    score_red: number
    score_blue: number
    str_red: number
    str_blue: number
}

export interface LivePugFinished extends Omit<LivePugFinishedDto, 'players' | 'team_red' | 'team_blue'> {
    players: string[]
    team_red: string[]
    team_blue: string[]
    server_id: string
}

export interface LivePugScored extends LivePugFinished {
    score_red: number
    score_blue: number
    str_red: number
    str_blue: number
    score_msg_id: string
}

export interface PugScoredWithMap extends LivePugScored {
    map_name: string
}

export interface MapLastPlayed {
    finished: string
}

export interface PugStats {
    count: number
    diff: number
    caps: number
}

export interface TodaysPugs {
    id: string
    finished: string
    str_red: number
    str_blue: number
    map_name: string
    score_red: number
    score_blue: number
}

interface PugsCount {
    day: Date
    cnt: number
}

//Gets
export const getCurrentPug = async (mode_id: number): Promise<LivePug> => {
    const livePug: LivePugDto | undefined = await db.prepare(`
        SELECT id, players, team_red, team_blue,
        datetime(created, 'localtime') as created,
        datetime(started, 'localtime') as started,
        datetime(finished, 'localtime') as finished,
        str_red, str_blue, rating_change_red, rating_change_blue,
        mode_id, map_id, score_id, server_id
        FROM pugs
        WHERE mode_id = ? AND started IS NULL
        ORDER BY created DESC
    `).get(mode_id) as LivePugDto | undefined

    if (!livePug) {
        const newPug = await insertNewEmptyPug(mode_id)
        return {
            ...newPug,
            players: newPug.players ? newPug.players.split(',') : []
        }
    }

    return {
        ...livePug,
        players: livePug.players ? livePug.players.split(',') : []
    }
}


export const getLastPug = async (mode_id: number): Promise<LastPug | null> => {
    const lastPug: LastPugDto | undefined = await db.prepare(`
        SELECT pug.id, pug.team_red, pug.team_blue,
        datetime(pug.created, 'localtime') as created,
        datetime(pug.started, 'localtime') as started,
        datetime(pug.finished, 'localtime') as finished,
        pug.str_red, pug.str_blue, pug.mode_id, map.name,
        score.score_red, score.score_blue
        FROM all_pugs pug
        JOIN maps map ON map.id = pug.map_id
        JOIN all_scores score ON score.pug_id = pug.id
        WHERE pug.mode_id = ? AND pug.started IS NOT NULL AND score.status != '${ScoreStatus.Rejected}'
        ORDER BY pug.created DESC
    `).get(mode_id) as LastPugDto | undefined

    if (!lastPug) return null

    return {
        ...lastPug,
        team_red: lastPug.team_red ? lastPug.team_red.split(',') : [],
        team_blue: lastPug.team_blue ? lastPug.team_blue.split(',') : []
    }
}

export const getLasttPug = async (mode_id: number): Promise<LasttPug[]> => {
    const lasttPugDtos: LasttPugDto[] = db.prepare(`
        SELECT pug.id, pug.team_red, pug.team_blue, datetime(pug.created, 'localtime') as created, datetime(pug.started, 'localtime') as started,
        datetime(pug.finished, 'localtime') as finished, pug.str_red, pug.str_blue, pug.mode_id, map.name as name, score.score_red, score.score_blue
        FROM all_pugs pug
        JOIN maps map ON map.id = pug.map_id
        JOIN all_scores score ON score.pug_id = pug.id
        WHERE pug.mode_id = ? AND pug.started is not null and score.status != '${ScoreStatus.Rejected}'
        ORDER BY pug.created DESC LIMIT 11
    `).all(mode_id) as LasttPugDto[]

    return lasttPugDtos.map(pug => ({
        ...pug,
        team_red: pug.team_red.split(','),
        team_blue: pug.team_blue.split(','),
    }))
}

export const getSpecificLastPug = async (mode_id: number, pug_id: string): Promise<LastPug> => {
    const lastPugDto = await db.prepare(`
        SELECT pug.id, pug.team_red, pug.team_blue, datetime(pug.created, 'localtime') as created, datetime(pug.started, 'localtime') as started,
        datetime(pug.finished, 'localtime') as finished, pug.str_red, pug.str_blue, pug.mode_id, map.name, score.score_red, score.score_blue, score.status
        FROM all_pugs pug
        JOIN maps map ON map.id = pug.map_id
        JOIN all_scores score ON score.pug_id = pug.id
        WHERE pug.mode_id = ? AND pug.id = ? AND pug.started is not null
    `).get(mode_id, pug_id) as LastPugDto | undefined

    if (lastPugDto) {
        return {
            ...lastPugDto,
            team_red: lastPugDto.team_red.split(','),
            team_blue: lastPugDto.team_blue.split(',')
        }
    } else {
        throw new Error("No pug found with the given ID.")
    }
}

export const getPugById = async (pug_id: number): Promise<LivePugScoredDto> => {
    const pug = await db.prepare(`SELECT * from pugs WHERE id = ?`).get(pug_id) as LivePugScoredDto
    return pug
}

export const getLivePugById = async (pug_id: number): Promise<LivePugScoredDto> => {
    const pug = await db.prepare(`
        SELECT * from pugs p JOIN scores s ON s.pug_id = p.id
        WHERE p.id = ? AND p.started is not null AND p.rating_change_blue is null AND p.finished is null AND s.status IN ('PENDING', 'LIVE', 'MANUAL')
    `).get(pug_id) as LivePugScoredDto
    return pug
}

export const getLastPlayerPug = async (playerDiscordID: string): Promise<LivePugFinishedDto[]> => {
    const pugs = db.prepare(`
        SELECT id, players, team_red, team_blue,
        datetime(created, 'localtime') as created,
        datetime(started, 'localtime') as started,
        datetime(finished, 'localtime') as finished,
        str_red, str_blue, rating_change_red, rating_change_blue,
        mode_id, map_id, score_id, server_id
        FROM pugs
        WHERE rating_change_blue is not null AND players LIKE '%${playerDiscordID}%'
        ORDER BY finished desc
        LIMIT 1
    `).all() as LivePugFinishedDto[]
    return pugs
}

export const getRunningPugsForUser = async (playerDiscordID: string): Promise<LivePugFinishedDto[]> => {
    const pugs = db.prepare(`
        SELECT id, players, team_red, team_blue,
        datetime(created, 'localtime') as created,
        datetime(started, 'localtime') as started,
        datetime(finished, 'localtime') as finished,
        str_red, str_blue, rating_change_red, rating_change_blue,
        mode_id, map_id, score_id, server_id
        FROM pugs
        WHERE players LIKE '%${playerDiscordID}%'
        ORDER BY finished desc
    `).all() as LivePugFinishedDto[]
    return pugs
}

export const getAllFinishedPugsForUser = async (discord_id: string, limit?: number): Promise<LivePugFinishedDto[]> => {
    const pugs = db.prepare(`
        SELECT id, players, team_red, team_blue,
        datetime(created, 'localtime') AS created,
        datetime(started, 'localtime') AS started,
        datetime(finished, 'localtime') AS finished,
        str_red, str_blue, rating_change_red, rating_change_blue,
        mode_id, map_id, score_id, server_id
        FROM all_pugs
        WHERE players LIKE '%${discord_id}%' AND rating_change_red IS NOT NULL
        ORDER BY finished DESC
        ${limit ? 'LIMIT ' + limit : ''}
    `).all() as LivePugFinishedDto[]
    return pugs
}

export const getSetOfFinishedPugsForUser = async (discord_id: string): Promise<LivePugFinishedDto[]> => {
    const pugs = db.prepare(`
        SELECT id, players, team_red, team_blue,
        datetime(created, 'localtime') AS created,
        datetime(started, 'localtime') AS started,
        datetime(finished, 'localtime') AS finished,
        str_red, str_blue, rating_change_red, rating_change_blue,
        mode_id, map_id, score_id, server_id
        FROM all_pugs
        WHERE players LIKE '%${discord_id}%' AND rating_change_red IS NOT NULL
        ORDER BY finished DESC
        LIMIT 11
    `).all() as LivePugFinishedDto[]
    return pugs
}

export const getMapLastPlayed = async (map_name: string): Promise<MapLastPlayed> => {
    return db.prepare(`
        SELECT datetime(finished, 'localtime') as finished
        FROM pugs JOIN maps ON maps.id = pugs.map_id
        WHERE maps.name = ?
        ORDER BY pugs.finished DESC
        LIMIT 1
    `).get(map_name) as MapLastPlayed
}

export const getAllStrongestPugs = async (): Promise<LivePugFinished[]> => {
    const pugs = db.prepare(`
        SELECT (str_red + str_blue) as strength, p.id, p.players, p.team_red, p.team_blue, p.str_red, p.str_blue, datetime(p.finished, 'localtime') as finished, s.score_red, s.score_blue
        FROM all_pugs p
        JOIN all_scores s ON p.id = s.pug_id
        WHERE p.finished NOT NULL AND s.status != '${ScoreStatus.Rejected}'
        ORDER BY strength desc
        LIMIT 3
    `).all() as LivePugFinished[]
    return pugs
}

export const getAllWeakestPugs = async (): Promise<LivePugFinished[]> => {
    return db.prepare(`
        SELECT (str_red + str_blue) AS strength, p.id, p.players, p.team_red, p.team_blue, p.str_red, p.str_blue, datetime(p.finished, 'localtime') AS finished, s.score_red, s.score_blue
        FROM all_pugs p JOIN all_scores s ON p.id = s.pug_id
        WHERE p.finished IS NOT NULL AND s.status != '${ScoreStatus.Rejected}'
        ORDER BY strength
        LIMIT 3
    `).all() as LivePugFinished[]
}

export const getSeasonStrongestPugs = async (): Promise<LivePugFinished[]> => {
    return db.prepare(`
        SELECT (str_red + str_blue) AS strength, p.id, p.players, p.team_red, p.team_blue, p.str_red, p.str_blue, datetime(p.finished, 'localtime') AS finished, s.score_red, s.score_blue
        FROM pugs p JOIN scores s ON p.id = s.pug_id
        WHERE p.finished IS NOT NULL AND s.status != '${ScoreStatus.Rejected}'
        ORDER BY strength DESC
        LIMIT 3
    `).all() as LivePugFinished[]
}

export const getSeasonWeakestPugs = async (): Promise<LivePugFinished[]> => {
    return db.prepare(`
        SELECT (str_red + str_blue) AS strength, p.id, p.players, p.team_red, p.team_blue, p.str_red, p.str_blue, datetime(p.finished, 'localtime') AS finished, s.score_red, s.score_blue
        FROM pugs p JOIN scores s ON p.id = s.pug_id
        WHERE p.finished IS NOT NULL AND s.status != '${ScoreStatus.Rejected}'
        ORDER BY strength
        LIMIT 3
    `).all() as LivePugFinished[]
}

export const getLivePugForUser = async (player_discord_id: string): Promise<LivePugFinished | null> => {
    const pug = await db.prepare(`
        SELECT p.id, p.players, p.team_red, p.team_blue,
        datetime(p.created, 'localtime') as created,
        datetime(p.started, 'localtime') as started,
        datetime(p.finished, 'localtime') as finished,
        p.str_red, p.str_blue, p.rating_change_red, p.rating_change_blue,
        p.mode_id, p.map_id, p.score_id, p.server_id
        FROM pugs p JOIN scores s ON p.id = s.pug_id
        WHERE (s.status = '${ScoreStatus.Pending}' or s.status = '${ScoreStatus.Live}')
        AND p.players LIKE '%${player_discord_id}%'
    `).get() as LivePugFinished | null
    return pug
}

export const getLiveServers = async (): Promise<Server[]> => {
    const servers = db.prepare(`
        SELECT serv.*, p.id AS pug_id FROM pugs p
        JOIN scores s ON p.id = s.pug_id
        JOIN servers serv ON serv.id = p.server_id
        WHERE s.status = '${ScoreStatus.Live}'
    `).all() as Server[]
    return servers
}

export const getAllPugStats = async (mode_id: number): Promise<PugStats> => {
    const stats = await db.prepare(`
        SELECT count(s.pug_id) AS count, round(avg(abs(s.score_blue - s.score_red)), 2) AS diff, round(avg(abs(s.score_blue + s.score_red)), 2) AS caps
        FROM all_pugs p
        JOIN all_scores s ON s.pug_id = p.id
        JOIN maps m ON m.id = p.map_id
        WHERE NOT s.status = 'REJECTED' AND s.score_blue NOT NULL AND p.mode_id = ?
        ORDER BY diff DESC
    `).get(mode_id) as PugStats
    return stats
}

export const getSeasonPugStats = async (mode_id: number): Promise<PugStats> => {
    const stats = await db.prepare(`
        SELECT count(s.pug_id) AS count, round(avg(abs(s.score_blue - s.score_red)), 2) AS diff, round(avg(abs(s.score_blue + s.score_red)), 2) AS caps
        FROM pugs p
        JOIN scores s ON s.pug_id = p.id
        JOIN maps m ON m.id = p.map_id
        WHERE NOT s.status = 'REJECTED' AND s.score_blue IS NOT NULL AND p.mode_id = ?
        ORDER BY diff DESC
    `).get(mode_id) as PugStats
    return stats
}

export const getTodaysPugs = async (mode_id: number): Promise<TodaysPugs[]> => {
    const pugs = db.prepare(`
        SELECT pug.id, datetime(pug.finished, 'localtime') AS finished, pug.str_red, pug.str_blue, pug.mode_id, map.name AS map_name, score.score_red, score.score_blue
        FROM pugs pug
        JOIN maps map ON map.id = pug.map_id
        JOIN scores score ON score.pug_id = pug.id
        WHERE pug.mode_id = ? AND pug.started IS NOT NULL AND score.status != '${ScoreStatus.Rejected}' AND pug.finished >= CURRENT_DATE
        ORDER BY pug.created
    `).all(mode_id) as TodaysPugs[]
    return pugs
}

export const getLastPugId = async (): Promise<HasId> => {
    const lastPug = await db.prepare(`
        SELECT id
        FROM pugs
        WHERE started is not null
        ORDER BY started DESC
        LIMIT 1
    `).get() as HasId
    return lastPug
}

export const getPugsByDay = async ({ discord_id, map }: { discord_id?: string, map?: string } = {}): Promise<PugsCount[]> => {
    const filters = ['rating_change_blue IS NOT NULL'];
    const statementParams = [];

    if (discord_id) {
        filters.push(`players LIKE '%${discord_id}%'`);
    }

    if (map) {
        filters.push(`map.name = ?`);
        statementParams.push(map);
    }

    const whereConditions = filters.join(' AND ');

    return db.prepare(`
        SELECT count(*) AS cnt, map.name as mapName, strftime('%d-%m-%Y', started) AS day
        FROM all_pugs
        JOIN maps map ON map.id = map_id
        WHERE ${whereConditions}
        GROUP BY day
        ORDER BY started DESC
    `).all(...statementParams) as PugsCount[]
}

//Updates
export const updatePugPlayers = async (players: string, pug_id: string) => {
    db.prepare(`UPDATE pugs SET players = ? WHERE id = ? `).run(players, pug_id)
}

export const updatePugToStart = async (team_red: string, team_blue: string, str_red: number, str_blue: number, server_id: string, pug_id: string, map_id: string, score_id: string) => {
    db.prepare(`UPDATE pugs SET team_red = ?, team_blue = ?, str_red = ?, str_blue = ?, started = CURRENT_TIMESTAMP, server_id = ?, map_id = ?, score_id = ? WHERE id = ? `)
        .run(team_red, team_blue, str_red, str_blue, server_id, map_id, score_id, pug_id)
}

export const updateRatings = async (pug: LivePugScoredDto, rating_red: number, rating_blue: number): Promise<void> => {
    db.exec('BEGIN')

    db.prepare(`update players SET rating = rating + ?, rating_change = ?, pugs_played = pugs_played + 1 WHERE discord_id in (${pug.team_red})`).run(rating_red, rating_red)
    db.prepare(`update players SET rating = rating + ?, rating_change = ?, pugs_played = pugs_played + 1 WHERE discord_id in (${pug.team_blue})`).run(rating_blue, rating_blue)

    db.prepare('update pugs SET rating_change_red = ?, rating_change_blue = ? WHERE id = ?').run(rating_red, rating_blue, pug.id)

    db.exec('END')
}

export const finishPug = async (score_id: string, pug_id: string) => {
    await db.prepare(`UPDATE pugs SET finished = CURRENT_TIMESTAMP, score_id = ? WHERE id = ?  returning *`).get(score_id, pug_id)
}

export const resetPug = async (pug_id: string) => {
    db.prepare(
        `UPDATE pugs SET players = NULL WHERE id = ? AND started IS NULL AND finished IS NULL`
    ).run(pug_id)
}

export const setPugServerIp = async (pug_id: string) => {
    db.prepare(
        `UPDATE pugs SET server_id = 99 WHERE id = ?`
    ).run(pug_id)
}

//Inserts
export const insertNewEmptyPug = async (mode_id: number): Promise<LivePugDto> => {
    return <LivePugDto><unknown>db.prepare(`INSERT INTO pugs (mode_id) VALUES (?) RETURNING *`).get(mode_id)
}

////////////// STATS STUFF
export interface WonPugsTeammates {
    blue: PugScoredWithMap[],
    red: PugScoredWithMap[]
}

interface TeamStrengthCapDiff {
    elo6k7: number,
    elo7k: number,
    elo7k3: number,
    elo7k6: number,
    elo7k9: number,
    elo8k2: number,
}

type CapDiffResult = {
    capdiff: number | null
}

export const getAllPugsWithTwoTeammates = async (id1: string, id2: string): Promise<WonPugsTeammates> => {
    const blue = db.prepare(`
        SELECT p.*, s.score_red, s.score_blue FROM all_pugs p JOIN all_scores s ON s.pug_id = p.id
        WHERE (p.team_blue LIKE ('%${id1}%${id2}%') OR p.team_blue LIKE ('%${id2}%${id1}%'))
        AND p.rating_change_blue IS NOT NULL AND s.status IN ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')
    `).all() as PugScoredWithMap[]
    const red = db.prepare(`
        SELECT p.*, s.score_red, s.score_blue FROM all_pugs p JOIN all_scores s ON s.pug_id = p.id
        WHERE (p.team_red LIKE ('%${id1}%${id2}%') OR p.team_red LIKE ('%${id2}%${id1}%'))
        AND p.rating_change_red IS NOT NULL AND s.status IN ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')
    `).all() as PugScoredWithMap[]
    return { blue, red }
}

export const getSeasonPugsWithTwoTeammates = async (id1: string, id2: string): Promise<WonPugsTeammates> => {
    const queryBlue = `
        SELECT p.*, s.score_red, s.score_blue, m.name as map_name
        FROM pugs p
        JOIN scores s ON p.id = s.pug_id
        JOIN maps m ON p.map_id = m.id
        WHERE (p.team_blue LIKE ('%${id1}%${id2}%') OR p.team_blue LIKE ('%${id2}%${id1}%'))
        AND p.rating_change_blue IS NOT NULL
        AND s.status IN ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')
    `
    const queryRed = `
        SELECT p.*, s.score_red, s.score_blue, m.name as map_name
        FROM pugs p
        JOIN scores s ON p.id = s.pug_id
        JOIN maps m ON p.map_id = m.id
        WHERE (p.team_red LIKE ('%${id1}%${id2}%') OR p.team_red LIKE ('%${id2}%${id1}%'))
        AND p.rating_change_red IS NOT NULL
        AND s.status IN ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')
    `

    const blue = (db.prepare(queryBlue).all()) as PugScoredWithMap[]
    const red = (db.prepare(queryRed).all()) as PugScoredWithMap[]

    return { blue, red }
}

export const getAllScoredPugsForPlayer = async (id1: string): Promise<WonPugsTeammates> => {
    const query = `
        SELECT p.*, s.score_red, s.score_blue, m.name as map_name
        FROM all_pugs p
        JOIN all_scores s ON p.id = s.pug_id
        JOIN maps m ON p.map_id = m.id
        WHERE (p.team_blue LIKE ('%${id1}%') OR p.team_red LIKE ('%${id1}%'))
        AND p.rating_change_blue IS NOT NULL
        AND s.status IN ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')
    `

    const results = (db.prepare(query).all()) as PugScoredWithMap[]
    const blue = results.filter(r => r.team_blue.includes(id1))
    const red = results.filter(r => r.team_red.includes(id1))

    return { blue, red }
}

export const getSeasonScoredPugsForPlayer = async (id1: string): Promise<WonPugsTeammates> => {
    const queryBlue = `
        SELECT p.*, s.score_red, s.score_blue, m.name as map_name
        FROM pugs p
        JOIN scores s ON p.id = s.pug_id
        JOIN maps m ON p.map_id = m.id
        WHERE (p.team_blue LIKE ('%${id1}%'))
        AND p.rating_change_blue IS NOT NULL
        AND s.status IN ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')
    `
    const queryRed = `
        SELECT p.*, s.score_red, s.score_blue, m.name as map_name
        FROM pugs p
        JOIN scores s ON p.id = s.pug_id
        JOIN maps m ON p.map_id = m.id
        WHERE (p.team_red LIKE ('%${id1}%'))
        AND p.rating_change_red IS NOT NULL
        AND s.status IN ('${ScoreStatus.Accepted}', '${ScoreStatus.Manual}')
    `

    const blueResults = db.prepare(queryBlue).all() as PugScoredWithMap[]
    const redResults = db.prepare(queryRed).all() as PugScoredWithMap[]

    return {
        blue: blueResults,
        red: redResults
    }
}

export const getScoredPugsByTeamStrength = async (): Promise<TeamStrengthCapDiff> => {
    const fetchCapDiff = async (strengthBoundaryLower: number, strengthBoundaryUpper: number): Promise<number> => {
        const result = await db.prepare(`
            SELECT AVG(ABS(s.score_blue - s.score_red)) AS capdiff
            FROM pugs p
            JOIN scores s ON p.id = s.pug_id
            WHERE (p.str_red + p.str_blue) < ${strengthBoundaryUpper}
                AND (p.str_red + p.str_blue) >= ${strengthBoundaryLower}
                AND p.rating_change_blue IS NOT NULL
                AND s.status != '${ScoreStatus.Rejected}'
       `).get() as CapDiffResult

        return result.capdiff ?? 0
    }

    const elo6k7 = await fetchCapDiff(0, 6700)
    const elo7k = await fetchCapDiff(6700, 7000)
    const elo7k3 = await fetchCapDiff(7000, 7300)
    const elo7k6 = await fetchCapDiff(7300, 7600)
    const elo7k9 = await fetchCapDiff(7600, 7900)
    const elo8k2 = await fetchCapDiff(7900, 8200)

    return {
        elo6k7,
        elo7k,
        elo7k3,
        elo7k6,
        elo7k9,
        elo8k2,
    }
}

export const getScoredPugs = async (): Promise<LivePugScored[]> => {
    const rows = db.prepare(`
        SELECT p.*, s.*, m.name AS map_name
        FROM pugs p
        JOIN scores s ON p.id = s.pug_id
        JOIN maps m ON p.map_id = m.id
        WHERE rating_change_blue IS NOT NULL AND s.status != '${ScoreStatus.Rejected}'
    `).all() as LivePugScored[]
    return rows
}

export const getCapDiffs = async (): Promise<number[]> => {
    const results = db.prepare(`
        SELECT abs(s.score_red - s.score_blue) AS diff
        FROM pugs p
        JOIN scores s ON p.id = s.pug_id
        WHERE rating_change_blue IS NOT NULL AND s.status = '${ScoreStatus.Accepted}'
    `).all() as { diff: number }[]
    return results.map(res => res.diff)
}

export const getEnemyWinrateAll = async (id1: string, id2: string): Promise<[number, number] | null> => {
    const blueResults = await db.prepare(`
        SELECT
            SUM(CASE WHEN s.score_red < s.score_blue THEN 1 ELSE 0 END) AS wins,
            SUM(CASE WHEN s.score_red > s.score_blue THEN 1 ELSE 0 END) AS losses
        FROM all_pugs p
        JOIN all_scores s ON p.id = s.pug_id
        WHERE team_blue LIKE '%${id1}%' AND team_red LIKE '%${id2}%'
        AND s.status != 'REJECTED'
    `).get() as { wins: number, losses: number }

    const redResults = await db.prepare(`
        SELECT
            SUM(CASE WHEN s.score_red > s.score_blue THEN 1 ELSE 0 END) AS wins,
            SUM(CASE WHEN s.score_red < s.score_blue THEN 1 ELSE 0 END) AS losses
        FROM all_pugs p
        JOIN all_scores s ON p.id = s.pug_id
        WHERE team_red LIKE '%${id1}%' AND team_blue LIKE '%${id2}%'
        AND s.status != 'REJECTED'
    `).get() as { wins: number, losses: number }

    if (blueResults.wins + blueResults.losses === 0) return null

    const totalPlayedAgainst = redResults.losses + blueResults.losses + redResults.wins + blueResults.wins
    const player1Winrate = Math.round(100 * (redResults.wins + blueResults.wins) / totalPlayedAgainst)

    return [player1Winrate, totalPlayedAgainst]
}

export const getEnemyWinrateSeason = async (id1: string, id2: string): Promise<[number, number] | null> => {
    const blueResults = await db.prepare(`
        SELECT
            SUM(CASE WHEN s.score_red < s.score_blue THEN 1 ELSE 0 END) AS wins,
            SUM(CASE WHEN s.score_red > s.score_blue THEN 1 ELSE 0 END) AS losses
        FROM pugs p
        JOIN scores s ON p.id = s.pug_id
        WHERE team_blue LIKE '%${id1}%' AND team_red LIKE '%${id2}%'
        AND s.status != 'REJECTED'
    `).get() as { wins: number, losses: number }

    const redResults = await db.prepare(`
        SELECT
            SUM(CASE WHEN s.score_red > s.score_blue THEN 1 ELSE 0 END) AS wins,
            SUM(CASE WHEN s.score_red < s.score_blue THEN 1 ELSE 0 END) AS losses
        FROM pugs p
        JOIN scores s ON p.id = s.pug_id
        WHERE team_red LIKE '%${id1}%' AND team_blue LIKE '%${id2}%'
        AND s.status != 'REJECTED'
    `).get() as { wins: number, losses: number }

    if (blueResults.wins + blueResults.losses === 0) return null

    const totalPlayedAgainst = redResults.losses + blueResults.losses + redResults.wins + blueResults.wins
    const player1Winrate = Math.round(100 * (redResults.wins + blueResults.wins) / totalPlayedAgainst)

    return [player1Winrate, totalPlayedAgainst]
}

interface PugWithScoreRow {
    id: string
    team_red: string
    team_blue: string
    score_red: number
    score_blue: number
    map_name: string
    map_id: string
}

export interface PugWithScore {
    id: string
    team_red: string[]
    team_blue: string[]
    score_red: number
    score_blue: number
    map_name: string
    map_id: string
}

export const fetchAllPugsWithScores = async (): Promise<PugWithScore[]> => {
    const rows: PugWithScoreRow[] = db.prepare(`
        SELECT p.id, p.team_red, p.team_blue, s.score_red, s.score_blue, m.name AS map_name, m.id AS map_id
        FROM all_pugs p JOIN all_scores s ON p.id = s.pug_id JOIN maps m ON p.map_id = m.id
        WHERE p.finished NOT NULL AND p.rating_change_blue IS NOT NULL AND s.status != '${ScoreStatus.Rejected}'
    `).all() as PugWithScoreRow[]

    return rows.map(row => ({
        id: row.id,
        team_red: row.team_red.split(','),
        team_blue: row.team_blue.split(','),
        score_red: row.score_red,
        score_blue: row.score_blue,
        map_name: row.map_name,
        map_id: row.map_id
    }))
}

export const fetchPugsWithScoresByMapAndPlayers = async (mapName: string, playerIds: string[]): Promise<PugWithScore[]> => {
    const placeholders = playerIds.map(() => '?').join(',')

    const sql = `
        SELECT
            p.id,
            p.team_red,
            p.team_blue,
            s.score_red,
            s.score_blue,
            m.name as map_name,
            m.id as map_id
        FROM all_pugs p
        JOIN all_scores s ON p.id = s.pug_id
        JOIN maps m ON p.map_id = m.id
        WHERE p.finished IS NOT NULL
        AND (p.team_red IN (${placeholders}) OR p.team_blue IN (${placeholders}))
        AND m.name = ?
    `

    const rows = (db.prepare(sql).all([...playerIds, ...playerIds, mapName])) as PugWithScoreRow[]

    return rows.map(row => ({
        id: row.id,
        team_red: row.team_red.split(','),
        team_blue: row.team_blue.split(','),
        score_red: row.score_red,
        score_blue: row.score_blue,
        map_name: row.map_name,
        map_id: row.map_id
    }))
}

export const fetchAllPugsWithScoresSinceDate = async (date: string): Promise<PugWithScore[]> => {
    const rows: PugWithScoreRow[] = db.prepare(`
        SELECT p.id, p.team_red, p.team_blue, s.score_red, s.score_blue, m.name AS map_name, m.id AS map_id
        FROM all_pugs p JOIN all_scores s ON p.id = s.pug_id JOIN maps m ON p.map_id = m.id
        WHERE p.finished NOT NULL AND p.rating_change_blue IS NOT NULL AND s.status != '${ScoreStatus.Rejected}'
        AND p.finished >= ?
    `).all(date) as PugWithScoreRow[]

    return rows.map(row => ({
        id: row.id,
        team_red: row.team_red.split(','),
        team_blue: row.team_blue.split(','),
        score_red: row.score_red,
        score_blue: row.score_blue,
        map_name: row.map_name,
        map_id: row.map_id
    }))
}
