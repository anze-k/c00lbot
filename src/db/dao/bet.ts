import sqlite3 from 'better-sqlite3'
import config from 'config'

import { ScoreStatus } from '../../bot/embeds/utils/constants'
import { BetOdds, BetOption, BetResult } from '../../pug/bet/bet'

const db = sqlite3(config.get('dbConfig.dbName'))

interface TopBetPlayer {
    player_id: string
    coins?: number
    nickname: string
}

interface PlayerBet extends TopBetPlayer {
    amount: number
    bet_on: string
    net: number
    created: string
    pug_id?: string
}

interface BetStats {
    total: number
    amount_total: number
    red_count: number
    blue_count: number
    draw_count: number
    sum_net: number
    avg_net: number
    avg_amount: number
    avg_odds: number
    win_bets: number
}

export interface Bet {
    id: number
    pug_id: number
    player_id: string
    amount: number
    bet_on: BetOption
    net: number
    created: Date
    realised: Date
    odds: number
}

interface Rank {
    rank: number
}

interface Coins {
    coins: number
}

interface PlayerBetInfo {
    nickname: string
    coins: number
}

interface Net {
    net: number
}

//Gets
export const getPlayerBetInfo = async (playerId: string): Promise<PlayerBetInfo> => {
    const playerInfo = await db.prepare(`SELECT nickname, coins FROM players WHERE discord_id = ?`).get(playerId) as PlayerBetInfo | undefined
    if (!playerInfo) {
        throw new Error("Player not found")
    }
    return playerInfo
}

export const doesPlayerHaveMoneyz = async (playerId: string, amount: number): Promise<boolean> => {
    const currentBet = await db.prepare(`SELECT coins FROM players WHERE discord_id = ?`).get(playerId) as PlayerBetInfo | undefined
    if (!currentBet || currentBet.coins < amount) {
        return false
    }
    return true
}

export const checkPugBettingEligibility = async (pugId: number): Promise<boolean> => {
    const eligiblePug = await db.prepare(`SELECT * FROM pugs p JOIN scores s on s.pug_id = p.id
            WHERE p.id = ? AND p.started is not null AND p.finished is null
            AND CURRENT_TIMESTAMP < DATETIME(p.started, '+5 minutes')
            AND (s.status = '${ScoreStatus.Live}' OR s.status = '${ScoreStatus.Manual}')
            AND s.score_red is null
            AND s.score_blue is null`).get(pugId)

    return eligiblePug ? true : false
}

export const getTopBetPlayers = async (limit: number): Promise<TopBetPlayer[]> => {
    return db.prepare(`SELECT DISTINCT p.nickname, p.coins, b.player_id
    FROM players p INNER JOIN bets b ON p.discord_id = b.player_id
    WHERE b.bet_on IS NOT NULL AND b.realised IS NOT NULL
    GROUP BY p.nickname ORDER BY p.coins DESC LIMIT ?,10`).all(limit) as TopBetPlayer[]
}

export const getBetsForPug = async (pugId: number): Promise<PlayerBet[]> => {
    return db.prepare(`SELECT b.amount, b.bet_on, b.net, p.coins, p.nickname, b.created, b.player_id
        FROM bets b
        JOIN players p on p.discord_id = b.player_id
        WHERE b.pug_id = ?`).all(pugId) as PlayerBet[]
}

export const isFirstBet = async (userId: string, pugId: number): Promise<boolean> => {
    return await db.prepare(`SELECT * FROM bets
        WHERE pug_id = ? AND player_id = ?`).get(pugId, userId) ? false : true
}

export const getPlayerBetStats = async (playerId: string): Promise<BetStats> => {
    return await db.prepare(`
        SELECT
            Count(id) as total,
            Sum(amount) as amount_total,
            Count(CASE WHEN bet_on = '${BetOption.Red}' THEN 1 ELSE NULL END) AS red_count,
            Count(CASE WHEN bet_on = '${BetOption.Blue}' THEN 1 ELSE NULL END) AS blue_count,
            Count(CASE WHEN bet_on = '${BetOption.Draw}' THEN 1 ELSE NULL END) AS draw_count,
            Sum(net) as sum_net,
            round(avg(net)) as avg_net,
            round(avg(amount)) as avg_amount,
            100/avg(odds) as avg_odds,
            count(CASE WHEN net > 0 THEN 1 ELSE NULL END) as win_bets
        FROM bets
        WHERE player_id = ? AND realised IS NOT NULL`).get(playerId) as BetStats
}

export const getPlayerLastBet = async (playerId: string): Promise<PlayerBet> => {
    return await db.prepare(`SELECT b.amount, b.bet_on, b.net, p.nickname, b.created, b.pug_id
        FROM bets b
        JOIN players p on p.discord_id = b.player_id
        WHERE b.player_id = ?
        ORDER BY b.created DESC
        LIMIT 1`).get(playerId) as PlayerBet
}

export const selectUnfinishedBetsForPug = async (pugId: number | string, whoWon: BetResult): Promise<Bet[]> => {
    let oddsColumn = ''

    switch (whoWon) {
        case 'RED':
            oddsColumn = 'red_odds'
            break
        case 'BLUE':
            oddsColumn = 'blue_odds'
            break
        default:
            oddsColumn = 'draw_odds'
    }

    return db.prepare(`SELECT b.*, o.${oddsColumn} as odds FROM bets b JOIN pug_odds o on b.pug_id = o.pug_id WHERE b.pug_id = ? AND realised is null AND net is null`).all(pugId) as Bet[]
}

export const getPlayerBetRank = async (discord_id: string): Promise<Rank> => {
    const rank = db.prepare(`SELECT count(DISTINCT(b.player_id))+1 as rank FROM bets b JOIN players p on b.player_id = p.discord_id WHERE b.realised NOT NULL AND p.coins > (SELECT coins FROM players WHERE discord_id = ?)`).get(discord_id) as Rank
    return rank
}

export const getPlayerCoins = async (discord_id: string): Promise<Coins> => {
    const result = db.prepare(`SELECT coins FROM players WHERE discord_id = ?`).get(discord_id) as Coins | undefined
    if (!result) {
        throw new Error("Player not found")
    }
    return result
}

export const getPlayerLastNet = async (playerId: string): Promise<Net> => {
    return await db.prepare(`SELECT SUM(NET) as net FROM BETS WHERE pug_id = (SELECT MAX(pug_id) FROM BETS WHERE player_id = ?)`).get(playerId) as Net
}

//Inserts
export const insertBet = async (playerId: string, pugId: number, amount: number, option: BetOption): Promise<void> => {
    db.prepare(`INSERT INTO bets (pug_id, amount, bet_on, player_id) values (?,?,?,?)`).run(pugId, amount, option, playerId)
}


export const insertOdds = async (pugId: string | number, blue_odds: number, red_odds: number, draw_odds: number): Promise<void> => {
    db.prepare(`INSERT INTO pug_odds (pug_id, blue_odds, red_odds, draw_odds) values (?,?,?,?)`).run(pugId, blue_odds, red_odds, draw_odds)
}

//Updates
export const removeCoinsFromPlayer = async (playerId: string, amount: number): Promise<void> => {
    db.prepare(`UPDATE players set coins = coins - ? WHERE discord_id = ?`).run(Math.round(amount), playerId)
}

export const addCoinsToPlayer = async (playerId: string, amount: number): Promise<void> => {
    db.prepare(`UPDATE players set coins = coins + ? WHERE discord_id = ?`).run(Math.round(amount), playerId)
}

export const finishBet = async (id: number, amount: number): Promise<void> => {
    db.prepare(`UPDATE bets set net = ?, realised = CURRENT_TIMESTAMP WHERE id = ?`).run(Math.round(amount), id)
}

export const calculateBettingOdds = async (pugId: number | string): Promise<BetOdds> => {
    const str = db.prepare(`SELECT str_red, str_blue, map_id FROM all_pugs WHERE id = ?`).get(pugId) as { str_red: number, str_blue: number, map_id: string } | undefined
    if (!str) {
        throw new Error("Pug not found")
    }

    const drawsTotal = db.prepare(`SELECT count(*) as cnt FROM all_scores s JOIN all_pugs p on s.pug_id = p.id WHERE p.map_id = ? AND s.score_blue = s.score_red AND s.status is ?`).get(str.map_id, ScoreStatus.Accepted) as { cnt: number }
    const nonDrawsTotal = db.prepare(`SELECT count(*) as cnt FROM all_scores s JOIN all_pugs p on s.pug_id = p.id WHERE p.map_id = ? AND s.score_blue != s.score_red AND s.status is ?`).get(str.map_id, ScoreStatus.Accepted) as { cnt: number }

    if (drawsTotal.cnt + nonDrawsTotal.cnt < 10 || drawsTotal.cnt === 0 || nonDrawsTotal.cnt === 0) {
        return {
            red: 42,
            blue: 42,
            draw: 16,
        }
    }

    const drawPercentage = Math.round(drawsTotal.cnt / (drawsTotal.cnt + nonDrawsTotal.cnt) * 1000) / 10
    const redOdds = Math.round(str.str_red / (str.str_red + str.str_blue) * 1000) / 10 - drawPercentage / 2
    const blueOdds = 100 - redOdds - drawPercentage

    console.log(drawPercentage, redOdds, blueOdds)

    return {
        red: redOdds,
        blue: blueOdds,
        draw: drawPercentage,
    }
}

export const getDrawOdds = async (mapId: number | string): Promise<number> => {
    const drawsTotal = await db.prepare(`SELECT count(*) as cnt FROM all_scores s JOIN all_pugs p ON s.pug_id = p.id WHERE p.map_id = ? AND s.score_blue = s.score_red AND s.status IS ?`).get(mapId, ScoreStatus.Accepted) as { cnt: number }
    const nonDrawsTotal = await db.prepare(`SELECT count(*) as cnt FROM all_scores s JOIN all_pugs p ON s.pug_id = p.id WHERE p.map_id = ? AND s.score_blue != s.score_red AND s.status IS ?`).get(mapId, ScoreStatus.Accepted) as { cnt: number }

    return drawsTotal.cnt / (nonDrawsTotal.cnt + drawsTotal.cnt)
}
