import sqlite3 from 'better-sqlite3'
import config from 'config'

import { MapStatus, ScoreStatus } from '../../bot/embeds/utils/constants'

const db = sqlite3(config.get('dbConfig.dbName'))

interface MapName {
    name: string
}

interface MapFound extends MapName {
    found: boolean
}

interface MapDeleted extends MapFound {
    deleted: boolean
}

interface MapStatusWeight extends MapName {
    default_weight: number
    status?: string
}

export interface UtMap extends MapStatusWeight {
    id: string
    current_weight: number
}

interface UtMapStats {
    id: string
    name: string
    count: number
    diff: number
    caps: number
}

// Gets
export const getAllMaps = async (): Promise<UtMap[]> => {
    return db.prepare(`SELECT * FROM maps ORDER BY name`).all() as UtMap[]
}

export const getMap = async (map_id: string): Promise<UtMap> => {
    const map: UtMap = await db.prepare(`SELECT * from maps where id = ?`).get(map_id) as UtMap
    return map
}

export const getAllEnabledMaps = async (): Promise<UtMap[]> => {
    return db.prepare(`SELECT * FROM maps WHERE status = ? ORDER BY name`).all(MapStatus.Enabled) as UtMap[]
}

export const getCurrentTop3Weights = async (): Promise<UtMap[]> => {
    return db.prepare(`SELECT * FROM maps WHERE status = ? ORDER BY current_weight DESC LIMIT 3`).all(MapStatus.Enabled) as UtMap[]
}

export const getAllMapStatsWithCount = async (date: string): Promise<UtMapStats[]> => {
    return db.prepare(`
        SELECT count(p.map_id) as count, m.name, round(avg(abs(s.score_blue - s.score_red)), 2) as diff, round(avg(abs(s.score_blue + s.score_red)), 2) as caps
        FROM all_scores s
        JOIN all_pugs p ON p.id = s.pug_id
        JOIN maps m ON m.id = p.map_id
        WHERE s.status != ? AND s.score_blue IS NOT NULL AND p.finished >= ?
        GROUP BY m.name
        ORDER BY count DESC
    `).all(ScoreStatus.Rejected, date) as UtMapStats[]
}

export const getAllTimeMapStatsWithCount = async (): Promise<UtMapStats[]> => {
    return db.prepare(`
        SELECT count(p.map_id) as count, m.name, round(avg(abs(s.score_blue - s.score_red)), 2) as diff, round(avg(abs(s.score_blue + s.score_red)), 2) as caps
        FROM all_scores s
        JOIN all_pugs p ON p.id = s.pug_id
        JOIN maps m ON m.id = p.map_id
        WHERE s.status != ? AND s.score_blue IS NOT NULL
        GROUP BY m.name
        ORDER BY count DESC
    `).all(ScoreStatus.Rejected) as UtMapStats[]
}

export const getSeasonMapStatsWithCount = async (): Promise<UtMapStats[]> => {
    return (db.prepare(`
        SELECT count(p.map_id) as count, m.name, round(avg(abs(s.score_blue - s.score_red)), 2) as diff, round(avg(abs(s.score_blue + s.score_red)), 2) as caps
        FROM scores s
        JOIN pugs p ON p.id = s.pug_id
        JOIN maps m ON m.id = p.map_id
        WHERE NOT s.status = '${ScoreStatus.Rejected}' AND s.score_blue IS NOT NULL
        GROUP BY m.name
        ORDER BY count DESC
    `).all() as UtMapStats[])
}

export const getLastPlayedMaps = async (limit = 3): Promise<UtMap[]> => {
    return db.prepare(`
        SELECT m.* FROM pugs p
        JOIN maps m ON p.map_id = m.id
        JOIN scores s ON s.pug_id = p.id
        WHERE s.status != ? ORDER BY p.started DESC LIMIT ?
    `).all(ScoreStatus.Rejected, limit) as UtMap[]
}

export const getPlayerLastPlayedMaps = async (playerId: string, limit = 3): Promise<UtMap[]> => {
    return db.prepare(`
        SELECT m.* FROM pugs p
        JOIN maps m ON p.map_id = m.id
        JOIN scores s ON s.pug_id = p.id
        WHERE s.status != ? AND p.players LIKE ? ORDER BY p.started DESC LIMIT ?
    `).all(ScoreStatus.Rejected, `%${playerId}%`, limit) as UtMap[]
}

export const getSeasonPlayerMapStats = async (playerDiscordID: string): Promise<UtMapStats[]> => {
    return db.prepare(`
        SELECT count(p.map_id) as count, m.name, round(avg(abs(s.score_blue - s.score_red)), 2) as diff, round(avg(abs(s.score_blue + s.score_red)), 2) as caps
        FROM scores s
        JOIN pugs p ON p.id = s.pug_id
        JOIN maps m ON m.id = p.map_id
        WHERE s.status != ? AND s.score_blue IS NOT NULL AND p.players LIKE ?
        GROUP BY m.name
        ORDER BY count DESC
    `).all(ScoreStatus.Rejected, `%${playerDiscordID}%`) as UtMapStats[]
}

export const getAllPlayerMapStats = async (playerDiscordID: string): Promise<UtMapStats[]> => {
    return db.prepare(`
        SELECT count(p.map_id) as count, m.name, round(avg(abs(s.score_blue - s.score_red)), 2) as diff, round(avg(abs(s.score_blue + s.score_red)), 2) as caps
        FROM all_scores s
        JOIN all_pugs p ON p.id = s.pug_id
        JOIN maps m ON m.id = p.map_id
        WHERE s.status != ? AND s.score_blue IS NOT NULL AND p.players LIKE ?
        GROUP BY m.name
        ORDER BY count DESC
    `).all(ScoreStatus.Rejected, `%${playerDiscordID}%`) as UtMapStats[]
}

//Updates
export const setMapCurrentWeight = async (new_weight: number, name: string): Promise<void> => {
    db.prepare(`UPDATE maps SET current_weight = ? where name = ? AND status = '${MapStatus.Enabled}'`).run(new_weight, name)
}

export const bumpMapWeight = async (weight: number, name: string): Promise<void> => {
    db.prepare(`UPDATE maps SET current_weight = current_weight + ? where name = ? AND status = '${MapStatus.Enabled}'`).run(weight, name)
}

export const setMapDefaultWeight = async (new_weight: number, name: string): Promise<MapFound> => {
    const mapName = await db.prepare(`SELECT name FROM maps WHERE name = ?`).get(name)
    if (mapName === undefined) return { name, found: false }
    db.prepare(`UPDATE maps SET current_weight = ?, default_weight = ? WHERE name = ?`).run(new_weight, new_weight, name)
    return { name, found: true }
}

export const setMapStatus = async (status: string, name: string): Promise<MapFound> => {
    const mapName = await db.prepare(`SELECT name FROM maps WHERE name = ?`).get(name)
    if (mapName === undefined) return { name, found: false }
    db.prepare(`UPDATE maps SET status = ? WHERE name = ?`).run(status, name)
    return { name, found: true }
}

export const resetMapCurrentWeight = async (): Promise<void> => {
    db.prepare(`UPDATE maps SET current_weight = (default_weight)`).run()
}

//Inserts
export const insertMap = async (map: MapStatusWeight): Promise<void> => {
    db.prepare(`INSERT INTO maps (name, default_weight, current_weight, status) VALUES (?, ?, ?, ?)`).run(map.name, map.default_weight, map.default_weight, map.status)
}

//Deletes
export const removeMap = async (name: string): Promise<MapDeleted> => {
    try {
        const mapName = await db.prepare(`SELECT name FROM maps WHERE name = ?`).get(name)
        if (mapName === undefined) return { name, found: false, deleted: false }
        db.prepare(`DELETE FROM maps WHERE name = ?`).run(name)
        return { name, found: true, deleted: true }
    } catch (error) {
        console.log(`Deletion of map ${name} failed`)
        return { name, found: true, deleted: false }
    }
}
