import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface Mode {
    name: string
    team_size: number
}

// Gets
export const getAllModes = async (): Promise<Mode[]> => {
    const modes = db.prepare(`SELECT * FROM modes`).all() as Mode[]
    return Promise.resolve(modes)
}

export const getModeByName = async (name: string): Promise<Mode> => {
    const mode = db.prepare(`SELECT * FROM modes WHERE name = ?`).get(name) as Mode
    return Promise.resolve(mode)
}

export const getModeById = async (id: number): Promise<Mode> => {
    const mode = db.prepare(`SELECT * FROM modes WHERE id = ?`).get(id) as Mode
    return Promise.resolve(mode)
}

// Inserts
export const insertMode = async (mode: Mode): Promise<void> => {
    db.prepare(`INSERT INTO modes (name, team_size) VALUES (?, ?)`).run(mode.name, mode.team_size)
    return Promise.resolve()
}
