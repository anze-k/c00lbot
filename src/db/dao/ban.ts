import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface Ban {
    id?: string
    discord_id: string
    reason: string
    duration: number
    end_time: string
    admin_id: string
    admin_name: string
    status?: string
}

interface ActiveBan extends Ban {
    nickname: string,
}

//Gets
export const getAllCurrentBans = async (): Promise<ActiveBan[]> => {
    const bans = db.prepare(`SELECT b.id, b.discord_id, b.reason, b.duration, b.end_time, p.nickname FROM bans b JOIN players p ON p.discord_id = b.discord_id WHERE status IS NULL`).all() as ActiveBan[]
    return bans
}

export const getAllCurrentBansForUser = async (playerId: string): Promise<Ban[]> => {
    const bans = db.prepare(`SELECT * FROM bans WHERE discord_id = ? AND status IS NULL`).all(playerId) as Ban[]
    return bans
}

export const getAllBansForUser = async (playerId: string): Promise<Ban[]> => {
    const bans = db.prepare(`SELECT * FROM bans WHERE discord_id = ?`).all(playerId) as Ban[]
    return bans
}

export const getLastBanForUser = async (playerId: string): Promise<Ban> => {
    const ban = db.prepare(`SELECT * FROM bans WHERE discord_id = ? ORDER BY id DESC LIMIT 1`).get(playerId) as Ban
    return ban
}

export const getLastBanForUserWithReason = async (playerId: string, reason: string): Promise<Ban> => {
    const ban = db.prepare(`SELECT * FROM bans WHERE discord_id = ? AND reason = ? ORDER BY id DESC LIMIT 1`).get(playerId, reason) as Ban
    return ban
}

//Updates
export const updateBanStatus = async (banId: string, status: string) => {
    db.prepare(`UPDATE bans set status = ? where id = ? `).run(status, banId)
}

export const updateEndDate = async (banId: string, endDate: string) => {
    db.prepare(`UPDATE bans set end_time = ? where id = ? `).run(endDate, banId)
}

//Inserts
export const insertBan = async (ban: Ban): Promise<void> => {
    db.prepare(`INSERT INTO bans (discord_id, reason, duration, end_time, admin_id, admin_name) VALUES (?, ?, ?, ?, ?, ?)`).run(ban.discord_id, ban.reason, ban.duration, ban.end_time, ban.admin_id, ban.admin_name)
}
