import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface ServerPreference {
    server_shortname: string
    player_id: string
    score: -1 | 0 | 1
}

export const getPreferencesForPlayers = async (playerIds: string[]): Promise<ServerPreference[]> => {
    const placeholders = playerIds.map(() => '?').join(',');
    return (db.prepare(`SELECT * from server_preferences where player_id in (${placeholders}) order by server_shortname asc`).all(...playerIds)) as ServerPreference[]
}

export const insertServerPreference = async (playerId: string, serverShortname: string, score: number): Promise<void> => {
    db.prepare(`INSERT INTO server_preferences (player_id, server_shortname, score) VALUES (?, ?, ?)`).run(playerId, serverShortname, score)
}
