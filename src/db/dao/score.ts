import sqlite3 from 'better-sqlite3'
import config from 'config'

import { ScoreStatus } from '../../bot/embeds/utils/constants'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface Score {
    id: string
    pug_id: number
    score_blue: number
    score_red: number
    created: Date
    created_by: string
    status: ScoreStatus
    reactions_yes: string
    reactions_no: string
    reactions_dead: string
    score_msg_id: string
}

export interface ScoreWithServer extends Score {
    shortname: string
}

interface TotalScore {
    total: number
}

//Gets
export const getScoresThatNeedResults = async (): Promise<Score[]> => {
    return (db.prepare(
        `SELECT * FROM scores WHERE status = ?`
    ).all(ScoreStatus.Pending)) as Score[]
}

export const getScoresToFinalize = async (): Promise<Score[]> => {
    const scores = db.prepare(
        `SELECT * FROM scores WHERE status = ? AND CURRENT_TIMESTAMP > DATETIME(created, '+${config.get('scoring.timeoutMinutes')} minutes')`
    ).all(ScoreStatus.Concluded) as Score[]
    return scores
}

export const getUnconcludedPugs = async (): Promise<Score[]> => {
    const scores = db.prepare(
        `SELECT * FROM scores WHERE status = ? OR status = ? OR (status = ? AND created_by = 'bot')`
    ).all(ScoreStatus.Pending, ScoreStatus.Live, ScoreStatus.Manual) as Score[]
    return scores
}

export const getAllTotalCaps = async (): Promise<TotalScore> => {
    const result = await db.prepare(`SELECT sum(score_red + score_blue) as total FROM all_scores`).get() as TotalScore | undefined
    return result || { total: 0 }
}

export const getSeasonTotalCaps = async (): Promise<TotalScore> => {
    const result = await db.prepare(`SELECT sum(score_red + score_blue) as total FROM scores`).get() as TotalScore | undefined
    return result || { total: 0 }
}

export const getLiveScoreForIp = async (ip: string): Promise<ScoreWithServer> => {
    const result = await db.prepare(`
        SELECT sco.*, serv.shortname FROM scores sco
        JOIN pugs p ON sco.pug_id = p.id
        JOIN servers serv ON p.server_id = serv.id
        WHERE sco.status = ? AND serv.ip = ?
    `).get(ScoreStatus.Live, ip) as ScoreWithServer | undefined

    if (!result) {
        throw new Error('Live score not found for the specified IP.')
    }
    return result
}

//Updates
export const updatePugScoreMsgId = async (updatePugScoreMsgId: string, scoreId: number) => {
    db.prepare(`UPDATE scores set score_msg_id = ? where id = ? `).run(updatePugScoreMsgId, scoreId)
}

export const updateScoreStatus = async (scoreId: string, status: ScoreStatus) => {
    db.prepare(`UPDATE scores set status = ? where id = ? `).run(status, scoreId)
}

export const updateScore = async (scoreRed: number, scoreBlue: number, scoreId: string, status: ScoreStatus) => {
    db.prepare(`UPDATE scores set score_red = ?, score_blue = ?, status = ? where id = ? `).run(scoreRed, scoreBlue, status, scoreId)
}

//Inserts
export const insertScore = async (score_red: number, score_blue: number, pug_id: number, created_by: string): Promise<number> => {
    const result = db.prepare(`
        INSERT INTO scores (pug_id, score_red, score_blue, created_by, score_msg_id) VALUES (?, ?, ?, ?, ?)
        RETURNING id
    `).run(pug_id, score_red, score_blue, created_by, '1234')

    const scoreId = Number(result.lastInsertRowid)

    db.prepare(`
        UPDATE pugs SET score_id = ?, finished = CURRENT_TIMESTAMP WHERE id = ?
    `).run(scoreId, pug_id)

    return scoreId
}

export const insertEmptyScore = async (pug_id: string): Promise<string> => {
    return db.prepare(`insert into scores (pug_id, created_by, status) values (?,?,?) returning *`).run(pug_id, 'bot', ScoreStatus.Pending).lastInsertRowid.toString()
}
