import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))
const defaultCoins: number = config.get('features.defaultCoins')

export interface Player {
    id?: string
    discord_id: string
    nickname: string
    rating: number
    rating_change?: number
    created?: Date
    last_played?: Date
    pugs_played?: number
    starting_rating?: number
}

interface LiveTeam {
    players: Player[]
    rating: number
}

export interface SuggestedTeams {
    red: LiveTeam
    blue: LiveTeam
    expert_diff?: number
}

interface RatingAndName {
    rating: number
    nickname: string
}

interface Rank {
    rank: number
}

interface PlayerName {
    nickname: string
}

interface RatingChange {
    nickname: string
    discord_id?: string
    rating: number
    starting_rating: number
    diff: number
}

interface Countable {
    count: number
}

interface PlayerFound {
    discord_id?: string
    nickname?: string
    found: boolean
}

interface SumResult {
    totalRating: number | null
}

//Gets
export const getPlayerById = async (id: string): Promise<Player> => {
    const player = await db.prepare(`SELECT * from players where id = ?`).get(id) as Player
    return player
}

export const getPlayerByDiscordId = async (id: string): Promise<Player> => {
    const player = await db.prepare(`SELECT * from players where discord_id = ?`).get(id) as Player
    return player
}

export const getPlayersByDiscordId = async (id: string[] | string): Promise<Player[]> => {
    const players = db.prepare(`SELECT * from players where discord_id IN (${id})`).all() as Player[]
    return players
}

export const getPlayersStr = async (id: string[]): Promise<number> => {
    const placeholders = id.map(() => '?').join(',')
    const query = `SELECT SUM(rating) as totalRating from players where discord_id IN (${placeholders})`

    const result = await db.prepare(query).get(...id) as SumResult

    return result.totalRating ?? 0
}

export const getPlayerRating = async (discord_id: string): Promise<RatingAndName> => {
    const ratingAndName = await db.prepare(`SELECT rating, nickname from players where discord_id = ?`).get(discord_id) as RatingAndName
    return ratingAndName
}

export const checkIfPlayerRegistered = async (discord_id: string): Promise<boolean> => {
    const player = await db.prepare(`SELECT * from players where discord_id = ? and rating is not null`).get(discord_id)
    return player !== undefined
}

export const getPlayerRank = async (discord_id: string): Promise<Rank> => {
    const rank = await db.prepare(`SELECT count(*)+1 as rank from players where rating_change NOT NULL AND rating > (select rating from players where discord_id = ?)`).get(discord_id) as Rank
    return rank
}

export const getTopPlayers = async (limit: number): Promise<Player[]> => {
    const players = db.prepare(`SELECT * from players where rating_change NOT NULL order by rating desc limit ${limit},10`).all() as Player[]
    return players
}

export const getAllTopPlayers = async (): Promise<Player[]> => {
    const players = db.prepare(`SELECT * from players where rating_change NOT NULL order by rating desc`).all() as Player[]
    return players
}

export const getPlayerName = async (discord_id: string): Promise<PlayerName> => {
    const name = await db.prepare(`SELECT nickname from players where discord_id = ?`).get(discord_id) as PlayerName
    return name
}

export const getAllActivePlayers = async (): Promise<Player[]> => {
    const players = db.prepare(`SELECT * from players where rating_change NOT NULL`).all() as Player[]
    return players
}

export const getAllPlayers = async (): Promise<Player[]> => {
    const players = db.prepare(`SELECT * from players`).all() as Player[]
    return players
}

export const getCountAllActivePlayers = async (): Promise<Countable> => {
    const count = await db.prepare(`SELECT count(*) count FROM players WHERE pugs_played > 0`).get() as Countable
    return count
}

export const getCountSeasonActivePlayers = async (): Promise<Countable> => {
    const count = await db.prepare(`SELECT count(*) count FROM players WHERE rating_change IS NOT NULL`).get() as Countable
    return count
}

export const getPlayerRatingChange = async (discord_id: string): Promise<RatingChange> => {
    const ratingChange = await db.prepare(`SELECT nickname, rating, starting_rating, (rating - starting_rating) as diff FROM players WHERE rating_change NOT NULL AND discord_id = ?`).get(discord_id) as RatingChange
    return ratingChange
}

export const getTopPlayerRatingChange = async (): Promise<RatingChange[]> => {
    const ratingChanges = db.prepare(`SELECT nickname, discord_id, rating, starting_rating, (rating - starting_rating) as diff FROM players WHERE rating_change NOT NULL ORDER BY diff DESC LIMIT 3`).all() as RatingChange[]
    return ratingChanges
}

export const getWorstPlayerRatingChange = async (): Promise<RatingChange[]> => {
    const ratingChanges = db.prepare(`SELECT nickname, discord_id, rating, starting_rating, (rating - starting_rating) as diff FROM players WHERE rating_change NOT NULL ORDER BY diff LIMIT 3`).all() as RatingChange[]
    return ratingChanges
}

export const getStablePlayerRatingChange = async (): Promise<RatingChange[]> => {
    const ratingChanges = db.prepare(`SELECT nickname, discord_id, rating, starting_rating, (rating - starting_rating) as diff FROM players WHERE rating_change NOT NULL AND diff = 0 ORDER BY diff LIMIT 3`).all() as RatingChange[]
    return ratingChanges
}

//Updates
export const setPlayer = async (discord_id: string, rating: number, nickname: string): Promise<PlayerFound> => {
    const playerFound = await db.prepare(`SELECT nickname FROM players WHERE discord_id = ?`).get(discord_id) as PlayerName | undefined
    if (playerFound === undefined) return { found: false }
    db.prepare(`UPDATE players SET nickname = ?, rating = ?, starting_rating = ? WHERE discord_id = ?`).run(nickname, rating, rating, discord_id)
    return { found: true, nickname }
}

export const addCoins = async (discord_id: string, coins: number): Promise<PlayerFound> => {
    const playerFound = await db.prepare(`SELECT nickname FROM players WHERE discord_id = ?`).get(discord_id) as PlayerName | undefined
    if (playerFound === undefined) return { found: false }
    db.prepare(`UPDATE players SET coins = coins + ? WHERE discord_id = ?`).run(coins, discord_id)
    return { found: true }
}


//Inserts
export const insertPlayer = async (player: Player): Promise<void> => {
    db.prepare(`INSERT INTO players (discord_id, nickname, rating, starting_rating, coins) VALUES (?, ?, ?, ?, ?)`)
        .run(player.discord_id, player.nickname, player.rating, player.starting_rating, defaultCoins)
}
