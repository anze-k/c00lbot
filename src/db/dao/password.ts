import sqlite3 from 'better-sqlite3'
import config from 'config'

// import { ScoreStatus } from '../../bot/embeds/utils/constants'
import { Player } from './player'

const db = sqlite3(config.get('dbConfig.dbName'))

export interface Password extends InsertPassword {
    id: number
    active: boolean
    late: boolean
    noshow: boolean
}

export interface InsertPassword {
    pug_id: string
    player_id: string
    team: string
    password: string
}


export const insertPassword = async (pwd: InsertPassword): Promise<void> => {
    db.prepare(`INSERT INTO passwords (pug_id, player_id, team, password) VALUES (@pug_id, @player_id, @team, @password)`).run(pwd)
}

export const getPasswordByPwd = async (pwd: string): Promise<Password> => {
    const password = await db.prepare(`
        SELECT pwd.* FROM players ply
        JOIN passwords pwd ON pwd.player_id = ply.discord_id
        WHERE pwd.password = ? AND pwd.active = 1
    `).get(pwd) as Password | undefined

    if (!password) {
        throw new Error('Password not found or inactive.')
    }

    return password
}

export const getPlayerByPwd = async (pwd: string): Promise<Player> => {
    const player = await db.prepare(`
        SELECT ply.* FROM players ply
        JOIN passwords pwd ON pwd.player_id = ply.discord_id
        WHERE pwd.password = ? AND pwd.active = 1
    `).get(pwd) as Player | undefined

    if (!player) {
        throw new Error('Player not found or inactive.')
    }

    return player
}

export const setAllPlayersLateByPwd = async (gamepwd: string): Promise<void> => {
    const pugId = await getPugIdByPassword(gamepwd)
    await setPlayerLateByPugId(pugId)
}

export const setPlayerLateByPugId = async (pugId: string): Promise<void> => {
    db.prepare(`update passwords set late = 1 where pug_id = ?`).run(parseInt(pugId))
}

export const setPasswordsNotActiveForPug = async (pugId: number) => {
    try {
        db.prepare(`update passwords set active = 0 where pug_id = ?`).run(pugId)
    } catch (e) {
        console.log(e)
    }
}

export const setPlayerNonLate = async (pwd: string): Promise<void> => {
    try {
        const password = await getPasswordByPwd(pwd)
        if (!password) {
            throw new Error('Password not found.')
        }
        db.prepare(`UPDATE passwords SET late = 0 WHERE id = ?`).run(password.id)
    } catch (e) {
        console.error("Failed to set player non-late:", e)
        throw e
    }
}

export const setPlayerLate = async (pwd: string): Promise<void> => {
    try {
        const password = await getPasswordByPwd(pwd)
        if (!password) {
            throw new Error('Password not found.')
        }
        db.prepare(`UPDATE passwords SET late = 1 WHERE id = ?`).run(password.id)
    } catch (e) {
        console.error("Failed to set player late:", e)
        throw e
    }
}

export const setPlayerNoShow = async (pwd: string): Promise<void> => {
    try {
        const password = await getPasswordByPwd(pwd)
        if (!password) {
            throw new Error('Password not found.')
        }
        db.prepare(`UPDATE passwords SET noshow = 1 WHERE id = ?`).run(password.id)
    } catch (e) {
        console.error("Failed to set player no-show:", e)
        throw e
    }
}

export const getLatePlayersByPassword = async (gamepwd: string): Promise<Password[]> => {
    const pugId = await getPugIdByPassword(gamepwd)
    return (db.prepare(`
        SELECT * FROM passwords
        WHERE pug_id = ? AND late = 1 AND noshow = 0
    `).all(parseInt(pugId))) as Password[]
}

export const getNoShowPlayersByPassword = async (gamepwd: string): Promise<Password[]> => {
    const pugId = await getPugIdByPassword(gamepwd)
    return (db.prepare(`
        SELECT * FROM passwords
        WHERE pug_id = ? AND noshow = 1
    `).all(parseInt(pugId))) as Password[]
}

export const getPlayersByPassword = async (gamepwd: string): Promise<Password[]> => {
    const pugId = await getPugIdByPassword(gamepwd)
    return (db.prepare(`
        SELECT * FROM passwords
        WHERE pug_id = ?
    `).all(parseInt(pugId))) as Password[]
}

export const getLatePlayersByPugId = async (pugId: string): Promise<Password[]> => {
    const pugIdInt = parseInt(pugId)
    if (isNaN(pugIdInt)) {
        throw new Error("Invalid Pug ID provided.")
    }
    const passwords = db.prepare(`
        SELECT * FROM passwords WHERE pug_id = ? AND late = 1
    `).all(pugIdInt) as Password[]

    return passwords
}

export const getPugIdByPassword = async (gamepwd: string): Promise<string> => {
    const result = await db.prepare(`
        SELECT sco.pug_id FROM scores sco
        JOIN pugs p ON sco.pug_id = p.id
        JOIN servers serv ON p.server_id = serv.id
        WHERE serv.password = ? AND sco.status = 'LIVE'
    `).get(gamepwd) as { pug_id: string } | undefined

    if (!result) {
        throw new Error('No active pug found with the provided password.')
    }

    return result.pug_id
}

export const getScoreIdByPassword = async (gamepwd: string): Promise<string> => {
    const result = await db.prepare(`
        SELECT sco.id FROM scores sco
        JOIN pugs p ON sco.pug_id = p.id
        JOIN servers serv ON p.server_id = serv.id
        WHERE serv.password = ? AND sco.status = 'LIVE'
    `).get(gamepwd) as { id: string } | undefined

    if (!result) {
        throw new Error('No active score found with the provided password.')
    }

    return result.id
}
