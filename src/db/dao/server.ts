import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

interface ServerName {
    shortname: string
}

interface ServerFound extends ServerName {
    found: boolean
}

interface ServerDeleted extends ServerFound {
    deleted: boolean
}

export interface Server extends ServerName {
    id: string
    ip: string
    port: string
    modes: string
    busy: boolean
    password: string
    country: string
    pug_id?: string
    enabled?: number
}

//Gets
export const getNonBusyServers = async (): Promise<Server[]> => {
    return db.prepare(`SELECT * FROM servers WHERE busy = 0 AND enabled = 1`).all() as Server[]
}

export const getAllServers = async (): Promise<Server[]> => {
    return db.prepare(`SELECT * FROM servers`).all() as Server[]
}

export const getEnabledServers = async (): Promise<Server[]> => {
    return db.prepare(`SELECT * FROM servers where enabled = 1`).all() as Server[]
}


export const getServerOfPug = async (pug_id: number | string): Promise<Server> => {
    const server = db.prepare(`SELECT s.* FROM servers s JOIN all_pugs p ON p.server_id = s.id WHERE p.id = ?`).get(pug_id) as Server | undefined
    if (!server) {
        throw new Error(`Server not found for pug_id: ${pug_id}`)
    }
    return server
}

export const getServerByShortname = async (shortname: string): Promise<Server> => {
    const server = await db.prepare(`SELECT * FROM servers WHERE shortname = ?`).get(shortname) as Server | undefined
    if (!server) {
        throw new Error(`Server not found with shortname: ${shortname}`)
    }
    return server
}

export const getServerByIp = async (ip: string): Promise<Server> => {
    const server = db.prepare(`SELECT * FROM servers WHERE ip = ?`).get(ip) as Server | undefined
    if (!server) {
        throw new Error(`Server not found with IP: ${ip}`)
    }
    return server
}

//Updates
export const setServerFreeId = async (id: string): Promise<void> => {
    db.prepare(`UPDATE servers SET busy = 0 WHERE id = ?`).run(parseInt(id))
}

export const setServerFree = async (shortname: string): Promise<void> => {
    console.log(`[${new Date().getTime()}] Freeing server ${shortname}`)
    db.prepare(`UPDATE servers SET busy = 0 WHERE shortname = ?`).run(shortname)
}

export const setServerBusy = async (shortname: string, pug_id: string, map_id: string, password: string): Promise<void> => {
    const transaction = db.transaction(() => {
        db.prepare(`UPDATE servers SET busy = 1, password = ? WHERE shortname = ?`).run(password, shortname)
        db.prepare(`UPDATE pugs SET map_id = ? WHERE id = ?`).run(map_id, pug_id)
    })
    transaction()
}

export const setServerStatus = async (shortname: string, status: number): Promise<ServerFound> => {
    const serverName = await db.prepare(`SELECT shortname FROM servers WHERE shortname = ?`).get(shortname)
    if (serverName === undefined) return { shortname, found: false }
    db.prepare(`UPDATE servers SET enabled = ? WHERE shortname = ?`).run(status, shortname)
    return { shortname, found: true }
}

export const setServer = async (shortname: string, ip: string, port: string, modes: string, status: number, country: string): Promise<ServerFound> => {
    const serverName = await db.prepare(`SELECT shortname FROM servers WHERE shortname = ?`).get(shortname)
    if (serverName === undefined) return { shortname, found: false }
    db.prepare(`UPDATE servers SET ip = ?, port = ?, modes = ?, enabled = ?, country = ? WHERE shortname = ?`).run(ip, port, modes, status, country, shortname)
    return { shortname, found: true }
}

//Inserts
export const addServer = async (shortname: string, ip: string, port: string, modes: string, status: number, country: string): Promise<void> => {
    db.prepare(`INSERT INTO servers (shortname, ip, port, modes, enabled, country) values (?,?,?,?,?,?)`).run(shortname, ip, port, modes, status, country)
}

//Deletes
export const removeServer = async (shortname: string): Promise<ServerDeleted> => {
    try {
        const serverName = await db.prepare(`SELECT shortname FROM servers WHERE shortname = ?`).get(shortname) as ServerName | undefined
        if (!serverName) {
            return { shortname, found: false, deleted: false }
        }
        db.prepare(`DELETE FROM servers WHERE shortname = ?`).run(shortname)
        return { shortname, found: true, deleted: true }
    } catch (error) {
        console.error(`Deletion of server ${shortname} failed:`, error)
        return { shortname, found: true, deleted: false }
    }
}
