import { scorePug, FinishedScore } from '../../pug/score_pug'
import { generateLeaderboard } from '../../pug/stats/leaderboard'
import { ScoreGame } from '../router'

export const handleMatchEnded = async (score: ScoreGame, ip: string) => {
      if (!score.Teams || !score.Teams.Blue || !score.Teams.Red) {
        return
      }

      console.log(`Score request received. IP: ${ip}, red: ${score.Teams.Red.Score}, blue: ${score.Teams.Blue.Score}`)

      const parsedScore = parseScore(score, ip)

      try {
        await scorePug(parsedScore)
      } catch (e) {
        console.log(e)
      }

      generateLeaderboard()

}

const parseScore = (score: ScoreGame, ip: string): FinishedScore => {
    const red = score.Teams.Red.Score
    const blue = score.Teams.Blue.Score
  
    return { red, blue, ip }
}
  