import { sendEmbed } from '../../bot/bot'
import { getFlagCaptureEmbed } from '../../bot/embeds/user/flag_capture_embed'
import { ScoreStatus } from '../../bot/embeds/utils/constants'
import { getNickname } from '../../bot/embeds/utils/get_nickname'
import { getPlayerByPwd, getScoreIdByPassword } from '../../db/dao/password'
import { updateScore } from '../../db/dao/score'
import { Capture, ServerPlayer } from '../router'

export const handleFlagCapture = async (cap: Capture) => {
    const scoreId = await getScoreIdByPassword(cap.GamePassword)
    await updateScore(cap.Teams.Red.Score, cap.Teams.Blue.Score, scoreId, ScoreStatus.Live)

    if (cap.InstigatorId !== undefined) {
        const player: ServerPlayer = cap.Players.find(p => p.Id === cap.InstigatorId)!
        const dbPlayer = await getPlayerByPwd(player.Password)

        if (dbPlayer) {
            const nickname = await getNickname(dbPlayer.discord_id)
            console.log(`${dbPlayer.nickname} captured for team ${player.Team === 0 ? 'Red' : 'Blue'}! Current score: ${cap.Teams.Red.Score} - ${cap.Teams.Blue.Score}`)

            const embed = await getFlagCaptureEmbed(nickname, player.Team === 0 ? 'RED' : 'BLUE', cap.Teams.Red.Score, cap.Teams.Blue.Score, cap.RemainingTime, cap.Map)
            await sendEmbed(embed)
        }
    }
}
