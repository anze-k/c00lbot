import config from 'config'

import { fetchDiscordUser, sendBanMessage, sendEmbed, sendLogMessage, userToMember } from '../../bot/bot'
import { getBanEmbed } from '../../bot/embeds/admin/ban_embed'
import { getAccumulatedBanDuration } from '../../bot/embeds/utils/ban_valuation'
import { disc_banned_role_id } from '../../config'
import { insertBan } from '../../db/dao/ban'
import { getLatePlayersByPassword, getNoShowPlayersByPassword, getPlayerByPwd, getPugIdByPassword, Password } from '../../db/dao/password'

const whereToBan = 'all'

enum CrimeType {
    LATE = 'late',
    NOSHOW = 'noshow'
}

export const handleMatchStarted = async (gamepwd: string) => {
    const pugId = await getPugIdByPassword(gamepwd)
    await sendLogMessage(`[**#${pugId}**] Match started!`)

    const latePlayers = await getLatePlayersByPassword(gamepwd)
    const noShowPlayers = await getNoShowPlayersByPassword(gamepwd)

    await banPlayers(latePlayers.filter(p => !noShowPlayers.includes(p)), CrimeType.LATE)
    await banPlayers(noShowPlayers, CrimeType.NOSHOW)
}

export const banPlayers = async (latePlayers: Password[], crimeType: CrimeType) => {
    const banReasons = config.get('ban.types') as Array<{ name: string, duration: number }>

    let reasonDetail
    if (crimeType === CrimeType.LATE) {
        reasonDetail = banReasons.find(r => r.name === "Late")
    } else if (crimeType === CrimeType.NOSHOW) {
        reasonDetail = banReasons.find(r => r.name === "No show")
    }

    if (!reasonDetail) {
        throw new Error(`No matching ban reason found for crime type: ${crimeType}`)
    }

    const { name: reason, duration: baseDuration } = reasonDetail

    for (const p of latePlayers) {
        try {
            const player = await getPlayerByPwd(p.password)
            const user = await fetchDiscordUser(player.discord_id)

            if (user) {
                const duration = await getAccumulatedBanDuration(player.discord_id, reason, baseDuration)

                const bannedTill = new Date()
                bannedTill.setHours(bannedTill.getHours() + duration)
                const endTime = bannedTill.toString()

                if (crimeType === CrimeType.LATE) {
                    await sendLogMessage('Banning user **' + user.username + '** for being late')
                } else if (crimeType === CrimeType.NOSHOW) {
                    await sendLogMessage('Banning user **' + user.username + '** for no show')
                }

                const member = await userToMember(user)
                member!.roles.add(disc_banned_role_id)
                await insertBan({ discord_id: player.discord_id, reason, duration, end_time: endTime, admin_id: '69', admin_name: 'c00lbot2' })
                await sendBanMessage(`.ban discord_id:${player.discord_id} reason:${reason} duration:${duration}`)

                const banEmbed = await getBanEmbed(player, reason, duration, endTime, whereToBan)
                await sendEmbed(banEmbed)
            }
        } catch (e) {
            console.log(e)
        }
    }
}
