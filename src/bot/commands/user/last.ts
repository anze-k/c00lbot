import { GuildMember, SlashCommandBuilder } from 'discord.js'

import { getLastPug, getLasttPug, getSetOfFinishedPugsForUser, getSpecificLastPug } from '../../../db/dao/pug'
import { getModeById } from '../../../db/dao/mode'
import { getLastEmbed } from '../../embeds/user/liast/last_embed'
import { pugMode, ScoreStatus } from '../../embeds/utils/constants'
import { CommandHandler } from '../../commands'

export const lastCommand = new SlashCommandBuilder()
    .setName('last')
    .setDescription('Shows previous pugs')
    .addStringOption(option => option.setName('pugid').setDescription('What pug ID?'))
    .addMentionableOption(option => option.setName('player').setDescription('Fetches a players last pug'))
    .addIntegerOption(option => option.setName('increment').setDescription('How many additional pugs ago (1-10)? Works with player as well'))

export const lastCommandHandler: CommandHandler = {
    ...lastCommand,
    handle: async (interaction) => {
        try {
            if (!interaction.isChatInputCommand()) return
            const player = interaction.options.get('player')?.member as GuildMember
            const pugId = interaction.options.get('pugid')?.value as string ?? null
            const quantity = interaction.options.get('increment')?.value as number ?? 0

            // fetch last pug
            if (quantity <= 0 && !player && !pugId) {
                const lastPug = await getLastPug(pugMode)

                if (lastPug) {
                    const lastEmbed = await getLastEmbed(lastPug)
                    await interaction.reply({ embeds: [lastEmbed] })
                } else {
                    await interaction.reply({ ephemeral: true, content: 'No pug played yet! :face_with_monocle:' })
                }

                // fetch last pug with increment
            } else if (quantity <= 10 && !player && !pugId) {
                const getLasttPugs = await getLasttPug(pugMode)

                if (getLasttPugs.length > quantity) {
                    const lasttPug = getLasttPugs[quantity]
                    if (lasttPug) {
                        const lasttEmbed = await getLastEmbed(lasttPug)
                        await interaction.reply({ embeds: [lasttEmbed] })
                    } else {
                        await interaction.reply({ ephemeral: true, content: 'No pug this far back! :scream_cat:' })
                    }
                } else {
                    await interaction.reply({ ephemeral: true, content: 'No pug played yet! :face_with_monocle:' })
                }

                // fetch players last with increment
            } else if ((quantity <= 10 || !quantity) && player && !pugId) {
                const playersLastPugs = await getSetOfFinishedPugsForUser(player.user.id)

                if (playersLastPugs.length === 0) {
                    await interaction.reply({ ephemeral: true, content: 'This player has not played any pugs yet! :grimacing:' })
                } else if (playersLastPugs.length >= quantity) {
                    const noPug = quantity ?? 0
                    const playersSpecificLastPug = await getSpecificLastPug(pugMode, playersLastPugs[noPug].id)
                    const playersLastPugPugEmbed = await getLastEmbed(playersSpecificLastPug)
                    await interaction.reply({ embeds: [playersLastPugPugEmbed] })
                } else {
                    await interaction.reply({ ephemeral: true, content: 'Player has not played that many pugs yet! :exploding_head:' })
                }

                // fetch last pug by id
            } else if (pugId && !quantity && !player) {
                try {
                    const specificLastPug = await getSpecificLastPug(pugMode, pugId);
                    if (specificLastPug.status === `${ScoreStatus.Rejected}`) {
                        const modeName = (await getModeById(pugMode)).name;
                        await interaction.reply({ ephemeral: true, content: `**${modeName}** [#${specificLastPug.id}] pug died! :regional_indicator_f:` });
                    } else {
                        const specificLastPugEmbed = await getLastEmbed(specificLastPug);
                        await interaction.reply({ embeds: [specificLastPugEmbed] });
                    }
                } catch (error) {
                    console.error("Error fetching specific pug:", error);
                    await interaction.reply({ ephemeral: true, content: 'That pug does not exist or could not be retrieved! :face_with_monocle:' });
                }
            } else {
                await interaction.reply({ ephemeral: true, content: 'Follow the bloody instructions' })
            }
        } catch (err) {
            console.log(err)
        }
    }
}
