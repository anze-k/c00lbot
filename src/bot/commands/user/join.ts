import config from 'config'
import { GuildMember, SlashCommandBuilder } from 'discord.js'

import { getAllCurrentBansForUser } from '../../../db/dao/ban'
import { getLivePugForUser } from '../../../db/dao/pug'
import { CommandHandler } from '../../commands'
import { getJoinTimeoutEmbed } from '../../embeds/user/join_timeout_embed'
import { addPlayer } from '../../internal/pug_start'

export const joinCommand = new SlashCommandBuilder()
    .setName('join')
    .setDescription('Lets you join the pug')

export const joinCommandHandler: CommandHandler = {
    ...joinCommand,
    handle: async (interaction) => {
        try {
            await interaction.deferReply()
            const member = interaction.member as GuildMember
            const userId = interaction.user.id

            const bans = await getAllCurrentBansForUser(userId)
            if (bans.length > 0) {
                await interaction.editReply({
                    content: '🚫 You are currently banned and cannot join the pug.',
                });
                return
            }

            const pug = await getLivePugForUser(userId)

            if (pug) {
                const timeLimit = new Date(pug.started)

                timeLimit.setMinutes(timeLimit.getMinutes() + <number>config.get('scoring.timeoutMinutes'))

                if (timeLimit > new Date()) {
                    await interaction.editReply({ embeds: [await getJoinTimeoutEmbed(pug.id, Math.round((timeLimit.getTime() - new Date().getTime()) / 60000).toString())] })
                    return
                }
            }
            await addPlayer(interaction, member)
        } catch (err) {
            console.log(err)
        }
    }
}
