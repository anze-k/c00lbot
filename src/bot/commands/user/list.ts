
import { SlashCommandBuilder } from 'discord.js'

import { CommandHandler } from '../../commands'
import { getListEmbed } from '../../embeds/user/liast/list_embed'

export const listCommand = new SlashCommandBuilder()
    .setName('list')
    .setDescription('Lists current users in the pug')

export const listCommandHandler: CommandHandler = {
    ...listCommand,
    handle: async (interaction) => {
        try {
            const listEmbed = await getListEmbed()
            await interaction.reply({ embeds: [listEmbed] })
        } catch (err) {
            console.log(err)
        }
    }
}
