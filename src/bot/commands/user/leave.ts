
import { SlashCommandBuilder } from 'discord.js'

import { getNickname } from '../../embeds/utils/get_nickname'
import { getCurrentPug, updatePugPlayers } from '../../../db/dao/pug'
import { CommandHandler } from '../../commands'
import { getLeaveEmbed } from '../../embeds/user/leave_embed'
import { pugMode } from '../../embeds/utils/constants'

export const leaveCommand = new SlashCommandBuilder()
    .setName('leave')
    .setDescription('Lets you leave the pug')

export const leaveCommandHandler: CommandHandler = {
    ...leaveCommand,
    handle: async (interaction) => {
        try {
            const userId = interaction.user.id
            const playerName = await getNickname(userId)

            const livePug = await getCurrentPug(pugMode)

            if (!livePug.players.includes(userId)) {
                interaction.reply('not in the pug??')
                return
            }

            const newPlayers = livePug.players.filter(p => p !== userId)
            await updatePugPlayers(newPlayers.join(','), livePug.id!)
            const leaveEmbed = await getLeaveEmbed(playerName)
            await interaction.reply({ embeds: [leaveEmbed] })
        } catch (err) {
            console.log(err)
        }
    }
}
