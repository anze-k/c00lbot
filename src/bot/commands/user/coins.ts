import { AutocompleteInteraction, SlashCommandBuilder } from 'discord.js'

import { getEloBoostEmbed, getEloBoostFullEmbed } from '../../../bot/embeds/user/elo_boost_embed'
import { getMapBoostEmbed } from '../../../bot/embeds/user/map_boost_embed'
import { getNickname } from '../../embeds/utils/get_nickname'
import { doesPlayerHaveMoneyz, removeCoinsFromPlayer } from '../../../db/dao/bet'
import { bumpMapWeight, getAllEnabledMaps } from '../../../db/dao/map'
import { getCurrentPug } from '../../../db/dao/pug'
import { getActiveEloBoost, updatePugForBoost, updateValueForBoost, updateValueForBoostFinish } from '../../../db/dao/pug_boosts'
import { CommandHandler, InteractionHandler } from '../../commands'
import { pugMode } from '../../embeds/utils/constants'

export const coinsCommand = new SlashCommandBuilder()
    .setName('coins')
    .setDescription('Options to use your coins')

coinsCommand.addSubcommand(subcommand =>
    subcommand
        .setName('boostmap')
        .setDescription('Boost a map')
        .addStringOption(option => option.setName('map').setDescription('Map name').setRequired(true).setAutocomplete(true))
        .addIntegerOption(option => option.setName('coins').setDescription('How many coins? (250 coins = 1 weight increase, 50 = min)').setRequired(true).setMinValue(50)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('boostelo')
            .setDescription('Boost elo for next pug')
            .addIntegerOption(option => option.setName('coins').setDescription('How many coins?').setRequired(true).setMinValue(50)))

export const coinsInteractionHandler: InteractionHandler = {
    name: 'coins',
    handleInteraction: async (interaction) => {
        if (!interaction.isAutocomplete()) return
        const autocompleteInteraction = interaction as AutocompleteInteraction

        const focusedValue = autocompleteInteraction.options.get('map')!.value as string
        const subcommand = interaction.options.getSubcommand()

        if (subcommand === 'boostmap') {
            const allMaps = await getAllEnabledMaps()
            const choices = allMaps.map(map => map.name.slice(4))
            const filtered = choices.filter(choice => choice.startsWith(focusedValue) || choice.toLowerCase().startsWith(focusedValue))

            await interaction.respond(
                filtered.map(choice => ({ name: choice, value: choice }))
            )
        }
    }
}

export const coinsCommandHandler: CommandHandler = {
    ...coinsCommand,

    handle: async (interaction) => {
        try {
            if (!interaction.isChatInputCommand()) return

            const playerId = interaction.user.id
            const amount = Math.round(interaction.options.get('coins')!.value as number)

            if (!await doesPlayerHaveMoneyz(playerId, amount)) {
                await interaction.reply({ ephemeral: true, content: `You simply don't have the coins` })
                return
            }

            if (interaction.options.getSubcommand() === 'boostmap') {
                const map = interaction.options.get('map')!.value as string

                if (!((await getAllEnabledMaps()).map(m => m.name)).includes('CTF-' + map)) {
                    await interaction.reply({ ephemeral: true, content: `There is no such map enabled` })
                    return
                }

                const weightBump = Math.round(amount / 250 * 50) / 100
                await bumpMapWeight(weightBump, 'CTF-' + map)
                const playerName = await getNickname(interaction.user.id)
                const mapBoostEmbed = await getMapBoostEmbed(map, weightBump, playerName)

                await removeCoinsFromPlayer(playerId, amount)
                await interaction.reply({ embeds: [mapBoostEmbed] })

            } else if (interaction.options.getSubcommand() === 'boostelo') {
                const currentBoost = await getActiveEloBoost()
                const valueTillBoost = 5000 - currentBoost.value
                const playerName = await getNickname(interaction.user.id)

                if (amount >= valueTillBoost) {
                    // full boost
                    const livePug = await getCurrentPug(pugMode)

                    await updateValueForBoostFinish(currentBoost.id)
                    await updatePugForBoost(parseInt(livePug.id!), currentBoost.id)

                    if (amount > valueTillBoost) {
                        await removeCoinsFromPlayer(playerId, valueTillBoost)
                    } else {
                        await removeCoinsFromPlayer(playerId, amount)
                    }
                    const boostEmbed = await getEloBoostFullEmbed(amount, currentBoost.value + amount, playerName)
                    await interaction.reply({ embeds: [boostEmbed] })
                } else {
                    await updateValueForBoost(amount, currentBoost.id)
                    await removeCoinsFromPlayer(playerId, amount)

                    const boostEmbed = await getEloBoostEmbed(amount, currentBoost.value + amount, playerName)
                    await interaction.reply({ embeds: [boostEmbed] })
                }
            }
        } catch (err) {
            console.log(err)
        }
    }
}
