
import { SlashCommandBuilder } from 'discord.js'

import { CommandHandler } from '../../commands'
import { getLiastEmbed } from '../../embeds/user/liast/liast_embed'

export const liastCommand = new SlashCommandBuilder()
    .setName('liast')
    .setDescription('Lists current users in the pug and the last played pug')

export const liastCommandHandler: CommandHandler = {
    ...liastCommand,
    handle: async (interaction) => {
        try {
            const liastEmbed = await getLiastEmbed()
            await interaction.reply({ embeds: [liastEmbed] })
        } catch (err) {
            console.log(err)
        }
    }
}
