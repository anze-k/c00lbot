
import { SlashCommandBuilder } from 'discord.js'

import { CommandHandler } from '../../commands'
import { getSpectateEmbed } from '../../embeds/user/spectate_embed'

export const spectateCommand = new SlashCommandBuilder()
    .setName('spectate')
    .setDescription('Fetches live pugs with server and password')

export const spectateCommandHandler: CommandHandler = {
    ...spectateCommand,
    handle: async (interaction) => {
        try {
            const spectateEmbed = await getSpectateEmbed()
            await interaction.reply({ ephemeral: true, embeds: [spectateEmbed] })
        } catch (err) {
            console.log(err)
        }
    }
}
