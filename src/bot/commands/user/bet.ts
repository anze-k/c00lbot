
import { SlashCommandBuilder } from 'discord.js'

import { getBetEmbed } from '../../../bot/embeds/user/bets/bet_embed'
import { getNickname } from '../../embeds/utils/get_nickname'
import { getLastPugId } from '../../../db/dao/pug'
import { betOnResult, BetOption } from '../../../pug/bet/bet'
import { CommandHandler } from '../../commands'

export const betCommand = new SlashCommandBuilder()
    .setName('bet')
    .setDescription('Lets you place a bet')
    .addStringOption(option => option.setName('amount').setDescription('How many coins? Use "all" for all-in').setRequired(true))
    .addStringOption(option => option.setName('option').setDescription('What are you betting on?').setRequired(true).addChoices(
        { name: 'Red to win', value: `${BetOption.Red}` },
        { name: 'Blue to win', value: `${BetOption.Blue}` },
        { name: 'Draw', value: `${BetOption.Draw}` }
    ))
    .addIntegerOption(option => option.setName('pugid').setDescription('Pug ID defaults to last pug'))
    .addBooleanOption(option => option.setName('hidden').setDescription('Hide command'))

export const betCommandHandler: CommandHandler = {
    ...betCommand,
    handle: async (interaction) => {
        try {
            if (!interaction.isChatInputCommand()) return
            const hidden = interaction.options.getBoolean('hidden') ?? false

            const playerId = interaction.user.id
            const amount = interaction.options.get('amount')!.value as string
            const option = interaction.options.get('option')!.value as BetOption
            const pugId = interaction.options.get('pugid')?.value as number ?? (await getLastPugId()).id
            const bet = await betOnResult(playerId, pugId, amount, option)
            if (bet.success) {
                const playerName = await getNickname(playerId)
                const betEmbed = await getBetEmbed(playerName, bet.amount, option, pugId)
                await interaction.reply({ ephemeral: hidden, embeds: [betEmbed] })
            } else {
                await interaction.reply({ ephemeral: true, content: `${bet.msg}` })
            }

        } catch (err) {
            console.log(err)
        }
    }
}
