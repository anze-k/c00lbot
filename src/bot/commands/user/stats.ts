import { APIApplicationCommandOptionChoice, GuildMember, SlashCommandBuilder, SlashCommandSubcommandBuilder } from 'discord.js'
import { getPlayerPugCountEmbed } from '../../../bot/embeds/user/stats/player_pug_count_embed'

import { getPlayerMapsEmbed } from '../../../bot/embeds/user/stats/player_maps_embed'
import { SeasonChoice } from '../../../bot/embeds/utils/constants'
import { getEnemyWinrateAll, getEnemyWinrateSeason } from '../../../db/dao/pug'
import { generateProgressImageForUser } from '../../../pug/stats/progress'
import { generatePugsCountGraph, PugCountChartMode, PugCountError } from '../../../pug/stats/pugs_count'
import { calculateWinrate } from '../../../pug/stats/winrate'
import { CommandHandler } from '../../commands'
import { getBestPugsEmbed } from '../../embeds/user/stats/best_pugs_embed'
import { getLeaderboardEmbed } from '../../embeds/user/stats/leaderboard_embed'
import { getMapStatsEmbed } from '../../embeds/user/stats/map_stats_embed'
import { getCombinedPerfEmbed, getPerfEmbed } from '../../embeds/user/stats/perf_embed'
import { getPugStatsEmbed } from '../../embeds/user/stats/pug_stats_embed'
import { getRatingTopEmbed } from '../../embeds/user/stats/rating_top_embed'
import { getWorstPugsEmbed } from '../../embeds/user/stats/worst_pugs_embed'
import { getNickname } from '../../../bot/embeds/utils/get_nickname'
import { generateMapWinrateGraphPerPlayer } from '../../../pug/stats/charts/generate_mapwinrate_player'
import { generateTeammateWinrate } from '../../../pug/stats/charts/generate_teammatewinrate_player'
import { generateMapWinrateGraphPerMap } from '../../../pug/stats/charts/generate_map_bestplayers'
import { getAllEnabledMaps } from '../../../db/dao/map'
import { PlayerMapWinRateChartMode, PlayerMapWinrateSorting } from "../../../pug/stats/charts/generate_chart_common";
import { getPlayerPugStats, PlayerPugsInvalidSinceDateError } from "../../embeds/user/stats/player_pugs_stats_embed";

export const statsCommand = new SlashCommandBuilder()
    .setName('stats')
    .setDescription('Shows various stats')

statsCommand
    .addSubcommand(subcommand =>
        subcommand
            .setName('compareprogress')
            .setDescription('Compares progress')
            .addMentionableOption(option => option.setName('player1').setDescription('Player to compare to').setRequired(true))
            .addMentionableOption(option => option.setName('player2').setDescription('Player to compare to'))
            .addMentionableOption(option => option.setName('player3').setDescription('Player to compare to'))
            .addMentionableOption(option => option.setName('player4').setDescription('Player to compare to'))
            .addIntegerOption(option => option.setName('history').setDescription('How many pugs back? Default: 20  Max: 40'))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('myprogress')
            .setDescription('Your progress')
            .addIntegerOption(option => option.setName('history').setDescription('How many pugs back? Default: all pugs'))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command'))
    )

    .addSubcommand(subcommand =>
        subcommand
            .setName('pugcount')
            .setDescription('Graph of pugs played')
            .addStringOption(option => option.setName('type').setDescription('All time chart or 30 days since specified date. Default: All time').addChoices(
                { name: 'All time', value: PugCountChartMode.AllTime },
                { name: '30 days since date', value: PugCountChartMode.Days30 }
            ))
            .addMentionableOption(option => option.setName('player').setDescription('Filter by player'))
            .addStringOption(option => option.setName('sincedate').setDescription('Since what date (use YYYY-MM-DD).'))
    )


    .addSubcommand(subcommand =>
        subcommand
            .setName('leaderboard')
            .setDescription('Displays leaderboard for span of players')
            .addIntegerOption(option => option.setName('quantity').setDescription('Limit').setRequired(true).addChoices(
                { name: '1-10', value: 0 },
                { name: '11-20', value: 10 },
                { name: '21-30', value: 20 },
                { name: '31-40', value: 30 },
                { name: '41-50', value: 40 },
                { name: '51-60', value: 50 },
                { name: '61-70', value: 60 },
                { name: '71-80', value: 70 },
                { name: '81-90', value: 80 },
                { name: '91-100', value: 90 }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('bestpugs')
            .setDescription('Displays strongest pugs')
            .addStringOption(option => option.setName('season').setDescription('When? Default: This season').addChoices(
                { name: 'All time', value: `${SeasonChoice.All}` },
                { name: 'This season', value: `${SeasonChoice.Current}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('performance')
            .setDescription('Player performance stats')
            .addMentionableOption(option => option.setName('player').setDescription('Player'))
            .addStringOption(option => option.setName('season').setDescription('When? Default: This season').addChoices(
                { name: 'All time', value: `${SeasonChoice.All}` },
                { name: 'This season', value: `${SeasonChoice.Current}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('performancecombined')
            .setDescription('Performance of 2 players stats')
            .addMentionableOption(option => option.setName('player1').setDescription('Player').setRequired(true))
            .addMentionableOption(option => option.setName('player2').setDescription('Player').setRequired(true))
            .addStringOption(option => option.setName('season').setDescription('When? Default: This season').addChoices(
                { name: 'All time', value: `${SeasonChoice.All}` },
                { name: 'This season', value: `${SeasonChoice.Current}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('ratingtop')
            .setDescription('Shows the most spectacular rating changes')
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('pugs')
            .setDescription('Shows stats for the pug')
            .addStringOption(option => option.setName('when').setDescription('Todays pugs or all time stats').setRequired(true).addChoices(
                { name: 'Today', value: 'today' },
                { name: 'This Season', value: `${SeasonChoice.Current}` },
                { name: 'All time', value: `${SeasonChoice.All}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('maps')
            .setDescription('Shows the map stats within a time span with avg caps and avg cap difference')
            .addStringOption(option => option.setName('time').setDescription('When?').setRequired(true).addChoices(
                { name: 'Today', value: 'today' },
                { name: 'Yesterday', value: 'yesterday' },
                { name: 'Three days', value: 'three' },
                { name: 'This week', value: 'week' },
                { name: 'This month', value: 'month' },
                { name: 'This year', value: 'year' },
                { name: 'This season', value: `${SeasonChoice.Current}` },
                { name: 'All time', value: `${SeasonChoice.All}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('worstpugs')
            .setDescription('Like best pugs but worse')
            .addStringOption(option => option.setName('season').setDescription('When? Default: This season').addChoices(
                { name: 'All time', value: `${SeasonChoice.All}` },
                { name: 'This season', value: `${SeasonChoice.Current}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('playermapsplayed')
            .setDescription('Map stats for specific player')
            .addMentionableOption(option => option.setName('player').setDescription('Player'))
            .addStringOption(option => option.setName('season').setDescription('When? Default: This season').addChoices(
                { name: 'This season', value: `${SeasonChoice.Current}` },
                { name: 'All time', value: `${SeasonChoice.All}` }
            ))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')))

    .addSubcommand(subcommand =>
        subcommand
            .setName('playerpugstotal')
            .setDescription('Shows how many pugs a player has played since date')
            .addStringOption(option => option.setName('sincedate').setDescription('Since what date (use YYYY-MM-DD)').setRequired(true))
            .addNumberOption(option => option.setName('minimumplayed').setDescription('Default: 4'))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command'))
    )

    .addSubcommand(subcommand =>
        subcommand
            .setName('playerpugs')
            .setDescription('Shows how many pugs a player has played since date')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addNumberOption(option => option.setName('history').setDescription('min: 1, max: 30'))
            .addStringOption(option => option.setName('beforedate').setDescription('Before what date (use YYYY-MM-DD)'))
    )

    .addSubcommand(subcommand =>
        subcommand
            .setName('playerteammates')
            .setDescription('How well does a player do with other players')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addStringOption(option => option.setName('datatype').setDescription('Show winrate % or game count')
                .addChoices({ name: 'Map Count', value: PlayerMapWinRateChartMode.MapCount }, { name: 'Percent', value: PlayerMapWinRateChartMode.Percent }))
            .addStringOption(option => option.setName('sorting').setDescription('Criteria to sort by')
                .addChoices(
                    { name: 'Win', value: PlayerMapWinrateSorting.Win },
                    { name: 'Draw', value: PlayerMapWinrateSorting.Draw },
                    { name: 'Loss', value: PlayerMapWinrateSorting.Loss },
                    { name: 'Total', value: PlayerMapWinrateSorting.Total },
                    { name: 'Win (Group by rank)', value: PlayerMapWinrateSorting.GroupByElo },
                ))
            .addNumberOption(option => option.setName('minimumplayed').setDescription('Default: 20. Minimal value is 10'))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command'))
    )

    .addSubcommand(subcommand =>
        subcommand
            .setName('playermaps')
            .setDescription('How well does a player do on certain maps')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addStringOption(option => option.setName('datatype').setDescription('Show winrate % or game count')
                .addChoices({ name: 'Map Count', value: PlayerMapWinRateChartMode.MapCount }, { name: 'Percent', value: PlayerMapWinRateChartMode.Percent }))
            .addStringOption(option => option.setName('sorting').setDescription('Criteria to sort by')
                .addChoices(
                    { name: 'Win', value: PlayerMapWinrateSorting.Win },
                    { name: 'Draw', value: PlayerMapWinrateSorting.Draw },
                    { name: 'Loss', value: PlayerMapWinrateSorting.Loss },
                    { name: 'Total', value: PlayerMapWinrateSorting.Total },
                ))
            .addNumberOption(option => option.setName('minimumplayed').setDescription('Default: 5'))
            .addBooleanOption(option => option.setName('hidden').setDescription('Hide command'))
    )


    .addSubcommand(subcommand =>
        subcommand
            .setName('mapbestplayers')
            .setDescription('Who is the king of the map??'))


export const statsCommandHandler: CommandHandler = {
    ...statsCommand,
    handle: async (interaction) => {
        try {
            if (!interaction.isChatInputCommand()) return
            const season = interaction.options.get('season')?.value as SeasonChoice ?? SeasonChoice.Current
            const hidden = interaction.options.getBoolean('hidden') ?? false

            if (interaction.options.getSubcommand() === 'compareprogress') {
                await interaction.deferReply({ ephemeral: hidden })

                const ids = []
                const player1 = interaction.options.get('player1')?.member as GuildMember
                const player2 = interaction.options.get('player2')?.member as GuildMember
                const player3 = interaction.options.get('player3')?.member as GuildMember
                const player4 = interaction.options.get('player4')?.member as GuildMember

                if (player1) ids.push(player1.id)
                if (player2) ids.push(player2.id)
                if (player3) ids.push(player3.id)
                if (player4) ids.push(player4.id)

                const limit = interaction.options.get('history')?.value as number | undefined

                const image = await generateProgressImageForUser(ids, limit)
                await interaction.editReply({ files: [{ attachment: image }] })
            }

            else if (interaction.options.getSubcommand() === 'myprogress') {
                await interaction.deferReply({ ephemeral: hidden })

                const limit = interaction.options.get('history')?.value as number | undefined
                const image = await generateProgressImageForUser([interaction.user.id], limit)

                await interaction.editReply({ files: [{ attachment: image }] })
            }

            else if (interaction.options.getSubcommand() === 'pugcount') {
                const type = interaction.options.get('type')?.value as PugCountChartMode ?? PugCountChartMode.AllTime;
                const from = interaction.options.get('sincedate')?.value as string
                const player = interaction.options.get('player')?.member as GuildMember
                const mapName = interaction.options.get('mapname')?.value as string

                try {
                    const image = await generatePugsCountGraph({
                        type,
                        from,
                        map: mapName,
                        player: await (player ? getNickname(player.id) : Promise.resolve(player)),
                        discordId: player?.id
                    });

                    await interaction.deferReply({ ephemeral: hidden });
                    await interaction.editReply({ files: [{ attachment: image }] })
                } catch (e) {
                    if (e instanceof PugCountError) {
                        await interaction.reply({ ephemeral: hidden, content: e.message });
                        return;
                    } else {
                        await interaction.reply({ ephemeral: hidden, content: 'Congrats, you broke the bot' });
                        throw e;
                    }
                }
            }

            else if (interaction.options.getSubcommand() === 'leaderboard') {
                const leaderboardEmbed = await getLeaderboardEmbed(interaction.options.get('quantity')!.value as number)
                await interaction.reply({ ephemeral: hidden, embeds: [leaderboardEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'bestpugs') {
                await interaction.deferReply({ ephemeral: hidden })
                const bestPugsEmbed = await getBestPugsEmbed(season)
                await interaction.editReply({ embeds: [bestPugsEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'performance') {
                const player = interaction.options.get('player')?.member as GuildMember ?? interaction.member as GuildMember

                const winrate = await calculateWinrate(season, player.id)
                const winrateEmbed = await getPerfEmbed(winrate, player.id)
                await interaction.reply({ ephemeral: hidden, embeds: [winrateEmbed] })
                return
            }

            else if (interaction.options.getSubcommand() === 'performancecombined') {
                const player1 = interaction.options.get('player1')!.member as GuildMember
                const player2 = interaction.options.get('player2')!.member as GuildMember

                if (player1 === player2) {
                    await interaction.reply({ ephemeral: hidden, content: 'Wow you found a bug, honestly, you did' })
                    return
                }

                if (player1 && player2) {
                    let enemyWinrate
                    const winrate = await calculateWinrate(season, player1.id, player2.id)
                    if (season === SeasonChoice.Current) {
                        enemyWinrate = await getEnemyWinrateSeason(player1.id, player2.id)
                    } else {
                        enemyWinrate = await getEnemyWinrateAll(player1.id, player2.id)
                    }
                    const combinedPerfEmbed = await getCombinedPerfEmbed(winrate, enemyWinrate, player1, player2)
                    await interaction.reply({ ephemeral: hidden, embeds: [combinedPerfEmbed] })
                    return
                }
            }

            else if (interaction.options.getSubcommand() === 'ratingtop') {
                const ratingTopEmbed = await getRatingTopEmbed()
                await interaction.reply({ ephemeral: hidden, embeds: [ratingTopEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'pugs') {
                const pugStatsEmbed = await getPugStatsEmbed(interaction.options.get('when')!.value as SeasonChoice)
                await interaction.reply({ ephemeral: hidden, embeds: [pugStatsEmbed] })
            }

            else if (interaction.options.getSubcommand() === 'maps') {
                const mapStatsEmbed = await getMapStatsEmbed(interaction.options.get('time')!.value as string)
                await interaction.reply({ ephemeral: hidden, embeds: [mapStatsEmbed] })
            }
            else if (interaction.options.getSubcommand() === 'worstpugs') {
                const worstPugsEmbed = await getWorstPugsEmbed(season)
                await interaction.reply({ ephemeral: hidden, embeds: [worstPugsEmbed] })
            }
            else if (interaction.options.getSubcommand() === 'playermapsplayed') {
                const player = interaction.options.get('player')?.member as GuildMember ?? interaction.member as GuildMember
                const playerMapsEmbed = await getPlayerMapsEmbed(season, player.id)
                await interaction.reply({ ephemeral: hidden, embeds: [playerMapsEmbed] })
            } else if (interaction.options.getSubcommand() === 'playerpugs') {
                const player = interaction.options.get('player')?.member as GuildMember ?? interaction.member as GuildMember
                const beforedate = interaction.options.get('beforedate')?.value as string;
                const limit = interaction.options.get('history')?.value as number | undefined
                const mapName = interaction.options.get('mapname')?.value as string

                try {
                    const playerPugsEmbed = await getPlayerPugStats({
                        limit,
                        playerId: player.id,
                        map: mapName,
                        referenceDate: beforedate
                    });

                    await interaction.reply({ ephemeral: hidden, embeds: [playerPugsEmbed] });
                } catch (e) {
                    if (e instanceof PlayerPugsInvalidSinceDateError) {
                        await interaction.reply({ content: e.message, ephemeral: hidden })
                    } else {
                        await interaction.reply({ content: 'An unknown error occurred', ephemeral: hidden })
                    }
                }
            } else if (interaction.options.getSubcommand() === 'playerpugstotal') {
                const sinceDate = interaction.options.get('sincedate')!.value as string
                const minimum = interaction.options.get('minimumplayed')?.value as number ?? undefined

                try {
                    if (sinceDate.length !== 10) throw new Error()
                    if (sinceDate.split('-').length !== 3) throw new Error()
                    const date = Date.parse(sinceDate)
                    if (isNaN(date)) throw new Error()

                    const playerMapsEmbed = await getPlayerPugCountEmbed(sinceDate, minimum)
                    await interaction.reply({ ephemeral: hidden, embeds: [playerMapsEmbed] })

                } catch (err) {
                    await interaction.reply({ content: 'Date is of wrong format, use YYYY-MM-DD', ephemeral: hidden })
                }

            }
            else if (interaction.options.getSubcommand() === 'playermaps') {
                await interaction.deferReply({ ephemeral: hidden })

                const player = interaction.options.get('player')!.member as GuildMember
                const datatype = interaction.options.get('datatype')?.value as PlayerMapWinRateChartMode ?? PlayerMapWinRateChartMode.Percent;
                const minimumplayed = interaction.options.get('minimumplayed')?.value as number ?? 5;
                const sorting = interaction.options.get('sorting')?.value as PlayerMapWinrateSorting ?? PlayerMapWinrateSorting.Win;

                const nickname = await getNickname(player.id)
                const image = await generateMapWinrateGraphPerPlayer(nickname, datatype, minimumplayed, sorting);

                await interaction.editReply({ files: [{ attachment: image }] })
            }
            else if (interaction.options.getSubcommand() === 'playerteammates') {
                await interaction.deferReply({ ephemeral: hidden })

                const player = interaction.options.get('player')!.member as GuildMember
                const datatype = interaction.options.get('datatype')?.value as PlayerMapWinRateChartMode ?? PlayerMapWinRateChartMode.Percent;
                const minimumplayed = interaction.options.get('minimumplayed')?.value as number ?? 20;
                const sorting = interaction.options.get('sorting')?.value as PlayerMapWinrateSorting ?? PlayerMapWinrateSorting.Win;

                const nickname = await getNickname(player.id)
                const image = await generateTeammateWinrate(nickname, datatype, minimumplayed, sorting)

                await interaction.editReply({ files: [{ attachment: image }] })
            }
            else if (interaction.options.getSubcommand() === 'mapbestplayers') {
                await interaction.deferReply({ ephemeral: hidden })
                const mapName = interaction.options.get('mapname')!.value as string
                const datatype = interaction.options.get('datatype')?.value as PlayerMapWinRateChartMode ?? PlayerMapWinRateChartMode.Percent;
                const minimumplayed = interaction.options.get('minimumplayed')?.value as number ?? 20;
                const sorting = interaction.options.get('sorting')?.value as PlayerMapWinrateSorting ?? PlayerMapWinrateSorting.Win;

                const image = await generateMapWinrateGraphPerMap(mapName, datatype, minimumplayed, sorting)

                await interaction.editReply({ files: [{ attachment: image }] })
            }
        } catch (err) {
            console.log(err)
        }
    }
}

async function loadMapsForStats() {
    const maps = await getAllEnabledMaps();
    const choices: APIApplicationCommandOptionChoice<string> | { name: string; value: string }[] = []
    for (const map of maps) {
        choices.push({ name: map.name, value: map.name })
    }

    const mapBestSubcommand = statsCommand.options.find(o => o.toJSON().name == 'mapbestplayers')! as SlashCommandSubcommandBuilder
    mapBestSubcommand.addStringOption(
        option => option
            .setName('mapname').setDescription('Map Name').setRequired(true).addChoices(choices))
        .addStringOption(option => option.setName('datatype').setDescription('Show winrate % or game count')
            .addChoices({ name: 'Map Count', value: PlayerMapWinRateChartMode.MapCount }, { name: 'Percent', value: PlayerMapWinRateChartMode.Percent }))
        .addStringOption(option => option.setName('sorting').setDescription('Criteria to sort by')
            .addChoices(
                { name: 'Win', value: PlayerMapWinrateSorting.Win },
                { name: 'Draw', value: PlayerMapWinrateSorting.Draw },
                { name: 'Loss', value: PlayerMapWinrateSorting.Loss },
                { name: 'Total', value: PlayerMapWinrateSorting.Total },
                { name: 'Win (Group by rank)', value: PlayerMapWinrateSorting.GroupByElo }
            ))
        .addNumberOption(option => option.setName('minimumplayed').setDescription('Default: 20'))
        .addBooleanOption(option => option.setName('hidden').setDescription('Hide command')
        );

    const pugCountSubcommand = statsCommand.options.find(o => o.toJSON().name == 'pugcount')! as SlashCommandSubcommandBuilder
    pugCountSubcommand
        .addStringOption(option => option.setName('mapname').setDescription('Map Name').addChoices(choices))
        .addBooleanOption(option => option.setName('hidden').setDescription('Hide command'))

    const playerPugsSubcommand = statsCommand.options.find(o => o.toJSON().name == 'playerpugs')! as SlashCommandSubcommandBuilder
    playerPugsSubcommand
        .addStringOption(option => option.setName('mapname').setDescription('Map Name').addChoices(choices))
        .addBooleanOption(option => option.setName('hidden').setDescription('Hide command'))

}

loadMapsForStats()