import { CommandInteraction, SlashCommandBuilder } from 'discord.js'

import { getAllMapListEmbed } from '../../embeds/admin/all_map_list_embed'
import { insertMap, removeMap, resetMapCurrentWeight, setMapDefaultWeight, setMapStatus } from '../../../db/dao/map'
import { CommandHandler } from '../../commands'

export const adminMapCommand = new SlashCommandBuilder()
    .setName('adminmap')
    .setDescription('Admin commands for maps')

adminMapCommand
    .addSubcommand(subcommand =>
        subcommand
            .setName('add')
            .setDescription('Adds a map to the pug')
            .addStringOption(option => option.setName('name').setDescription('Map name (example: CTF-Face)').setRequired(true))
            .addIntegerOption(option => option.setName('weight').setDescription('Map weight (from 1 to x)').setRequired(true))
            .addStringOption(option => option.setName('status').setDescription('Status').setRequired(true).addChoices(
                { name: 'Enabled', value: 'ENABLED' },
                { name: 'Disabled', value: 'DISABLED' }
            )))
    .addSubcommand(subcommand =>
        subcommand
            .setName('changestatus')
            .setDescription('Change status of map')
            .addStringOption(option => option.setName('name').setDescription('Map name (example: CTF-Face)').setRequired(true))
            .addStringOption(option => option.setName('status').setDescription('Status').setRequired(true).addChoices(
                { name: 'Enabled', value: 'ENABLED' },
                { name: 'Disabled', value: 'DISABLED' }
            )))
    .addSubcommand(subcommand =>
        subcommand
            .setName('changeweight')
            .setDescription('Change default weight of a map')
            .addStringOption(option => option.setName('name').setDescription('Map name (example: CTF-Face)').setRequired(true))
            .addIntegerOption(option => option.setName('weight').setDescription('Weight of the map (from 1 to x)').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('list')
            .setDescription('List all maps, with the status and default weight'))
    .addSubcommand(subcommand =>
        subcommand
            .setName('remove')
            .setDescription('Removes one map (can not remove map that has been played, change status to disabled instead)')
            .addStringOption(option => option.setName('name').setDescription('Map name (example: CTF-Face)').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('resetcurrentweight')
            .setDescription('Reset all maps current weight'))

export const adminMapCommandHandler: CommandHandler = {
    ...adminMapCommand,
    handle: async (interaction: CommandInteraction) => {
        try {
            if (!interaction.isChatInputCommand()) return

            const subcommand = interaction.options.getSubcommand()

            if (subcommand === 'add') {
                const name = interaction.options.get('name')!.value as string
                const weight = interaction.options.get('weight')!.value as number
                const status = interaction.options.get('status')!.value as string
                await insertMap({ name, default_weight: weight, status })
                await interaction.reply({ ephemeral: true, content: `**${name}** added with weight **${weight}** and has the status **${status}**` })
            }

            else if (subcommand === 'changestatus') {
                const name = interaction.options.get('name')!.value as string
                const status = interaction.options.get('status')!.value as string
                const map = await setMapStatus(status, name)
                if (map.found === true) {
                    await interaction.reply({ ephemeral: true, content: `**${name}** updated to **${status}**` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${name}** does not exist, check spelling or add the map first` })
                }
            }

            else if (subcommand === 'changeweight') {
                const name = interaction.options.get('name')!.value as string
                const weight = interaction.options.get('weight')!.value as number
                const map = await setMapDefaultWeight(weight, name)
                if (map.found === true) {
                    await interaction.reply({ ephemeral: true, content: `**${name}** weight set to **${weight}**` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${name}** does not exist, check spelling or add the map first` })
                }
            }

            else if (subcommand === 'list') {
                const listAllMapEmbed = await getAllMapListEmbed()
                await interaction.reply({ ephemeral: true, embeds: [listAllMapEmbed] })
            }

            else if (subcommand === 'remove') {
                const name = interaction.options.get('name')!.value as string
                const map = await removeMap(name)
                if (map.found === false) {
                    await interaction.reply({ ephemeral: true, content: `**${name}** does not exist, check spelling and version` })
                } else if (map.deleted === false) {
                    await interaction.reply({ ephemeral: true, content: `**${name}** could not be removed, it has already been played! Change status instead.` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${name}** removed` })
                }
            }

            else if (subcommand === 'resetcurrentweight') {
                await resetMapCurrentWeight()
                await interaction.reply({ ephemeral: true, content: `All current weights reset to the default weight` })
            }
        } catch (err) {
            console.log(err)
        }
    }
}
