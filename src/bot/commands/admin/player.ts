import config from 'config'
import { CommandInteraction, GuildMember, SlashCommandBuilder } from 'discord.js'

import { getNickname } from '../../embeds/utils/get_nickname'
import { disc_banned_role_id } from '../../../config'
import { getAllCurrentBansForUser, insertBan, updateEndDate } from '../../../db/dao/ban'
import { addCoins, insertPlayer, setPlayer } from '../../../db/dao/player'
import { getCurrentPug, updatePugPlayers } from '../../../db/dao/pug'
import { sendBanMessage } from '../../bot'
import { CommandHandler } from '../../commands'
import { getBanEmbed } from '../../embeds/admin/ban_embed'
import { getLeaveEmbed } from '../../embeds/user/leave_embed'
import { getAccumulatedBanDuration } from '../../embeds/utils/ban_valuation'
import { pugMode } from '../../embeds/utils/constants'
import { addPlayer } from '../../internal/pug_start'

const banReasons = config.get('ban.types') as Array<{ name: string, duration: number }>

export const adminPlayerCommand = new SlashCommandBuilder()
    .setName('adminplayer')
    .setDescription('Admin commands for player')

adminPlayerCommand
    .addSubcommand(subcommand =>
        subcommand
            .setName('add')
            .setDescription('Adds player to the pug')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('remove')
            .setDescription('Removes player from the pug')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('ban')
            .setDescription('Bans a player from the pug for a fixed reason & duration')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addStringOption(option => option.setName('reason').setDescription('Reason').setRequired(true).addChoices(
                ...banReasons.map(reason => ({
                    name: reason.name, value: reason.name
                }))
            )))
    .addSubcommand(subcommand =>
        subcommand
            .setName('customban')
            .setDescription('Create a custom ban')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addStringOption(option => option.setName('reason').setDescription('Reason').setRequired(true))
            .addIntegerOption(option => option.setName('duration').setDescription('Duration in hours').setRequired(true))
            .addStringOption(option => option.setName('where').setDescription('Where do you want to ban?').setRequired(true).addChoices(
                { name: 'All', value: 'all' },
                { name: 'Ranked pugs', value: 'ranked' },
                { name: 'Chill pugs', value: 'chill' },
            )))
    .addSubcommand(subcommand =>
        subcommand
            .setName('unban')
            .setDescription('Unbans a player from the pug')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addStringOption(option => option.setName('where').setDescription('Where do you want to unban?').setRequired(true).addChoices(
                { name: 'All', value: 'all' },
                { name: 'Ranked pugs', value: 'ranked' },
                { name: 'Chill pugs', value: 'chill' },
            )))
    .addSubcommand(subcommand =>
        subcommand
            .setName('register')
            .setDescription('Adds a player to DB')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addIntegerOption(option => option.setName('rating').setDescription('Set rating').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('update')
            .setDescription('Updates the name and rating of a player')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addStringOption(option => option.setName('nickname').setDescription('New nickname').setRequired(true))
            .addIntegerOption(option => option.setName('rating').setDescription('Set rating').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('givecoins')
            .setDescription('Adds x amount of coins to user')
            .addMentionableOption(option => option.setName('player').setDescription('Player').setRequired(true))
            .addIntegerOption(option => option.setName('coins').setDescription('How many coins to add').setRequired(true)))

export const adminPlayerCommandHandler: CommandHandler = {
    ...adminPlayerCommand,
    handle: async (interaction: CommandInteraction) => {
        try {
            if (!interaction.isChatInputCommand()) return
            const subcommand = interaction.options.getSubcommand()
            const player = interaction.options.get('player')!.member as GuildMember

            if (subcommand === 'add') {
                await interaction.deferReply()
                await addPlayer(interaction, player)
            }

            else if (subcommand === 'remove') {
                const livePug = await getCurrentPug(pugMode)
                const nickname = await getNickname(player.id)
                if (!livePug.players.includes(player.id)) {
                    await interaction.reply({ ephemeral: true, content: `${nickname} not in the pug??` })
                    return
                }

                const newPlayers = livePug.players.filter(p => p !== player.id)
                await updatePugPlayers(newPlayers.join(','), livePug.id!)
                const leaveEmbed = await getLeaveEmbed(nickname)
                await interaction.reply({ embeds: [leaveEmbed] })
            }

            else if (subcommand === 'ban' || subcommand === 'customban') {
                const reason = interaction.options.get('reason')!.value as string
                const isBanCommand = subcommand === 'ban'
                const baseDuration: number = isBanCommand
                    ? banReasons.find(r => r.name.replace(/\s+/g, '').toLowerCase() === reason.replace(/\s+/g, '').toLowerCase())!.duration
                    : interaction.options.get('duration')!.value as number
                const duration = await getAccumulatedBanDuration(player.id, reason, baseDuration)
                const bannedTill = new Date()
                bannedTill.setHours(bannedTill.getHours() + duration)
                const endTime = bannedTill.toString()

                const whereToBan: string = isBanCommand ? 'all' : interaction.options.get('where')!.value as string
                const admin = interaction.user
                const adminName = await getNickname(admin.id)

                switch (whereToBan) {
                    case 'all':
                        player.roles.add(disc_banned_role_id).catch(console.error)
                        await insertBan({
                            discord_id: player.id,
                            reason,
                            duration,
                            end_time: endTime,
                            admin_id: admin.id,
                            admin_name: adminName
                        })
                    // Fall through to 'chill' to handle banning in both contexts
                    case 'ranked':
                        if (whereToBan === 'ranked') {
                            player.roles.add(disc_banned_role_id).catch(console.error)
                            await insertBan({
                                discord_id: player.id,
                                reason,
                                duration,
                                end_time: endTime,
                                admin_id: admin.id,
                                admin_name: adminName
                            })
                        }
                        break
                    case 'chill':
                        await sendBanMessage(`.ban discord_id:${player.id} reason:${reason} duration:${duration}`)
                        break
                }
                const banEmbed = await getBanEmbed(player, reason, duration, endTime, whereToBan)
                await interaction.reply({ embeds: [banEmbed] })
            }

            else if (subcommand === 'unban') {
                const whereToUnban = interaction.options.get('where')!.value as string
                const unbanNow = new Date().toString()
                const bansToUnban = await getAllCurrentBansForUser(player.id)
                const playerName = await getNickname(player.id)
                let unbanStatus = false

                switch (whereToUnban) {
                    case 'all':
                        await Promise.all(bansToUnban.map(ban => updateEndDate(ban.id!, unbanNow)))
                        unbanStatus = true
                    // Fall through to 'chill' to handle banning in both contexts
                    case 'ranked':
                        if (whereToUnban === 'ranked') {
                            await Promise.all(bansToUnban.map(ban => updateEndDate(ban.id!, unbanNow)))
                            unbanStatus = true
                        }
                        break
                    case 'chill':
                        await sendBanMessage(`.delban discord_id:${player.id}`)
                        unbanStatus = true
                        break
                }

                const replyMessage = unbanStatus
                    ? `${playerName} will be unbanned from ${whereToUnban} pugs shortly.`
                    : `${playerName} is not currently banned.`

                await interaction.reply({ ephemeral: true, content: replyMessage })
            }

            else if (subcommand === 'register') {
                try {
                    const mentionable = interaction.options.get('player')
                    const user = (mentionable!.member as GuildMember)?.user
                    const rating = interaction.options.get('rating')!.value as number

                    await insertPlayer({ discord_id: user.id, nickname: user.username, rating, starting_rating: rating })
                    await interaction.reply({ ephemeral: true, content: `Registered player ${user.username} with rating ${rating}` })
                } catch (err) {
                    await interaction.reply({ ephemeral: true, content: `Player already registered!` })
                }
            }

            else if (subcommand === 'update') {
                const nickname = interaction.options.get('nickname')!.value as string
                const rating = interaction.options.get('rating')!.value as number
                const updatePlayer = await setPlayer(player.id, rating, nickname)
                const playerName = await getNickname(player.id)
                if (updatePlayer.found === true) {
                    await interaction.reply({ ephemeral: true, content: `${playerName} updated to nickname: *${nickname}* & rating: *${rating}*` })
                } else {
                    await interaction.reply({
                        ephemeral: true,
                        content: `**${playerName || (updatePlayer ? updatePlayer.nickname : 'Unknown Player')}** does not exist or is not registered`
                    })
                }
            }

            else if (subcommand === 'givecoins') {
                const coins = interaction.options.get('coins')!.value as number
                const playerFound = await addCoins(player.id, coins)
                const playerName = await getNickname(player.id)
                if (playerFound.found === true) {
                    await interaction.reply({ content: `**${playerName}** given ${coins} coins!` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${playerName}** does not exist` })
                }
            }
        } catch (err) {
            console.log(err)
        }
    }
}
