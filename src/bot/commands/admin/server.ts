import { CommandInteraction, SlashCommandBuilder } from 'discord.js'

import { addServer, removeServer, setServer, setServerStatus } from '../../../db/dao/server'
import { CommandHandler } from '../../commands'

export const adminServerCommand = new SlashCommandBuilder()
    .setName('adminserver')
    .setDescription('Server commands')

adminServerCommand.addSubcommand(subcommand =>
    subcommand
        .setName('add')
        .setDescription('Adds a server')
        .addStringOption(option => option.setName('ip').setDescription('Ip WITHOUT unreal:// (example: ranked2.utctfpug.com)').setRequired(true))
        .addStringOption(option => option.setName('port').setDescription('Port (example 7777)').setRequired(true))
        .addStringOption(option => option.setName('shortname').setDescription('Descriptive short name. example: ranked2').setRequired(true))
        .addStringOption(option => option.setName('modes').setDescription('Comma separated mode ids that this server can be used for. example: 1,2').setRequired(true))
        .addIntegerOption(option => option.setName('status').setDescription('Status you want to set').setRequired(true).addChoices(
            { name: 'Enabled', value: 1 },
            { name: 'Disabled', value: 0 }
        ))
        .addStringOption(option => option.setName('country').setDescription('Country (DE, UK, NL, ...)').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('changestatus')
            .setDescription('Enables or Disables a server')
            .addStringOption(option => option.setName('shortname').setDescription('Descriptive short name. example: ranked2').setRequired(true))
            .addIntegerOption(option => option.setName('status').setDescription('Status you want to set').setRequired(true).addChoices(
                { name: 'Enabled', value: 1 },
                { name: 'Disabled', value: 0 }
            )))
    .addSubcommand(subcommand =>
        subcommand
            .setName('remove')
            .setDescription('Removes a server')
            .addStringOption(option => option.setName('shortname').setDescription('Descriptive short name. example: ranked2').setRequired(true)))
    .addSubcommand(subcommand =>
        subcommand
            .setName('update')
            .setDescription('Updates a server')
            .addStringOption(option => option.setName('shortname').setDescription('Descriptive short name. example: ranked2').setRequired(true))
            .addStringOption(option => option.setName('ip').setDescription('IP WITHOUT unreal:// (example: ranked2.utctfpug.com)').setRequired(true))
            .addStringOption(option => option.setName('port').setDescription('Port (example 7777)').setRequired(true))
            .addStringOption(option => option.setName('modes').setDescription('Comma separated mode ids that this server can be used for. example: 1,2').setRequired(true)).addIntegerOption(option => option.setName('status').setDescription('Status you want to set').setRequired(true).addChoices(
                { name: 'Enabled', value: 1 },
                { name: 'Disabled', value: 0 }
            ))
            .addStringOption(option => option.setName('country').setDescription('Country (DE, UK, NL, ...)').setRequired(true)))

export const adminServerCommandHandler: CommandHandler = {
    ...adminServerCommand,
    handle: async (interaction: CommandInteraction) => {
        try {
            if (!interaction.isChatInputCommand()) return

            if (interaction.options.getSubcommand() === 'add') {
                const serverName = interaction.options.get('shortname')!.value as string
                await addServer(serverName,
                    interaction.options.get('ip')!.value as string,
                    interaction.options.get('port')!.value as string,
                    interaction.options.get('modes')!.value as string,
                    interaction.options.get('status')!.value as number,
                    (interaction.options.get('country')!.value as string).toUpperCase()
                )
                await interaction.reply({ ephemeral: true, content: `**${serverName}** added` })
            }

            else if (interaction.options.getSubcommand() === 'changestatus') {
                const serverName = interaction.options.get('shortname')!.value as string
                const server = await setServerStatus(serverName, interaction.options.get('status')!.value as number)
                if (server.found === true) {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** status updated` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** does not exist` })
                }
            }

            else if (interaction.options.getSubcommand() === 'remove') {
                const serverName = interaction.options.get('shortname')!.value as string
                const server = await removeServer(serverName)
                if (server.found === false) {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** does not exist, check spelling and version` })
                } else if (server.deleted === false) {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** could not be removed, it has already been used! Change status instead.` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** removed` })
                }
            }

            else if (interaction.options.getSubcommand() === 'update') {
                const serverName = interaction.options.get('shortname')!.value as string
                const server = await setServer(serverName,
                    interaction.options.get('ip')!.value as string,
                    interaction.options.get('port')!.value as string,
                    interaction.options.get('modes')!.value as string,
                    interaction.options.get('status')!.value as number,
                    (interaction.options.get('country')!.value as string).toUpperCase())
                if (server.found === true) {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** updated` })
                } else {
                    await interaction.reply({ ephemeral: true, content: `**${serverName}** does not exist` })
                }
            }
        } catch (err) {
            console.log(err)
        }
    }
}
