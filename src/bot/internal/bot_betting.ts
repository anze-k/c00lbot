import fs from 'fs'
import sqlite3 from 'better-sqlite3'
import config from 'config'

import { getDrawOdds, getPlayerCoins, insertBet, removeCoinsFromPlayer } from '../../db/dao/bet'
import { getMap } from '../../db/dao/map'
import { SuggestedTeams, getPlayerByDiscordId } from '../../db/dao/player'
import { fetchAllPugsWithScores } from '../../db/dao/pug'
import { BetOdds, BetOption } from '../../pug/bet/bet'
import { PlayerMapWinrate } from '../../pug/stats/daily'
import { Winrate } from '../../pug/stats/winrate'
import { bot, sendEmbed } from '../bot'
import { getBetEmbed } from '../embeds/user/bets/bet_embed'

interface BetCalcParams {
    drawRateTrigger: number,
    drawModifier: number,
    modifier1: number,
    modifier2: number,
    modifier3: number,
    modifier4: number,

    minimumGamesForLimits: number,
    calcFormula: number,
}

const parametersz = {
    drawRateTrigger: 25,
    drawModifier: 0.14,
    modifier1: 0.08,
    modifier2: 0.13,
    modifier3: 0.18,
    modifier4: 0.23,
    minimumGamesForLimits: 3,
    calcFormula: 3
}
async function calculateBotBet(teamRed: string[], teamBlue: string[], mapId: string, money: number, parameters: BetCalcParams = parametersz, debug = false) {
    const map = await getMap(mapId)
    const mapName = map.name

    const mapWinrateJson = fs.readFileSync('playerWinrateMap.json', 'utf8')
    const mapwinrateObj = JSON.parse(mapWinrateJson)

    const mapWinratesRed = await calculateTeamRatesForMap(teamRed, mapName, mapwinrateObj, parameters.minimumGamesForLimits)
    const mapWinratesBlue = await calculateTeamRatesForMap(teamBlue, mapName, mapwinrateObj, parameters.minimumGamesForLimits)

    const playerWinrateJson = fs.readFileSync('playerWinratePeople.json', 'utf8')
    const playerwinrateObj = JSON.parse(playerWinrateJson)

    const teammateSynergyRed = await calculateTeammateSynergy(teamRed, playerwinrateObj, parameters.minimumGamesForLimits)
    const teammateSynergyBlue = await calculateTeammateSynergy(teamBlue, playerwinrateObj, parameters.minimumGamesForLimits)

    if (isNaN(mapWinratesRed.avgWin) || isNaN(mapWinratesBlue.avgWin) || isNaN(teammateSynergyRed.avgWin) || isNaN(teammateSynergyBlue.avgWin)) {   
        console.log(mapWinratesRed, mapWinratesBlue, teammateSynergyRed, teammateSynergyBlue)
    }
    const mapDrawRate = await getDrawOdds(mapId)

    let redWinrate = 0
    let blueWinrate = 0
    let redDrawrate = 0
    let blueDrawrate = 0
    let redLossrate = 0
    let blueLossrate = 0


    if (parameters?.calcFormula === 1) {
        redWinrate = (mapWinratesRed.avgWin + teammateSynergyRed.avgWin) / 2
        blueWinrate = (mapWinratesBlue.avgWin + teammateSynergyBlue.avgWin) / 2
        redDrawrate = (mapWinratesRed.avgDraw + teammateSynergyRed.avgDraw) / 2
        blueDrawrate = (mapWinratesBlue.avgDraw + teammateSynergyBlue.avgDraw) / 2
        redLossrate = (mapWinratesRed.avgLoss + teammateSynergyRed.avgLoss) / 2
        blueLossrate = (mapWinratesBlue.avgLoss + teammateSynergyBlue.avgLoss) / 2
    } else if (parameters?.calcFormula === 2) { 
        redWinrate = (mapWinratesRed.avgWin + teammateSynergyRed.avgWin)
        blueWinrate = (mapWinratesBlue.avgWin + teammateSynergyBlue.avgWin)
        redDrawrate = (mapWinratesRed.avgDraw + teammateSynergyRed.avgDraw) / 2
        blueDrawrate = (mapWinratesBlue.avgDraw + teammateSynergyBlue.avgDraw) / 2
        redLossrate = (mapWinratesRed.avgLoss + teammateSynergyRed.avgLoss)
        blueLossrate = (mapWinratesBlue.avgLoss + teammateSynergyBlue.avgLoss)
    } else if (parameters?.calcFormula === 3) { 
        redWinrate = ((mapWinratesRed.avgWin*2) + teammateSynergyRed.avgWin) / 2
        blueWinrate = ((mapWinratesBlue.avgWin*2) + teammateSynergyBlue.avgWin) / 2
        redDrawrate = ((mapWinratesRed.avgDraw*2) + teammateSynergyRed.avgDraw) / 2
        blueDrawrate = ((mapWinratesBlue.avgDraw*2) + teammateSynergyBlue.avgDraw) / 2
        redLossrate = ((mapWinratesRed.avgLoss*2) + teammateSynergyRed.avgLoss) / 2
        blueLossrate = ((mapWinratesBlue.avgLoss*2) + teammateSynergyBlue.avgLoss) / 2
    } else if (parameters?.calcFormula === 4) { 
        redWinrate = (mapWinratesRed.avgWin + (teammateSynergyRed.avgWin*2)) / 2
        blueWinrate = (mapWinratesBlue.avgWin + (teammateSynergyBlue.avgWin*2)) / 2
        redDrawrate = (mapWinratesRed.avgDraw + (teammateSynergyRed.avgDraw*2)) / 2
        blueDrawrate = (mapWinratesBlue.avgDraw + (teammateSynergyBlue.avgDraw*2)) / 2
        redLossrate = (mapWinratesRed.avgLoss + (teammateSynergyRed.avgLoss*2)) / 2
        blueLossrate = (mapWinratesBlue.avgLoss + (teammateSynergyBlue.avgLoss*2)) / 2
    }

    const redWinningAdvantage = redWinrate - blueWinrate - redLossrate + blueLossrate

    if (debug) {
        console.log(mapWinratesRed)
        console.log(mapWinratesBlue)

        console.log(teammateSynergyRed)
        console.log(teammateSynergyBlue)
        console.log('Red winrate: ' + redWinrate)
        console.log('Blue winrate: ' + blueWinrate)
        console.log('Red drawrate: ' + redDrawrate)
        console.log('Blue drawrate: ' + blueDrawrate)
        console.log('Red lossrate: ' + redLossrate)
        console.log('Blue lossrate: ' + blueLossrate)
        console.log('Red winning advantage: ' + redWinningAdvantage)
    }

    
    if (((100*mapDrawRate) + redDrawrate + blueDrawrate) > parameters.drawRateTrigger) {
        return { bet: BetOption.Draw, modifier: parameters.drawModifier }
    }

    let modifier = parseFloat((Math.abs(redWinningAdvantage) * 2).toFixed(3))
    const maxModifier = getMaxBetModifier(money, parameters)
    if (modifier > maxModifier) modifier = maxModifier

    if (redWinningAdvantage > 0) {
        return { bet: BetOption.Red, modifier }
    }

    return { bet: BetOption.Blue, modifier }
}

async function calculateTeamRatesForMap(team: string[], map: string, mapwinrateObj: Record<string, Record<string, PlayerMapWinrate>>, limit = 10) {
    const mapStats = mapwinrateObj[map]

    let avgWinrate = 0
    let avgDrawrate = 0
    let avgLossrate = 0
    let playersCount = 0

    for (const player of team) {
        const playerName = (await getPlayerByDiscordId(player)).nickname
        const playerStats = mapStats[playerName]

        if (!playerStats || playerStats.total < limit) {
            continue
        }

        avgWinrate = avgWinrate + (playerStats.win / playerStats.total)
        avgDrawrate = avgDrawrate + (playerStats.draw / playerStats.total)
        avgLossrate = avgLossrate + (playerStats.loss / playerStats.total)
        playersCount += 1
    }

    return {
        avgWin: avgWinrate ?? 0  / playersCount,
        avgDraw: avgDrawrate ?? 0 / playersCount,
        avgLoss: avgLossrate ?? 0 / playersCount,
    }
}

async function calculateTeammateSynergy(team: string[], playerwinrateObj: Record<string, Record<string, Winrate>>, limit = 10) {
    let avgWinrate = 0
    let avgDrawrate = 0
    let avgLossrate = 0

    const alreadyPaired = new Set<string>()

    for (const player of team) {
        const playerName = (await getPlayerByDiscordId(player)).nickname
        const otherTeammates = team.filter((p) => p !== player)

        for (const teammate of otherTeammates) {
            const teammateName = (await getPlayerByDiscordId(teammate)).nickname


            if (alreadyPaired.has(playerName + teammateName) || alreadyPaired.has(teammateName + playerName)) {
                continue
            }

            alreadyPaired.add(playerName + teammateName)

            if (!playerwinrateObj[playerName] || !playerwinrateObj[playerName][teammateName]) continue

            const statsForTeammate = playerwinrateObj[playerName][teammateName]

            if (!statsForTeammate || statsForTeammate.total < limit) {
                continue
            }

            avgWinrate = avgWinrate + (statsForTeammate.winRate / statsForTeammate.total)
            avgDrawrate = avgDrawrate + (statsForTeammate.drawRate / statsForTeammate.total)
            avgLossrate = avgLossrate + (statsForTeammate.lossRate / statsForTeammate.total)
        }
    }

    const normalisationModifier = (avgWinrate + avgDrawrate + avgLossrate)

    return {
        avgWin: (avgWinrate ?? 0 / normalisationModifier) ,
        avgDraw: (avgDrawrate ?? 0 / normalisationModifier),
        avgLoss:(avgLossrate ?? 0 / normalisationModifier),
    }
}

// async function calculateRivalry(team: string[], team2: string[], playerwinrateObj: Record<string, Record<string, Winrate>>) {
//     let swayedWinrate = 0

//     const alreadyPaired = new Set<string>()

//     for (const player of team) {
//         const playerName = (await getPlayerByDiscordId(player)).nickname
//         const opponents = team2.filter((p) => p !== player)

//         for (const teammate of opponents) {
//             const teammateName = (await getPlayerByDiscordId(teammate)).nickname


//             if (alreadyPaired.has(playerName + teammateName) || alreadyPaired.has(teammateName + playerName)) {
//                 continue
//             }

//             alreadyPaired.add(playerName + teammateName)
//             const statsForTeammate = playerwinrateObj[playerName][teammateName]

//             if (!statsForTeammate || statsForTeammate.total < 10) {
//                 continue
//             }

//             avgWinrate = avgWinrate + (statsForTeammate.winRate / statsForTeammate.total)
//             avgDrawrate = avgDrawrate + (statsForTeammate.drawRate / statsForTeammate.total)
//             avgLossrate = avgLossrate + (statsForTeammate.lossRate / statsForTeammate.total)
//         }
//     }

//     const normalisationModifier = (avgWinrate + avgDrawrate + avgLossrate)

//     return {
//         avgWin: (avgWinrate ?? 0 / normalisationModifier) ,
//         avgDraw: (avgDrawrate ?? 0 / normalisationModifier),
//         avgLoss:(avgLossrate ?? 0 / normalisationModifier),
//     }
// }

async function testOnePug(pugId: string): Promise<void> {
    interface PugWithScoreRow {
        id: string
        team_red: string
        team_blue: string
        score_red: number
        score_blue: number
        map_name: string
        map_id: string
    }

    const db = sqlite3(config.get('dbConfig.dbName'))


    const rows: PugWithScoreRow[] = db.prepare(`
        SELECT p.id, p.team_red, p.team_blue, s.score_red, s.score_blue, m.name AS map_name, m.id AS map_id
        FROM pugs p JOIN scores s ON p.id = s.pug_id JOIN maps m ON p.map_id = m.id
        WHERE p.id = ?
    `).all(pugId) as PugWithScoreRow[]

    const pug = rows.map(row => ({
        id: row.id,
        team_red: row.team_red.split(','),
        team_blue: row.team_blue.split(','),
        score_red: row.score_red,
        score_blue: row.score_blue,
        map_name: row.map_name,
        map_id: row.map_id
    }))[0]
        
    const bet = await calculateBotBet(pug.team_red, pug.team_blue, pug.map_id, 400)
    console.log(bet)

}

async function testAllPugs(parameters: BetCalcParams = parametersz): Promise<object> {

    interface PugWithScoreRow {
        id: string
        team_red: string
        team_blue: string
        score_red: number
        score_blue: number
        map_name: string
        map_id: string
    }

    const db = sqlite3(config.get('dbConfig.dbName'))

    // const allPugs = (await fetchAllPugsWithScores()).slice(6000)
       const rows: PugWithScoreRow[] = db.prepare(`
        SELECT p.id, p.team_red, p.team_blue, s.score_red, s.score_blue, m.name AS map_name, m.id AS map_id
        FROM pugs p JOIN scores s ON p.id = s.pug_id JOIN maps m ON p.map_id = m.id
        WHERE p.finished NOT NULL AND p.rating_change_blue IS NOT NULL AND s.status != 'REJECTED'
    `).all() as PugWithScoreRow[]

    const allPugs = rows.map(row => ({
        id: row.id,
        team_red: row.team_red.split(','),
        team_blue: row.team_blue.split(','),
        score_red: row.score_red,
        score_blue: row.score_blue,
        map_name: row.map_name,
        map_id: row.map_id
    }))

    let hits = 0
    let missess = 0
    let drawHits = 0

    let blueBets = 0
    let redBets = 0
    let drawBets = 0

    let money = 400
    const allMoneys = []
    console.log(allPugs.length)

    let m100k = false
    for (const p of allPugs) {
        // if (money < 1) {
        //     console.log('----')
        //     console.log('bankrupt')

        //     console.log('Hits: ' + hits, 'Missess: ' + missess, 'Draw hits: ' + drawHits)
        //     console.log('Blue bets: ' + blueBets, 'Red bets: ' + redBets, 'Draw bets: ' + drawBets)
        //     console.log("money: " + money)

        //     console.log(JSON.stringify(parameters))
        //     console.log('----')

        //     break
        // }
        // if (!m100k && money > 1000000) {
        //     console.log('----')

        //     console.log('bot reached 1M at bet ' + (blueBets + redBets + drawBets) )
        //     m100k = true

        //     console.log('Hits: ' + hits, 'Missess: ' + missess, 'Draw hits: ' + drawHits)
        //     console.log('Blue bets: ' + blueBets, 'Red bets: ' + redBets, 'Draw bets: ' + drawBets)
        //     console.log("money: " + money)

        //     console.log(JSON.stringify(parameters))
        //     console.log('----')


        //     break
        // }
        let odds =  <BetOdds> await db.prepare(`SELECT * from pug_odds where pug_id = ?`).get(p.id)

        if (!odds || !odds.blue || !odds.red || !odds.draw) {
            odds = { blue: 2.23, red: 2.23, draw: 7 }
        }

        // if ((blueBets + redBets + drawBets + 1) % 1000 === 0) {
        //     console.log('Hits: ' + hits, 'Missess: ' + missess, 'Draw hits: ' + drawHits)
        //     console.log('Blue bets: ' + blueBets, 'Red bets: ' + redBets, 'Draw bets: ' + drawBets)
        //     console.log("money: " + money)
        //     // break
        // }

        const bet = await calculateBotBet(p.team_red, p.team_blue, p.map_id, money, parameters)
        const betMoney = money * bet.modifier
        // console.log("money ", money)
        money = money - betMoney
        // console.log("money after betting ", money)
        // console.log("bet ", bet)
        // console.log("score ", p.score_red, p.score_blue)

        if (!bet.bet || isNaN(bet.modifier)) {
            console.log('NaN')
            console.log(p.id)
            continue
        }
        // if (parseInt(p.id) % 100 === 0) {
        //     console.log(bet)
        // }

        if (bet.bet === BetOption.Red) {
            redBets += 1
        } else if (bet.bet === BetOption.Blue) {
            blueBets += 1
        } else {
            drawBets += 1
        }

        if (bet.bet === BetOption.Draw && p.score_blue === p.score_red) {
            hits += 1
            drawHits += 1

            money = money + (betMoney * odds.draw)


            if (parseInt(p.id) % 100 === 0) {
                console.log(p.score_red, p.score_blue, p.id, p.map_id)
                console.log("HIT")
            }
        } else if (bet.bet === BetOption.Red && p.score_red > p.score_blue) {
            hits += 1

            money = money + (betMoney * odds.red)


            if (parseInt(p.id) % 100 === 0) {
                console.log(p.score_red, p.score_blue, p.id, p.map_id, bet.bet)
                console.log("HIT")
            }
        } else if (bet.bet === BetOption.Blue && p.score_blue > p.score_red) {
            hits += 1

            money = money + (betMoney * odds.blue)

            if (parseInt(p.id) % 100 === 0) {
                console.log(p.score_red, p.score_blue, p.id, p.map_id, bet.bet)
                console.log("HIT")
            }
        } else {
            missess += 1

            if (parseInt(p.id) % 100 === 0) {
                console.log(p.score_red, p.score_blue, p.id, p.map_id, bet.bet)
                console.log("MISS")
            }
        }

        // console.log("money after  ", money)
        // if (parseInt(p.id) % 300 === 0) {
        //     allMoneys.push(money * 0.000001)
        //     money = 400
        // }
    }

    // console.log("moneys: " + allMoneys.reduce((partialSum, a) => partialSum + a, 0))
    console.log('Hits: ' + hits, 'Missess: ' + missess, 'Draw hits: ' + drawHits)
    console.log('Blue bets: ' + blueBets, 'Red bets: ' + redBets, 'Draw bets: ' + drawBets)
    console.log("accuarcy: " + hits / (hits + missess))


    return { hits, missess, drawHits, blueBets, redBets, drawBets, money, accuracy: hits / (hits + missess), parameters }

}
const getMaxBetModifier = (currentMoney: number, parameters: BetCalcParams) => {
    if (currentMoney < 200) return parameters.modifier1
    if (currentMoney < 400) return parameters.modifier2
    if (currentMoney < 1000) return parameters.modifier3
    return parametersz.modifier4
}

export const placeBotBet = async (pugId: number, teams: SuggestedTeams, mapId: string) => {
    if (!bot.botName || !bot.botId) {
        console.error('Bot not initialized')
        return
    }

    const teamRedIDs = teams.red.players.map(player => player.discord_id)
    const teamBlueIDs = teams.blue.players.map(player => player.discord_id)

    const currentAmount = (await getPlayerCoins(bot.botId)).coins
    const decision = await calculateBotBet(teamRedIDs, teamBlueIDs, mapId, currentAmount)
    const { bet: betOption, modifier } = decision

    const betAmount = Math.round(currentAmount * modifier)

    await insertBet(bot.botId, pugId, betAmount, betOption)
    await removeCoinsFromPlayer(bot.botId, betAmount)

    const betEmbed = await getBetEmbed(bot.botName, betAmount, betOption, pugId)
    await sendEmbed(betEmbed)
}



const testMultiplePugs = async () => {
    for (let i = 0; i < 100; i++) {
    
        const parameters = {
            drawRateTrigger: Math.random() * (30 - 15) + 15,
            drawModifier: Math.random() * (0.5 - 0.01) + 0.1,
            modifier1: Math.random() * 0.3,
            modifier2: Math.random() * 0.3,
            modifier3: Math.random() * 0.3,
            modifier4: Math.random() * 0.3,
            minimumGamesForLimits: Math.floor(Math.random() * (30 - 1) + 1),
            calcFormula: Math.ceil(Math.random() * (4 - 0) + 0),
        }
    
        // console.log(parameters)
        const results = testAllPugs(parameters).then((res) => {
            console.log(JSON.stringify(res))
         })
        
    }
}
// testMultiplePugs()

// testAllPugs({
//     drawRateTrigger: 25,
//     drawModifier: 0.17,
//     modifier1: 0.38,
//     modifier2: 0.43,
//     modifier3: 0.48,
//     modifier4: 0.53,
//     minimumGamesForLimits: 3,
//     calcFormula: 4
//     // drawRateTrigger: 25,
//     // drawModifier: 0.125,
//     // modifier1: Math.random() * 0.3,
//     // modifier2: Math.random() * 0.3,
//     // modifier3: Math.random() * 0.3,
//     // modifier4: Math.random() * 0.3,
//     // minimumGamesForLimits: 3,
//     // calcFormula: 4,
// }).then((res) => {
//     console.log(JSON.stringify(res))
//  })