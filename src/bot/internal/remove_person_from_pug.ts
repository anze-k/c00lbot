import { getModeById } from '../../db/dao/mode'
import { checkIfPlayerRegistered } from '../../db/dao/player'
import { getCurrentPug, updatePugPlayers } from '../../db/dao/pug'
import { sendMessage } from '../bot'
import { pugMode } from '../embeds/utils/constants'

export const removeDudeFromPugIfSigned = async (userId: string, status = 'left the server') => {
    if (!await checkIfPlayerRegistered(userId)) {
        return
    }

    const livePug = await getCurrentPug(pugMode)
    const mode = await getModeById(pugMode)
    let statusMessage
    switch (status) {
        case 'offline':
            statusMessage = "went offline"
            break
        case 'idle':
            statusMessage = "became idle"
            break
        default:
            statusMessage = "left the server"
    }

    if (livePug.players.includes(userId)) {
        const newPlayers = livePug.players.filter(p => p !== userId)
        await updatePugPlayers(newPlayers.join(','), livePug.id!)
        await sendMessage(`<@${userId}> was removed from **${mode.name}** [#${livePug.id}] because they ${statusMessage}`)
    }
}
