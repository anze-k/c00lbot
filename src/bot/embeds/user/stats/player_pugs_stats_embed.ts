import {getModeById} from "../../../../db/dao/mode";
import {greenuparrow, orangebox, pugMode, reddownarrow} from "../../utils/constants";
import {getAllScoredPugsForPlayer} from "../../../../db/dao/pug";
import {getNickname} from "../../utils/get_nickname";
import {getBoldScore} from "../../utils/highlight_score";
import dayjs from "dayjs";
import {PugCountError} from "../../../../pug/stats/pugs_count";
import {EmbedBuilder} from "discord.js";
import customParseFormat from "dayjs/plugin/customParseFormat";

dayjs.extend(customParseFormat)

export class PlayerPugsInvalidSinceDateError extends PugCountError {
    constructor() {
        super('Invalid reference date.');
    }
}

export async function getPlayerPugStats(params: {
    limit?: number;
    playerId: string,
    map?: string;
    referenceDate?: string;
}) {
    const { playerId, map, referenceDate } = params;
    let { limit  } = params;
    if (!limit || limit < 1 || limit > 30) {
        limit = 10;
    }

    const fromDate = dayjs(referenceDate, 'YYYY-MM-DD', true);
    if (referenceDate && !fromDate.isValid()) {
        throw new PlayerPugsInvalidSinceDateError();
    }

    const mode = await getModeById(pugMode);
    const nickname = await getNickname(playerId);
    const pugStatsEmbed = new EmbedBuilder().setColor("Random");

    const pugs = await getAllScoredPugsForPlayer(playerId);

    const sortedPugs = [...pugs.red, ...pugs.blue]
        .filter((p) => !map || p.map_name === map)
        .filter((p) => !referenceDate || referenceDate.localeCompare(String(p.finished)) >= 0)
        .sort((a, b) => String(b.finished).localeCompare(String(a.finished)))
        .slice(0, limit);

    const playerPugsData = [];
    for (const pug of sortedPugs) {
        let result: 'win' | 'draw' | 'loss' = 'loss';
        if (pug.team_red.includes(playerId) && pug.score_red > pug.score_blue) { result = 'win' }
        if (pug.team_blue.includes(playerId) && pug.score_blue > pug.score_red) { result = 'win' }
        if (pug.score_blue === pug.score_red) { result = 'draw' }

        playerPugsData.push({
            ...pug,
            result,
            scoreLeft: (pug.team_red.includes(playerId) ? pug.score_red : pug.score_blue) ?? 0,
            scoreRight: (pug.team_blue.includes(playerId) ? pug.score_red : pug.score_blue) ?? 0,
        });
    }

    const embedNameParts = [`[${nickname}] - Last ${Math.min(limit, playerPugsData.length)}`];
    if (map) {
        embedNameParts.push(map);
    }
    if (referenceDate) {
        embedNameParts.push(`before ${fromDate.format('YYYY MMM DD')}`);
    }

    const topLabel = embedNameParts.join(' - ');

    if (playerPugsData.length > 0) {
        let embedNumber = 1
        embedNumber = Math.floor(playerPugsData.length / 5) + 1;
        if (embedNumber > 10) embedNumber = 10

        const part = Math.round(playerPugsData.length / embedNumber)

        for (let i = 0; i < embedNumber; i++) {
            const embed = {
                name: i === 0 ? `${topLabel}` : '\u200B',
                value: ''
            }

            const blockPugs = i === embedNumber - 1 ? playerPugsData.slice(i * part) : playerPugsData.slice(i * part, (i + 1) * part);
            blockPugs.forEach(p => {
                let resultEmote: string;
                switch (p.result) {
                    case 'win': {
                        resultEmote = greenuparrow;
                        break;
                    }
                    case 'draw': {
                        resultEmote = orangebox;
                        break;
                    }
                    case 'loss': {
                        resultEmote = reddownarrow;
                        break;
                    }
                }

                const finishedDate = dayjs(p.finished, 'YYYY-MM-DD hh:mm:ss');
                const format = dayjs().isSame(finishedDate, 'year') ? 'MMM DD HH:mm' : 'YYYY MMM DD HH:mm'
                const finishedFormatted = finishedDate.format(format)
                embed.value += `**[#${p.id}]** ${finishedFormatted} **${p.map_name?.replace('CTF-', '')}** ${getBoldScore(p)}${p.str_red! + p.str_blue!} ${resultEmote}\n`
            })
            pugStatsEmbed.addFields(embed)
        }
    } else {
        const embed = {
            name: `Pug stats for ${topLabel}`,
            value: `No pugs yet? :pirate_flag:`,
        }
        pugStatsEmbed.addFields(embed)
    }

    return pugStatsEmbed;
}