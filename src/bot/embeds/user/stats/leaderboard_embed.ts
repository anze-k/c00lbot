import { EmbedBuilder } from 'discord.js'

import { getAllTopPlayers, getTopPlayers } from '../../../../db/dao/player'
import { greenuparrow, orangebox, reddownarrow } from '../../utils/constants'
import { getSuffixOf } from '../../utils/number_suffix'
import { getNickname } from '../../utils/get_nickname'

export async function getLeaderboardEmbed(limit: number) {
    const players = await getTopPlayers(limit)
    const startNo = limit + 1

    let embedValue = ''
    let i = startNo

    if (players.length > 0) {
        const nicknames = await Promise.all(players.map(player =>
            getNickname(player.discord_id)
        ))

        players.forEach((p, index) => {
            let trend
            if (!p.rating_change || p.rating_change === 0) trend = orangebox
            else if (p.rating_change > 0) trend = greenuparrow
            else trend = reddownarrow

            embedValue += `${trend} ${getSuffixOf(i)} **${nicknames[index]}** - ${p.rating}\n`
            i++
        })
    } else {
        embedValue = 'There is nobody this far down yet!'
    }

    const leaderboardEmbed = new EmbedBuilder()
        .addFields(
            { name: `Leaderboard ${startNo} - ${startNo + 9}`, value: embedValue }
        )
        .setColor("Random")

    return leaderboardEmbed
}

export async function getLeaderboardEmbedNoLimit() {
    const players = await getAllTopPlayers()
    const leaderboardEmbed = new EmbedBuilder().setColor("Random")

    if (!players || players.length === 0) {
        leaderboardEmbed.addFields({ name: `Leaderboard`, value: 'Leaderboard empty' })
    } else {
        let embedNumber = 1
        if (players.length > 135) embedNumber = 5
        else if (players.length > 105) embedNumber = 4
        else if (players.length > 60) embedNumber = 3
        else if (players.length > 30) embedNumber = 2

        const part = Math.round(players.length / embedNumber)
        const nicknames = await Promise.all(players.map(player =>
            getNickname(player.discord_id)
        ))

        for (let i = 0; i < embedNumber; i++) {
            const embedValue = players.slice(i * part, (i + 1) * part)
                .map((p, index) => `${getSuffixOf(i * part + index + 1)} **${nicknames[i * part + index]}** - ${p.rating}`)
                .join('\n')

            leaderboardEmbed.addFields(
                { name: i === 0 ? `Leaderboard` : '⠀⠀⠀', value: embedValue, inline: true }
            )
        }
    }

    return leaderboardEmbed
}
