import { EmbedBuilder } from 'discord.js'

import { getPlayersByDiscordId } from '../../../../db/dao/player'
import { getAllStrongestPugs, getSeasonStrongestPugs } from '../../../../db/dao/pug'
import { blueDot, redDot, SeasonChoice } from '../../utils/constants'
import { timeAgo } from '../../utils/time_ago'
import { getNickname } from '../../utils/get_nickname'

export async function getBestPugsEmbed(season: SeasonChoice) {
    let pugs
    let name = ` `

    if (season === SeasonChoice.Current) {
        pugs = await getSeasonStrongestPugs()
        name = `This Season`
    } else {
        pugs = await getAllStrongestPugs()
        name = `All Time`
    }

    if (pugs.length === 0) {
        const bestPugsEmbed = new EmbedBuilder()
            .addFields(
                {
                    name: 'No best pugs yet!',
                    value: 'No pugs have been played yet. :desert:'
                }
            )
            .setColor('Random')
        return bestPugsEmbed
    }

    const embed = {
        name: `Strongest 3 Pugs So Far ${name}`,
        value: ''
    }

    embed.value += `\n`

    if (pugs.length > 0) {
        for (let i = 0; i < pugs.length; i++) {
            let icon = ''
            let redTeam = '*'
            let blueTeam = '*'
            let redScore = ''
            let blueScore = ''

            if (i === 0) icon = ':first_place:'
            else if (i === 1) icon = ':second_place:'
            else if (i === 2) icon = ':third_place:'

            const pug = pugs[i]
            const players = await getPlayersByDiscordId(pug.players)
            const nicknames = await Promise.all(players.map(player => getNickname(player.discord_id)))

            players.forEach((p, idx) => {
                if (pug.team_blue.includes(p.discord_id)) {
                    blueTeam += `${nicknames[idx]} `
                } else {
                    redTeam += `${nicknames[idx]} `
                }
            })

            redTeam.slice(0, -1)
            blueTeam.slice(0, -1)

            redTeam += '*'
            blueTeam += '*'

            if (pug.score_red! > pug.score_blue!) {
                redScore = `**${pug.score_red}**`
                blueScore = `${pug.score_blue}`
            }
            else if (pug.score_red! < pug.score_blue!) {
                redScore = `${pug.score_red}`
                blueScore = `**${pug.score_blue}**`
            } else {
                redScore = `${pug.score_red}`
                blueScore = `${pug.score_blue}`
            }

            embed.value += `${icon} [#${pug.id}] **${pug.str_red! + pug.str_blue!}** - ${timeAgo(pug.finished)}\n`
            embed.value += `${redDot}` + redScore + `) ${redTeam}\n${blueDot}` + blueScore + `) ${blueTeam}\n`
            embed.value += `\n`
        }
    }

    const bestPugsEmbed = new EmbedBuilder()
        .addFields(
            embed
        )
        .setColor('Random')
    return bestPugsEmbed
}
