import { EmbedBuilder } from 'discord.js'

import { getStablePlayerRatingChange, getTopPlayerRatingChange, getWorstPlayerRatingChange } from '../../../../db/dao/player'
import { greenuparrow, orangebox, reddownarrow } from '../../utils/constants'
import { getNickname } from '../../utils/get_nickname'

export async function getRatingTopEmbed() {
    const [topPlayerRating, worstPlayerRating, stablePlayerRating] = await Promise.all([
        getTopPlayerRatingChange(),
        getWorstPlayerRatingChange(),
        getStablePlayerRatingChange()
    ])

    let value = ''
    if (topPlayerRating.length === 0) {
        value += 'No pugs played yet! :open_mouth:'
    } else {
        const topGainnicknames = await Promise.all(topPlayerRating.map(t => getNickname(t.discord_id!)))
        value += '**Top 3 gainers** ' + greenuparrow + '\n'
        value += topPlayerRating.map((t, i) => `**${topGainnicknames[i]}** from ${t.starting_rating} to ${t.rating} **+${t.diff}**`).join('\n') + '\n'

        const worstLossnicknames = await Promise.all(worstPlayerRating.map(w => getNickname(w.discord_id!)))
        value += '\n**Top 3 losers** ' + reddownarrow + '\n'
        value += worstPlayerRating.map((w, i) => `**${worstLossnicknames[i]}** from ${w.starting_rating} to ${w.rating} **${w.diff}**`).join('\n') + '\n'

        if (stablePlayerRating.length > 0) {
            const stablePlayernicknames = await Promise.all(stablePlayerRating.map(s => getNickname(s.discord_id!)))
            value += '\n**Most stable players** ' + orangebox + '\n'
            value += stablePlayerRating.map((s, i) => `**${stablePlayernicknames[i]}** has not moved from ${s.rating}`).join('\n')
        }
    }

    const ratingTopEmbed = new EmbedBuilder()
        .addFields({ name: 'Top Rating Changes', value })
        .setColor('Random')

    return ratingTopEmbed
}
