import c from 'config'
import { EmbedBuilder } from 'discord.js'
import { fetchAllPugsWithScoresSinceDate } from '../../../../db/dao/pug'
import { getNickname } from '../../utils/get_nickname'

export async function getPlayerPugCountEmbed(sinceDate: string, minimumPugs = 4) {
    const pugsSinceDate = await fetchAllPugsWithScoresSinceDate(sinceDate)

    const count = new Map<string, number>()

    for (const pug of pugsSinceDate) {
        const players = pug.team_blue.concat(pug.team_red)
        for (const player of players) {
            if (!count.has(player)) {
                count.set(player, 1)
            } else {
                count.set(player, count.get(player)! + 1)
            }
        }
    }

    const sortedCount = new Map([...count.entries()].sort((a, b) => b[1] - a[1]));
    
    const playerMapsEmbed = new EmbedBuilder()
        .setColor('Random')

    let countString = ['', '', '', '', '', '', '', '', '', '']
    let embedCount = 1

    for (const key of sortedCount.keys()) {
        if (sortedCount.get(key)! < minimumPugs) continue

        const playerName = await getNickname(key)

        countString[embedCount-1] += `[${sortedCount.get(key)}] **${playerName}**\n`
        if (countString[embedCount-1].length > 900) {
            embedCount++
            console.log('yes')
        }
    }

    countString = countString.filter(s => s != '')

    console.log(countString)

    for (let i = 0; i < countString.length; i++) {
        playerMapsEmbed.addFields(
            {
                name: `[${i}] Count`,
                value: countString[i],
                inline: true,
            },
        )
    }

    if (countString.length == 0) {
        playerMapsEmbed.addFields(
            {
                name: `Empty list`,
                value: `:person_in_manual_wheelchair:`,
            },
        )
    }

    return playerMapsEmbed
}
