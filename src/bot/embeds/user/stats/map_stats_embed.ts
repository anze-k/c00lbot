import { EmbedBuilder } from 'discord.js'

import { getAllMapStatsWithCount, getAllTimeMapStatsWithCount, getSeasonMapStatsWithCount } from '../../../../db/dao/map'
import { SeasonChoice } from '../../utils/constants'
import getStringDate from '../../utils/string_date'

export async function getMapStatsEmbed(time: SeasonChoice | string): Promise<EmbedBuilder> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let maps: any
    let timeString: string
    let result: string | undefined

    switch (time) {
        case SeasonChoice.Current:
            maps = await getSeasonMapStatsWithCount()
            timeString = 'This Season'
            break
        case SeasonChoice.All:
            maps = await getAllTimeMapStatsWithCount()
            timeString = 'All Time'
            break
        default:
            // Get the expected string date and time period using the helper function
            result = getStringDate(time)
            timeString = time
            break
    }

    if (result) {
        // Get the expected string date using the helper function
        maps = await getAllMapStatsWithCount(result)
    }

    const mapStatsEmbed = new EmbedBuilder()
        .setColor('Random')

    if (maps.length > 0) {
        let countName = ``
        let caps = ``
        let diff = ``

        for (const m of maps) {
            countName += `[${m.count}] **${m.name}**\n`
            caps += `*${m.caps}*\n`
            diff += `*${m.diff}*\n`
        }

        mapStatsEmbed.addFields({
            name: `[#] Name`,
            value: countName,
            inline: true,
        }, {
            name: `Avg Caps`,
            value: caps,
            inline: true,
        }, {
            name: `Avg Cap Diff`,
            value: diff,
            inline: true,
        })

    } else {
        mapStatsEmbed.addFields(
            {
                name: `No Stats For ${timeString} Yet`,
                value: `No pugs yet?? :pleading_face:`,
            },
        )
    }
    return mapStatsEmbed
}
