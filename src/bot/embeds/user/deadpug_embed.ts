import { EmbedBuilder } from 'discord.js'

import { Player } from '../../../db/dao/player'
import { getNickname } from '../utils/get_nickname';

export async function getDeadpugEmbed(modeName: string, pugId: string, players: Player[]) {
    const playerNames = await Promise.all(players.map(player =>
        getNickname(player.discord_id)
    ))

    const deadPugEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `**${modeName}** [#${pugId}] has died! :headstone:`,
                value: `Players that voted for dead pug\n${playerNames.map(name => `**${name}**`).join(` :white_small_square: `)}`
            },
        )
        .setColor('DarkerGrey')
    return deadPugEmbed
}
