import { EmbedBuilder } from 'discord.js'

import { getModeById } from '../../../db/dao/mode'
import { pugMode } from '../utils/constants'

export async function getJoinTimeoutEmbed(pugId: string, timeoutMinutes: string) {
    const gameMode = await getModeById(pugMode)

    const joinEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `Join pug timeout`,
                value: `Can't join! Pug ${gameMode.name} [#${pugId}] is still running. Pug needs to be scored or deadpugged.`
            },
        )
        .setFooter({ text: `${timeoutMinutes} minutes until timeout expires` })
        .setColor('Random')
    return joinEmbed
}
