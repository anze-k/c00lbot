import { EmbedBuilder } from 'discord.js'
import { Server } from 'src/db/dao/server'

import { getModeById } from '../../../db/dao/mode'
import { Player } from '../../../db/dao/player'
import { blueDot, redDot } from '../utils/constants'
import { getNickname } from '../utils/get_nickname'
import { getScoreColor } from '../utils/highlight_score'

export async function getDMUserEmbed(player: string, mapName: string, players: Player[], server: Server, serverPassword: string, pugMode: number, pugId: string, team: string, time: string) {
    const gameMode = await getModeById(pugMode)
    let color = ``
    let scoreBlue = 0
    let scoreRed = 0

    if (team === 'red') {
        color = `${redDot}**TEAM RED**`
        scoreRed = 1
    } else {
        color = `${blueDot}**TEAM BLUE**`
        scoreBlue = 1
    }

    const getServer = `unreal://${server.ip}:${server.port}?password=${serverPassword}`
    const nicknamesPromises = players.map(p => getNickname(p.discord_id))
    const nicknames = await Promise.all(nicknamesPromises)

    const playerNames = nicknames.map(name => `**${name}**`).join(' :white_small_square:')

    const dmUserEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `The ranked ${gameMode.name} [#${pugId}] pug has filled`,
                value: `<@${player}> you should be ready and clicked-in on the server before __${time}__\n\nYou are on ${color}\n:white_small_square: ${playerNames}\n\nMap: **${mapName}**\nServer: ${server.shortname}\n<${getServer}>\nYou can find the tunnels for all ranked servers [**HERE**](https://discord.com/channels/445007281621565440/743586681109610586/934489024146599976)`
            }
        )
        .setColor(getScoreColor(scoreRed, scoreBlue))
    return dmUserEmbed
}
