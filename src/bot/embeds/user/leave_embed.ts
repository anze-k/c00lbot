import { EmbedBuilder } from 'discord.js'

import { getModeById } from '../../../db/dao/mode'
import { getCurrentPug } from '../../../db/dao/pug'
import { pugMode } from '../utils/constants'

export async function getLeaveEmbed(user: string) {
    const livePug = await getCurrentPug(pugMode)
    const gameMode = await getModeById(livePug.mode_id)

    const leaveEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${gameMode.name} [${livePug.players.length}/${gameMode.team_size * 2}]`,
                value: `${user} left the pug`
            },
        )
        .setColor('Random')
    return leaveEmbed
}
