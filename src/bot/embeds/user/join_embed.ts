import { EmbedBuilder } from 'discord.js'

import { getModeById } from '../../../db/dao/mode'
import { getCurrentPug } from '../../../db/dao/pug'
import { pugMode } from '../utils/constants'

export async function getJoinEmbed(user: string) {
    const livePug = await getCurrentPug(pugMode)
    const gameMode = await getModeById(livePug.mode_id)

    const joinEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${gameMode.name} [${livePug.players.length}/${gameMode.team_size * 2}]`,
                value: `${user} joined the pug`
            },
        )
        .setColor('Random')
    return joinEmbed
}
