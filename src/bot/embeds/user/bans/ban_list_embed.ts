import { EmbedBuilder } from 'discord.js'

import { getAllCurrentBans } from '../../../../db/dao/ban'
import { getNickname } from '../../utils/get_nickname'
import { timeAgo } from '../../utils/time_ago'

export async function getBanListEmbed() {
    const currentBans = await getAllCurrentBans()

    const nicknames = await Promise.all(currentBans.map(player =>
        getNickname(player.discord_id)
    ))

    let value = '';
    if (currentBans.length > 0) {
        value = currentBans.map((ban, index) =>
            `**${nicknames[index]}** banned for ${ban.duration} hours with the reason: *${ban.reason}* (${timeAgo(ban.end_time)})`
        ).join('\n');
    } else {
        value = 'Good community, **no one** is currently banned! :face_holding_back_tears:';
    }

    const banListEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `Ban List :hammer:`,
                value,
            },
        )
        .setColor('Random')
    return banListEmbed
}
