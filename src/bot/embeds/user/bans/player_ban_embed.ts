import { EmbedBuilder, GuildMember } from 'discord.js'

import { getAllBansForUser, getLastBanForUser } from '../../../../db/dao/ban'
import { timeAgo } from '../../utils/time_ago'
import { getNickname } from '../../utils/get_nickname'

export async function getPlayerBanEmbed(player: GuildMember) {
    const playerId = player.id
    const allBans = await getAllBansForUser(playerId)
    const lastBan = await getLastBanForUser(playerId)
    const playerName = await getNickname(playerId)
    let totalDuration = 0

    let value = `Wow! No bans?? Good boy. :clap:`

    if (allBans.length > 0) {
        for (let i = 0; i < allBans.length; i++) {
            totalDuration += allBans[i].duration
        }
        value = `${playerName} has been banned ${allBans.length} times and spent a total of ${totalDuration} hours banned so far!\nLast ban: ${timeAgo(lastBan.end_time)}. Duration: ${lastBan.duration}. Reason: *${lastBan.reason}*`
    }

    const playerBanEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${playerName} Bans`,
                value: value
            },
        )
        .setColor('Random')
    return playerBanEmbed
}
