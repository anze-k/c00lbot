import { EmbedBuilder } from 'discord.js'

import { ResolvedBet } from '../../../../pug/bet/bet'
import { getBetColor } from '../../utils/bet_color'
import { greenuparrow, reddownarrow } from '../../utils/constants'
import { getNickname } from '../../utils/get_nickname'

export async function getBetResultEmbed(pugId: number, bets: ResolvedBet[]): Promise<EmbedBuilder | null> {
    const betListEmbed = new EmbedBuilder().setColor("Random")

    bets.sort((a, b) => Number(b.net) - Number(a.net))

    let embedNumber = Math.floor(bets.length / 10) + 1
    if (embedNumber > 10) embedNumber = 10

    const part = Math.round(bets.length / embedNumber)

    for (let i = 0; i < embedNumber; i++) {
        const start = i * part
        const end = Math.min((i + 1) * part, bets.length)
        const currentBets = bets.slice(start, end)

        const fieldsValue = await Promise.all(currentBets.map(async bet => {
            const color = getBetColor(bet.betOn)
            const nickname = await getNickname(bet.discord_id)
            return `${bet.net > 0 ? greenuparrow : reddownarrow} **${nickname}** [${bet.balance}]${bet.net > 0 ? ' +' : ' '}${color}${bet.net} coins`
        }))

        if (fieldsValue.length === 0) return null
        
        betListEmbed.addFields({
            name: i === 0 ? `Bet result [#${pugId}]` : '\u200B',
            value: fieldsValue.join('\n')
        })
    }

    return betListEmbed
}
