import { EmbedBuilder } from 'discord.js'

export async function getMapBoostEmbed(map: string, weight: number, playerName: string) {

    const mapBoostEmbed = new EmbedBuilder()
        .addFields(
            { value: `**${playerName}** boosted *CTF-${map}* for **${weight}** weight points`, name: 'Map boost' }
        )
        .setColor('Random')
    return mapBoostEmbed
}
