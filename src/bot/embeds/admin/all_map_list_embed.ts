import { EmbedBuilder } from 'discord.js'

import { getAllMaps } from '../../../db/dao/map'

export async function getAllMapListEmbed() {
    try {
        const maps = await getAllMaps()
        const allMapListEmbed = new EmbedBuilder()
            .setColor('Random')

        if (maps.length === 0) {
            return allMapListEmbed.addFields(
                {
                    name: `No Maps`,
                    value: `Not a single map added yet?`,
                }
            )
        }

        const fields = maps.map(m => ({
            name: '**' + m.name + '**',
            weight: ' \u200B \u200B ' + m.default_weight,
            status: m.status === 'ENABLED' ? ' \u200B \u200B :white_check_mark:' : ' \u200B \u200B :x:'
        }))

        const nameValue = fields.map(f => f.name).join('\n')
        const weightValue = fields.map(f => f.weight).join('\n')
        const statusValue = fields.map(f => f.status).join('\n')

        allMapListEmbed.addFields(
            { name: 'Name', value: nameValue, inline: true },
            { name: 'Weight', value: weightValue, inline: true },
            { name: 'Status', value: statusValue, inline: true }
        )

        return allMapListEmbed
    } catch (error) {
        console.error('Failed to fetch maps:', error)
        throw error
    }
}
