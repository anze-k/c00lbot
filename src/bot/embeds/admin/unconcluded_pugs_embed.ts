import { EmbedBuilder } from 'discord.js'

import { getUnconcludedPugs } from '../../../db/dao/score'
import { formatDateTime } from '../utils/time_formatter'

export async function getUnconcludedPugsEmbed() {
    let unconcludedPugs
    try {
        unconcludedPugs = await getUnconcludedPugs()
    } catch (error) {
        console.error('Failed to fetch unconcluded PUGs:', error)
        return new EmbedBuilder()
            .addFields({
                name: `Error`,
                value: `Failed to retrieve data. Please try again later.`,
            })
            .setColor('Red')
    }

    const value = unconcludedPugs.length > 0
        ? unconcludedPugs.map(p => `[#${p.pug_id}] **${p.status}** - Created: ${formatDateTime(p.created, true)}`).join('\n')
        : `No PUGs need to be scored at this moment.`;

    const unconcludedPugsEmbed = new EmbedBuilder()
        .addFields({
            name: `Unconcluded PUGs`,
            value,
        })
        .setColor('Random')

    return unconcludedPugsEmbed
}
