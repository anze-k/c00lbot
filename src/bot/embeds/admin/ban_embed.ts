import { EmbedBuilder, GuildMember } from 'discord.js'

import { Player } from '../../../db/dao/player'
import { getNickname } from '../utils/get_nickname'
import { formatDateTime } from '../utils/time_formatter'

export async function getBanEmbed(player: Player | GuildMember, reason: string, duration: number, endTime: string, whereBanned: string) {
    const formattedEndTime = formatDateTime(endTime)
    const nickname = await getNickname(player instanceof GuildMember ? player.id : player.discord_id)

    const banEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${nickname} was banned in ${whereBanned} pugs`,
                value: `**Duration:** ${duration} hours\n**Reason:** ${reason}`
            },
        )
        .setFooter({ text: `Ban ends at: ${formattedEndTime}` })
        .setColor('Random')
    return banEmbed
}
