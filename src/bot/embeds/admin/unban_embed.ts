import { EmbedBuilder } from 'discord.js'

export async function getUnbanEmbed(player: string, reason: string, duration: number) {
    const unbanEmbed = new EmbedBuilder()
        .addFields(
            {
                name: `${player} was unbanned`,
                value: `**Duration:** ${duration} hours\n**Reason:** ${reason}`
            },
        )
        .setColor('Random')
    return unbanEmbed
}
