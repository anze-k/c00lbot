export function formatDateTime(dateTime: Date | string, hourFirst = true, timeZone = 'UTC'): string {
    const options: Intl.DateTimeFormatOptions = {
        timeZone: timeZone,
        timeZoneName: 'short',
        hour: 'numeric',
        minute: 'numeric',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour12: false,
    };

    const date = (dateTime instanceof Date) ? dateTime : new Date(dateTime);

    const formatted = date.toLocaleString('en-UK', options);

    if (hourFirst) {
        const timePart = formatted.split(', ')[1];
        const datePart = formatted.split(', ')[0];
        return `${timePart}, ${datePart}`;
    }

    return formatted;
}
