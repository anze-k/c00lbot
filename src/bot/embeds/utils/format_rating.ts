const pow = Math.pow, floor = Math.floor, abs = Math.abs, log = Math.log
const abbrev = 'km' as const;

function round(n: number, precision: number): number {
    const prec = Math.pow(10, precision)
    return Math.round(n * prec) / prec
}

export function format(n: number): string {
    const base = floor(log(abs(n)) / log(1000))
    const suffix = abbrev[Math.min(2, base - 1)]
    const baseIndex = abbrev.indexOf(suffix) + 1
    return suffix ? round(n / pow(1000, baseIndex), 2) + suffix : '' + n
}
