import { getModeById } from '../../../db/dao/mode'
import { getPlayersByDiscordId } from '../../../db/dao/player'
import { LastPug } from '../../../db/dao/pug'
import { isPugDoubleElo } from '../../../db/dao/pug_boosts'
import { blueDot, redDot } from './constants'
import { getNickname } from './get_nickname'
import { getBoldScore } from './highlight_score'

export async function getLastField(lastPug: LastPug): Promise<{ name: string, value: string }[]> {
    try {
        const gameMode = await getModeById(lastPug.mode_id)
        const teamRed = await getPlayersByDiscordId(lastPug.team_red)
        const teamBlue = await getPlayersByDiscordId(lastPug.team_blue)
        const doubleElo = await isPugDoubleElo(parseInt(lastPug.id!))
        teamRed.sort((a, b) => Number(b.rating) - Number(a.rating))
        teamBlue.sort((a, b) => Number(b.rating) - Number(a.rating))

        const teamRedPlayerNames = await Promise.all(teamRed.map(player =>
            getNickname(player.discord_id)
        ))

        const teamBluePlayerNames = await Promise.all(teamBlue.map(player =>
            getNickname(player.discord_id)
        ))

        const lastField: { name: string, value: string }[] = [{
            name: `Last ${gameMode.name} [#${lastPug.id}]`,
            value: `${redDot}**Red Team** [${lastPug.str_red}]\n:white_small_square: ${teamRedPlayerNames.join(` :white_small_square: `)}\n\n${blueDot}**Blue Team** [${lastPug.str_blue}]\n:white_small_square: ${teamBluePlayerNames.join(` :white_small_square: `)}`
        },
        {
            name: '\u200B',
            value: `:white_small_square: **Map Score:** ${getBoldScore(lastPug)} ${lastPug.finished ? '' : '<a:live60:1040966576385572915>'}${doubleElo ? '✨' : ''}\n:white_small_square: **Map:** ${lastPug.name}\n:white_small_square: **Total Strength:** ${lastPug.str_red + lastPug.str_blue}`
        }]

        return lastField
    } catch (err) {
        console.error("Error in getLastField:", err)
        throw err
    }
}
