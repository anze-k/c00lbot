import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'

dayjs.extend(relativeTime)

export const timeAgo = (input: Date | string | number) => {
    let date

    if (typeof input === 'number') {
        if (input < 0) {
            date = dayjs().add(input, 'second')
        } else {
            date = dayjs.unix(input)
        }
    } else if (typeof input === 'string') {
        date = dayjs(input)
    } else {
        date = dayjs(input)
    }

    if (!date.isValid()) {
        return 'Invalid date'
    }

    return date.fromNow()
}
