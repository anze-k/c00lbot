import { SeasonChoice } from "./constants"

const adjustDate = (days: number) => {
    const date = new Date()
    date.setDate(date.getDate() + days)
    return date.toLocaleDateString('sv-SE')
}

const getStartOfWeek = () => {
    const date = new Date()
    const dayOfWeek = date.getDay()
    date.setDate(date.getDate() - dayOfWeek + (dayOfWeek === 0 ? -6 : 1))
    return date.toLocaleDateString('sv-SE')
}

const getStartOfMonth = () => {
    const date = new Date()
    date.setDate(1)
    return date.toLocaleDateString('sv-SE')
}

const getStartOfYear = () => {
    const date = new Date()
    date.setMonth(0)
    date.setDate(1)
    return date.toLocaleDateString('sv-SE')
}

export const getStringDate = (time: SeasonChoice | string) => {
    switch (time) {
        case 'today':
            return adjustDate(0)
        case 'yesterday':
            return adjustDate(-1)
        case 'three':
            return adjustDate(-3)
        case 'week':
            return getStartOfWeek()
        case 'month':
            return getStartOfMonth()
        case 'year':
            return getStartOfYear()
        default:
            throw new Error('Invalid time parameter')
    }
}

export default getStringDate
