import config from 'config'

import { getLastBanForUserWithReason } from '../../../db/dao/ban'

interface BanType {
    name: string
    duration: number
    mode: string
}

function normalizeReason(reason: string): string {
    return reason.trim().toLowerCase();
}

function isWithinRepeatTimeframe(endTime: string, repeatBanTimeframeDays: number): boolean {
    const endDate = new Date(endTime);
    const currentDate = new Date();
    const daysDifference = Math.abs(currentDate.getTime() - endDate.getTime()) / (1000 * 3600 * 24);
    return daysDifference <= repeatBanTimeframeDays;
}

export async function getAccumulatedBanDuration(userId: string, reasonName: string, initialDuration: number): Promise<number> {
    const banTypes = config.get('ban.types') as BanType[]
    const repeatBanTimeframeDays = config.get('ban.repeatBanTimeframeDays') as number
    const progressiveDurations = config.get('ban.progressiveDurations') as number[]

    const banType = banTypes.find(type => normalizeReason(type.name) === normalizeReason(reasonName))

    const lastBan = await getLastBanForUserWithReason(userId, reasonName)

    if (!banType || !lastBan || !isWithinRepeatTimeframe(lastBan.end_time, repeatBanTimeframeDays) || normalizeReason(lastBan.reason) !== normalizeReason(reasonName)) {
        return initialDuration
    }

    switch (banType.mode) {
        case "STATIC": {
            return initialDuration
        }
        case "ADDITIVE": {
            return initialDuration + lastBan.duration
        }
        case "MULTIPLICATIVE": {
            return lastBan.duration * 2
        }
        case "PROGRESSIVE": {
            let index = progressiveDurations.indexOf(lastBan.duration)
            if (index === -1 && lastBan.duration === banType.duration) {
                index = 0
            }
            if (index < progressiveDurations.length - 1) {
                return progressiveDurations[index + 1]
            }
            return progressiveDurations[progressiveDurations.length - 1]
        }
        default:
            throw new Error(`Unknown ban mode: ${banType.mode}`)
    }
}
