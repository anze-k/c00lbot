import { Player } from '../../../db/dao/player'
import { format } from './format_rating'
import { getNickname } from './get_nickname'

export async function getPlayersInPug(players: Player[]): Promise<string> {
    let playersInPug: string
    if (!players.length) {
        playersInPug = 'No one joined yet'
    } else {
        players.sort((a, b) => Number(b.rating) - Number(a.rating))
        const nicknamesPromises = players.map(p =>
            getNickname(p.discord_id).then(nickname =>
                `:small_orange_diamond: **${nickname}** (${format(p.rating)})`
            )
        )
        const playerNames = await Promise.all(nicknamesPromises)
        playersInPug = playerNames.join(' ')
    }
    return playersInPug
}
