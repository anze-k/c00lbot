import { BetOption } from "../../../pug/bet/bet"
import { redDot, blueDot, orangebox } from "./constants"

type BetColor = string | undefined

export function getBetColor(betOn: BetOption | string): BetColor {
    switch (betOn) {
        case BetOption.Red:
            return redDot
        case BetOption.Blue:
            return blueDot
        default:
            return orangebox
    }
}
