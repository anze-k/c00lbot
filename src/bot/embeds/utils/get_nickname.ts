import { getPlayerName } from "../../../db/dao/player"

export async function getNickname(playerId: string): Promise<string> {
    try {
        const player = (await getPlayerName(playerId))
        return player.nickname
    }
    catch (error: unknown) {
        if (error instanceof Error) {
            console.error('Failed to get nickname:', error.message)
        } else {
            console.error('An unexpected error occurred')
        }
        return 'unknown player'
    }
}
