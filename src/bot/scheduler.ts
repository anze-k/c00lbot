import schedule from 'node-schedule'
import { clearPasswords } from '../db/clear_passwords'
import { generateSeasonalStats, generateStaticJsonStats } from '../pug/stats/daily'
import { generateMapWinRates } from '../pug/stats/generate_map_winrate'

export const startScheduler = async () => {
    // 6 AM
    schedule.scheduleJob('0 6 * * *', async function () {
        await generateSeasonalStats()
        await generateStaticJsonStats()

        generateMapWinRates()

        clearPasswords()
    })
}
