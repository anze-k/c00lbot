import dotenv from 'dotenv'

dotenv.config()
export const token: string = <string>process.env.CLIENT_TOKEN
export const disc_channel_id: string = <string>process.env.DISC_CHANNEL_ID
export const disc_server_id: string = <string>process.env.DISC_SERVER_ID
export const disc_bot_id: string = <string>process.env.DISC_BOT_ID
export const disc_banned_role_id: string = <string>process.env.DISC_BANNED_ROLE_ID
export const disc_bet_role_id: string = <string>process.env.DISC_BET_ROLE_ID
export const disc_ban_channel_id: string = <string>process.env.DISC_BAN_CHANNEL_ID
export const disc_log_channel_id: string = <string>process.env.DISC_LOG_CHANNEL_ID
export const disc_stats_channel_id: string = <string>process.env.DISC_STATS_CHANNEL_ID
export const webserverUsername: string = <string>process.env.WEB_USERNAME
export const webserverPassword: string = <string>process.env.WEB_PASSWORD
