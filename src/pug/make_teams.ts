import combinations from 'combinations'
import config from 'config'

import { Player, SuggestedTeams } from '../db/dao/player'
import { getMapExperts } from './get_map_experts'

export async function makeTeams(allPlayers: Player[], mapId: string, teamSize: number): Promise<SuggestedTeams> {
    const shouldUseMapExperts: boolean = config.get('features.enableMapExperts')
    const experts: string[] = shouldUseMapExperts ? await getMapExperts(mapId, allPlayers) : []

    const allCombinations = combinations(allPlayers, teamSize)
    let bestCombinations: SuggestedTeams[] = []
    let minRatingDifference = Infinity

    for (const teamRedPlayers of allCombinations) {
        if (teamRedPlayers.length !== teamSize) continue

        const teamRedRating = sumRating(teamRedPlayers)
        const teamBluePlayers = allPlayers.filter(player => !teamRedPlayers.includes(player))
        const teamBlueRating = sumRating(teamBluePlayers)
        const ratingDifference = Math.abs(teamRedRating - teamBlueRating)

        if (ratingDifference < minRatingDifference || bestCombinations.length === 0) {
            minRatingDifference = ratingDifference
            bestCombinations = [{
                red: { players: teamRedPlayers, rating: teamRedRating },
                blue: { players: teamBluePlayers, rating: teamBlueRating }
            }]
        } else if (ratingDifference === minRatingDifference) {
            bestCombinations.push({
                red: { players: teamRedPlayers, rating: teamRedRating },
                blue: { players: teamBluePlayers, rating: teamBlueRating }
            })
        }
    }

    if (shouldUseMapExperts && experts.length > 1) {
        bestCombinations = bestCombinations.map(combination => {
            const redExpertsCount = combination.red.players.filter(player => experts.includes(player.discord_id)).length
            const blueExpertsCount = combination.blue.players.filter(player => experts.includes(player.discord_id)).length
            const expertDifference = Math.abs(redExpertsCount - blueExpertsCount)

            return { ...combination, expert_diff: expertDifference }
        })

        bestCombinations.sort((a, b) => (a.expert_diff ?? 0) - (b.expert_diff ?? 0))

        const minExpertDifference = bestCombinations[0].expert_diff ?? 0

        bestCombinations = bestCombinations.filter(combination => (combination.expert_diff ?? 0) === minExpertDifference)
    }

    const selectedCombination = bestCombinations[Math.floor(Math.random() * bestCombinations.length)]
    const shouldSwap = Math.random() < 0.5

    return shouldSwap ? {
        red: selectedCombination.blue,
        blue: selectedCombination.red
    } : selectedCombination
}

function sumRating(players: Player[]): number {
    return players.reduce((sum, player) => sum + player.rating, 0)
}
