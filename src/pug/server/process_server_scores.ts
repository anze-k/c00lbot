import { ScoreStatus } from '../../bot/embeds/utils/constants'
import { getScoresThatNeedResults, Score, updateScore, updateScoreStatus } from '../../db/dao/score'
import { getServerOfPug } from '../../db/dao/server'

export const startProcessing = async () => {
    setInterval(processLiveScore, 10000)
}

export const processLiveScore = async () => {
    const pugsReadyToScore = await getScoresThatNeedResults()

    pugsReadyToScore.map(async (scoreToProcess: Score) => {
        const server = await getServerOfPug(scoreToProcess.pug_id)
        if (!server) {
            await updateScore(0, 0, scoreToProcess.id, ScoreStatus.Manual)
            return
        }

        console.log(`[${new Date().toISOString()}] [#${scoreToProcess.pug_id}] Score fetcher started on ip ${server.ip} (${server.shortname})`)

        await updateScoreStatus(scoreToProcess.id, ScoreStatus.Live)
    })
}

