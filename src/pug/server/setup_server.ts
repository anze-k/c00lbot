import axios, { AxiosError } from 'axios'
import GameDig from 'gamedig'
import { getServerByShortname } from '../../db/dao/server'
import { sendLogMessage } from '../../bot/bot'
import { webserverPassword, webserverUsername } from '../../config'
import config from 'config'


export const setupServer = async (ip: string, port: string, map: string, password: string, shortname: string, pugId: string, modeName: string) => {
    const webAddress = `http://${webserverUsername}:${webserverPassword}@${ip}:${parseInt(port) - 1}`

    let passwordSet = false

    // we have 2 loops: one for the whole server setting and one for checking if it's set correctly
    for (let tryNum = 0; tryNum < 10; tryNum++) {

        if (!passwordSet) {
            passwordSet = await setPassword(webAddress, password)
            await new Promise(resolve => setTimeout(resolve, 500))
            if (!passwordSet) continue
        }
        
        await setMap(webAddress, map)

        for (let i = 0; i < 10; i++) {
            try {
                const currentServerSettings = <any><unknown>(await GameDig.query({
                    type: 'ut',
                    host: ip,
                    port: parseInt(port),
                })).raw

                if (currentServerSettings.mapname === map) {
                    console.log(`[${new Date().toISOString()}] ${modeName} [#${pugId}] ${map} set on server ${shortname}`)
                    return
                }

                await new Promise(resolve => setTimeout(resolve, 1000))

            } catch (e) {
                // void, don't care
            }
        }
    }

    await sendLogMessage(`@here **${modeName}** [#${pugId}] :bangbang: Possibly failed to set map **${map}** on server **${shortname}** :bangbang:`)
}

export const resetPasswordForIp = async (shortname: string): Promise<void> => {
    try {
        const server = await getServerByShortname(shortname)
        const url = `http://${webserverUsername}:${webserverPassword}@${server.ip}:${parseInt(server.port) - 1}`
        const defaultPwd = config.get('server.defaultPassword')

        await axios.post(url + '/current_rules', {
            MaxPlayers: 10,
            MaxSpectators: 4,
            GamePassword: defaultPwd,
            Apply: 'Apply'
        }, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
    } catch (e) {
        console.log(e)
    }
}

const setPassword = async (url: string, password: string): Promise<boolean> => {
    try {
        const response: any = await axios.post(url + '/current_rules', {
            MaxPlayers: 10,
            MaxSpectators: 4,
            GamePassword: password,
            Apply: 'Apply'
        }, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })

        if (response.status !== 200) return false

        return true
    } catch (e) {
        const error = <AxiosError>e
        console.log(error.cause)
        return false
    }
}

const setMap = async (url: string, map: string): Promise<boolean> => {
    try {
        const response: any = await axios.post(url + '/current_game', {
            MapSelect: map + '.unr',
            SwitchMap: 'Switch',
        }, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })

        if (response.status !== 200) {
            console.log('Error setting map!')
            console.log(response.body)
            return false
        }

        return true

    } catch (e) {
        const error = <AxiosError>e
        console.log(error.cause)
        return false
    }
}
