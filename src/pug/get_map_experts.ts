import { fetchEligiblePlayers } from '../db/dao/map_stats'
import { Player } from '../db/dao/player'

const WIN_RATE_PARAMETER = 65
const GAME_PLAYED_PARAMETER = 5

async function getMapExperts(mapId: string, players: Player[]): Promise<string[]> {
    const playerIds = players.map(p => p.discord_id)

    const eligiblePlayers = await fetchEligiblePlayers(mapId, playerIds, GAME_PLAYED_PARAMETER, WIN_RATE_PARAMETER)

    return eligiblePlayers.map(p => p.player_id)
}

export { getMapExperts }
