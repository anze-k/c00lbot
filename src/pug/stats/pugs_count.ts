import { getPugsByDay } from '../../db/dao/pug'
import customParseFormat from "dayjs/plugin/customParseFormat";
import dayjs, {Dayjs} from "dayjs";
import minMax from "dayjs/plugin/minMax";
import {CanvasRenderingContext2D, createCanvas, PNGStream} from "canvas";
import {max, ScaleBand, scaleBand, ScaleLinear, scaleLinear} from "d3";
import {BaseCanvasWidth, drawUpperLabel} from "./charts/generate_chart_common";
import fs from "fs";

dayjs.extend(customParseFormat);
dayjs.extend(minMax);

export enum PugCountChartMode {
  AllTime = 'alltime',
  Days30 = '30days'
}

export class PugCountError extends Error {}

export class PugCountInvalidSinceDateError extends PugCountError {
  constructor() {
    super('Invalid since date.');
  }
}

export class PugCountNoDataError extends PugCountError {
  constructor() {
    super('0 pugs found with provided options.');
  }
}

const axisFillColor = '#fff';
const defaultFontFamily = 'Helvetica Neue, Helvetica, Arial, sans-serif';
const defaultFont = `28px ${defaultFontFamily}`;

const margin = {
  top: 120,
  left: 140,
  right: 150,
  bottom: 210
}

interface ChartConfig {
  data: { cnt: number; day: string }[];
  width: number;
  height: number;
  context: CanvasRenderingContext2D;
  player?: string,
  map?: string
}

export const generatePugsCountGraph = async (params: {
  type: PugCountChartMode,
  from?: string,
  player?: string,
  discordId?: string,
  map?: string
} = { type: PugCountChartMode.AllTime }): Promise<PNGStream> => {
  const { type, from, player, discordId, map } = params;

  const pugsByDay = await getPugsByDay({ discord_id: discordId, map });

  const canvasWidth = BaseCanvasWidth;
  const canvasHeight = 1080;


  const canvas = createCanvas(canvasWidth, canvasHeight);
  const context = canvas.getContext('2d');

  context.fillStyle = "black";
  context.fillRect(0, 0, canvasWidth, canvasHeight);

  const fromDate = dayjs(from, 'YYYY-MM-DD');

  if (type === PugCountChartMode.AllTime) {
    drawAllTimePugCount({ data: pugsByDay as any, width: canvasWidth, height: canvasHeight, context, player, map }, fromDate);
  } else {
    if (!fromDate.isValid()) {
      throw new PugCountInvalidSinceDateError();
    }

    draw30DaysPugCount({ data: pugsByDay as any, width: canvasWidth, height: canvasHeight, context, player, map }, fromDate);
  }


  // const testFileCreation: Promise<void> = new Promise((resolve, reject) => {
  //   const out = fs.createWriteStream(__dirname + '/test.png')
  //   const stream = canvas.createPNGStream()
  //   stream.pipe(out)
  //   out.on('finish', () =>  {
  //     console.log('The PNG file was created.');
  //     resolve();
  //   });
  //   out.on('error', (err) =>  reject(err));
  // })
  //
  // await testFileCreation;

  return canvas.createPNGStream();
}

function drawAllTimePugCount(cfg: ChartConfig, fromDate: Dayjs) {
  const {
    data,
    width,
    height,
    context,
    player,
    map
  } = cfg;

  const minDate = fromDate && fromDate.isValid() ? fromDate : dayjs.min(data.map((item) => dayjs(item.day, 'DD-MM-YYYY')));
  if (!minDate) {
    throw new PugCountNoDataError();
  }

  const rangeDays: string[] = [];
  const now = dayjs();
  for (let iDate = minDate?.clone(); iDate?.isBefore(now); iDate = iDate.add(1, 'day')) {
    rangeDays.push(iDate?.format('DD-MM-YYYY'));
  }

  const rangeData = rangeDays.map((dateEntry) => {
    const cnt = data.find((item) => item.day === dateEntry)?.cnt ?? 0;
    return { day: dateEntry, cnt };
  });

  if (rangeData.length < 1) {
    throw new PugCountNoDataError();
  }

  const yDomainTopBound = max(rangeData.map((item) => item.cnt)) ?? 0;

  const x = scaleBand()
      .domain(rangeData.map((item) => item.day as unknown as string))
      .range([margin.left, width - margin.right])

  const y = scaleLinear()
      .domain([yDomainTopBound, 0])
      .range([margin.top, height - margin.bottom])
      .nice();

  const axisMargin = 6;
  if (rangeData.length <= 30) {
    draw30DaysXAxis(context, x, height - margin.bottom + axisMargin, margin.left, width - margin.right);
  } else {
    drawAllTimeXAxis(context, x, height - margin.bottom + axisMargin, margin.left, width - margin.right);
  }

  drawYAxis(context, y, margin.left - axisMargin, height - margin.bottom, margin.top);
  drawChartGrid(context, y, margin.left, width - margin.right);
  drawBars(context, rangeData, x, y);

  const labelParts = fromDate.isValid() ? [] : ['All time'];
  if (player) { labelParts.push(player) }
  if (map) { labelParts.push(map) }
  labelParts.push('pug count by days');
  if (fromDate.isValid()) {
    labelParts.push(`since ${fromDate.format('DD-MM-YYYY')}`);
  }

  const upperLabel = labelParts.join(' ');
  drawUpperLabel(context, upperLabel, margin.top - 50, BaseCanvasWidth - margin.right, margin.left);
}

function draw30DaysPugCount(cfg: ChartConfig, fromDate: Dayjs) {
  const {
    data,
    width,
    height,
    context,
    player,
    map
  } = cfg;

  const DAY_COUNT = 30;
  const margin = {
    top: 120,
    left: 140,
    right: 50,
    bottom: 200
  }

  const rangeDays: string[] = [];
  for (let i = 0; i < DAY_COUNT; i++) {
    rangeDays.push(fromDate.add(i, 'day').format('DD-MM-YYYY'));
  }

  const rangeData = rangeDays.map((dateEntry) => {
    const cnt = data.find((item) => item.day === dateEntry)?.cnt ?? 0;
    return { day: dateEntry, cnt };
  });

  if (rangeData.length < 1) {
    throw new PugCountNoDataError();
  }

  const x = scaleBand()
      .domain(rangeDays)
      .range([margin.left, width - margin.right]).padding(0);

  const yDomainTopBound = max(rangeData.map((item) => item.cnt)) ?? 0;

  const y = scaleLinear()
      .domain([yDomainTopBound, 0])
      .range([margin.top, height - margin.bottom])
      .nice();

  const axisMargin = 6;
  drawYAxis(context, y, margin.left - axisMargin, height - margin.bottom, margin.top);
  draw30DaysXAxis(context, x, height - margin.bottom + axisMargin, width - margin.right, margin.left);
  drawChartGrid(context, y, margin.left, width - margin.right);
  drawBars(context, rangeData, x, y);

  const labelParts = []
  if (player) { labelParts.push(player) }
  if (map) { labelParts.push(map) }
  labelParts.push(`pug count: 30 days since ${fromDate.format('DD-MM-YYYY')}`);
  const upperLabel = labelParts.join(' ');
  drawUpperLabel(context, upperLabel, margin.top - 50, BaseCanvasWidth - margin.right, margin.left);

}

export function draw30DaysXAxis(
    context: CanvasRenderingContext2D,
    xScale: ScaleBand<string>,
    y: number,
    startX: number,
    endX: number,
) {
  const tickSize = 6;
  const xTicks = xScale.domain();

  context.strokeStyle = axisFillColor;
  context.beginPath();
  xTicks.forEach((d, i) => {
    context.moveTo(xScale(d) ?? 0, y);
    context.lineTo(xScale(d) ?? 0, y + tickSize);
  });
  context.stroke();

  context.beginPath();
  context.moveTo(startX, y + tickSize);
  context.lineTo(startX, y);
  context.lineTo(endX, y);
  context.lineTo(endX, y + tickSize);
  context.stroke();

  context.textAlign = "center";
  context.textBaseline = "top";
  context.fillStyle = axisFillColor;
  context.font = `bold ${defaultFont}`;
  xTicks.forEach(label => {
    const textWidth = context.measureText(label)?.width ?? 0;
    context.save(); // Save the current state
    const x = xScale(label) ?? 0;
    context.translate(x - Math.cos(Math.PI / 3) * textWidth / 2 + xScale.bandwidth() / 2, y + textWidth / 2); // Move the origin to the label position
    context.rotate(-Math.PI / 3); // Rotate text 90 degrees counterclockwise
    context.beginPath();
    context.fillText(label, 0, 0);
    context.beginPath();
    context.restore(); // Restore the original state
  });

  context.save();
  const labelX = (startX + endX) / 2;
  const labelY = y + 156;

  context.font = `bold ${defaultFont}`;
  context.beginPath();
  context.fillText('Date', labelX, labelY);
  context.restore(); // Restore the original state
}

export function drawAllTimeXAxis(
    context: CanvasRenderingContext2D,
    xScale: ScaleBand<string>,
    y: number,
    startX: number,
    endX: number,
) {
  const tickSize = 6;

  const tickStep = Math.ceil(xScale.domain().length / 5);
  const xTicks: string[] = [];

  xTicks.push(xScale.domain()[0]);

  for (let i = tickStep; i <= tickStep * 5; i = i + tickStep) {
    if (xScale.domain()[i]) {
      xTicks.push(xScale.domain()[i]);
    }
  }

  xTicks.push(xScale.domain()[xScale.domain().length - 1]);

  context.strokeStyle = axisFillColor;
  context.beginPath();
  xTicks.forEach((d, i) => {
    if (d === xScale.domain()[xScale.domain().length - 1]) { return; }
    context.moveTo(xScale(d) ?? 0, y);
    context.lineTo(xScale(d) ?? 0, y + tickSize);
  });
  context.stroke();

  context.beginPath();
  context.moveTo(startX, y + tickSize);
  context.lineTo(startX, y);
  context.lineTo(endX, y);
  context.lineTo(endX, y + tickSize);
  context.stroke();

  context.textAlign = "center";
  context.textBaseline = "top";
  context.fillStyle = axisFillColor;
  context.font = `bold ${defaultFont}`;

  xTicks.forEach(tick => {
    context.beginPath();
    if (tick === xTicks[xTicks.length - 1]) { return; }
    context.fillText(tick, xScale(tick) ?? 0, y + tickSize);
  });

  context.beginPath();
  const lastTick = xTicks[xTicks.length - 1];
  context.fillText(lastTick, endX, y + tickSize);

  const labelX = (startX + endX) / 2;
  const labelY = y + 36;

  context.font = `bold ${defaultFont}`;
  context.beginPath();
  context.fillText('Date', labelX, labelY);
}

export function drawYAxis(
    context: CanvasRenderingContext2D,
    yScale: ScaleLinear<number, number>,
    x: number,
    startY: number,
    endY: number
) {
  const tickPadding = 3;
  const tickSize = 6;

  const yTicks = yScale.ticks().filter((tick) => tick % 1 === 0);

  context.strokeStyle = axisFillColor;
  context.beginPath();
  yTicks.forEach(d => {
    context.moveTo(x, yScale(d));
    context.lineTo(x - tickSize, yScale(d));
  });
  context.stroke();

  context.beginPath();
  context.moveTo(x - tickSize, startY);
  context.lineTo(x, startY);
  context.lineTo(x, endY);
  context.lineTo(x - tickSize, endY);
  context.stroke();

  context.textAlign = "right";
  context.textBaseline = "middle";
  context.fillStyle = axisFillColor;
  context.font = `bold ${defaultFont}`;

  yTicks.forEach(d => {
    const textX = x - tickSize - tickPadding;
    const textY = yScale(d);
    const textXOffset = 0;

    context.fillStyle = axisFillColor;
    context.beginPath();
    context.fillText(d.toString(), textX + textXOffset, textY);
  });
}

export function drawBars(
    context: CanvasRenderingContext2D,
    data: ChartConfig['data'],
    xScale: ScaleBand<string>,
    yScale: ScaleLinear<number, number>,
) {
  context.fillStyle = axisFillColor;
  const [max, min] = yScale.domain();

  const grad= context.createLinearGradient(0,0, 0,yScale(min) - yScale(max));

  grad.addColorStop(0, "#f64f59");
  grad.addColorStop(0.5, "#c471ed");
  grad.addColorStop(1, "#12c2e9");

  for (const { cnt, day} of data) {
    // draw segment
    context.save();
    const xCoord = xScale(day) ?? 0;
    const width = xScale.bandwidth() - 1;
    const yCoord = yScale(cnt) ?? 0;
    const height = yScale(0) - yScale(cnt);

    context.rect(xCoord, yCoord, width, height);
    context.clip();

    context.fillStyle = grad;
    context.fillRect(xCoord, yScale(max), width, yScale(min) - yScale(max));
    context.restore();
    context.fillStyle = axisFillColor;
  }
}

export function drawChartGrid(
    context: CanvasRenderingContext2D,
    yScale: ScaleLinear<number, number>,
    startX: number,
    endX: number,
) {
  const yTicks = yScale.ticks();

  context.strokeStyle = 'rgba(256, 256, 256, 0.3)';
  context.beginPath();

  yTicks.forEach(d => {
      context.moveTo(endX, yScale(d) as number);
      context.lineTo(startX, yScale(d) as number);
  });

  context.stroke();
}