import * as fs from 'fs'

import { getNickname } from '../../bot/embeds/utils/get_nickname'
import { getPlayerRating } from '../../db/dao/player'
import { getAllFinishedPugsForUser } from '../../db/dao/pug'
import {CanvasRenderingContext2D, createCanvas, PNGStream} from "canvas";
import {BaseCanvasWidth} from "./charts/generate_chart_common";
import {max, min, ScaleLinear, scaleLinear} from "d3";

const axisFillColor = '#fff';
const defaultFontFamily = 'Helvetica Neue, Helvetica, Arial, sans-serif';
const defaultFont = `40px ${defaultFontFamily}`;

const colors = [
  'rgba(76, 177, 64, 1)',
  'rgba(201, 25, 11, 1)',
  'rgba(0, 174, 253, 1)',
  'rgba(240, 240, 240, 1)',
  'rgba(244, 182, 120, 1)',
  'rgba(132, 129, 221, 1)'
];

const margin = {
  top: 80,
  left: 120,
  right: 10,
  bottom: 80
}

interface PlayerRatingHistoryDataItem {
  data:  number[];
  label: string;
}

export const generateProgressImageForUser = async (discordIds: string[], limit?: number | undefined): Promise<PNGStream> => {
  if (limit && limit < 0) {
    limit = 20;
  }

  let data: PlayerRatingHistoryDataItem[] = [];

  for (const discordId of discordIds) {
    const pugsForUser = (await getAllFinishedPugsForUser(discordId, discordIds.length > 1 ? Math.min((limit !== undefined && limit) || 20, 40) : limit))
    const ratingAndName = await getPlayerRating(discordId)
    const playerRating = ratingAndName.rating
    const nickname = await getNickname(discordId)

    const ratings: number[] = [playerRating]
    let currentRating = playerRating

    for (const pug of pugsForUser) {
      let ratingChange = 0

      if (pug.team_red.includes(discordId)) ratingChange = pug.rating_change_red!
      else ratingChange = pug.rating_change_blue!

      currentRating = currentRating - ratingChange
      ratings.push(currentRating)
    }

    data.push({
      data: ratings.reverse(),
      label: nickname,
    });
  }

  data = data.sort((a, b) => Number(b.data.length) - Number(a.data.length))

  const canvasWidth = BaseCanvasWidth;
  const canvasHeight = 1080;

  const canvas = createCanvas(canvasWidth, canvasHeight);
  const context = canvas.getContext('2d');
  context.fillStyle = "black";
  context.fillRect(0, 0, canvasWidth, canvasHeight);

  drawLinesAndArea(context, canvasWidth, canvasHeight, data)

  // const testFileCreation: Promise<void> = new Promise((resolve, reject) => {
  //   const out = fs.createWriteStream(__dirname + '/test.png')
  //   const stream = canvas.createPNGStream()
  //   stream.pipe(out)
  //   out.on('finish', () =>  {
  //     console.log('The PNG file was created.');
  //     resolve();
  //   });
  //   out.on('error', (err) =>  reject(err));
  // })
  //
  // await testFileCreation;

  return canvas.createPNGStream();
}

function drawLinesAndArea(context: CanvasRenderingContext2D, canvasWidth: number, canvasHeight: number, data: PlayerRatingHistoryDataItem[]) {
  const allElos = data.reduce<number[]>((acc, currentValue) => {
    acc.push(...currentValue.data);
    return acc;
  }, []);

  const yMax = max(allElos) ?? 0;
  const yMin = min(allElos) ?? 0;
  const maxX = data[0].data.length - 1;

  const y = scaleLinear()
      .domain([yMin - 20, yMax + 20])
      .range([canvasHeight - margin.bottom , margin.top]);

  const x = scaleLinear()
      .domain([0, maxX])
      .rangeRound([margin.left + 1, canvasWidth - margin.right]);

  const axisMargin = 6;
  drawYAxis(context, y, margin.left - axisMargin, canvasHeight - margin.bottom, margin.top);
  drawChartGrid(context, y, margin.left, canvasWidth - margin.right);

  context.beginPath();
  context.lineWidth = 5;
  context.strokeStyle = 'white';
  context.stroke();

  const now = Date.now();

  for (const [idx, playerData] of Object.entries(data)) {
    drawArea(context, x, y, canvasWidth, canvasHeight, playerData, colors[(+idx + now) % colors.length]);
  }

  for (const [idx, playerData] of Object.entries(data)) {
    drawLine(context, x, y, playerData, colors[(+idx + now) % colors.length]);
  }

  const legendData = data.map((item, idx) => ({ label: item.label, color: colors[(idx + now) % colors.length]}))
  drawLegend(context, legendData, canvasWidth);
}

function drawLine(
    context: CanvasRenderingContext2D,
    x: ScaleLinear<number, number>,
    y: ScaleLinear<number, number>,
    playerData: PlayerRatingHistoryDataItem,
    baseColor = 'rgba(255,0,0,1)'
) {
  context.save();
  context.strokeStyle = baseColor;
  context.lineWidth = 5;

  // draw line
  if (playerData.data.length === 2) {
    context.beginPath();
    context.moveTo(x(0), y(playerData.data[0]));
    context.lineTo(x(1), y(playerData.data[1]));
    context.stroke();
    context.closePath();
  } else if (playerData.data.length >= 3) {
    const pts: number[] = [];
    const cp: number[] = []

    const xGap = x(1) - x(0)
    const x0 = x(0) - xGap;
    const y0 = y(playerData.data[0]);
    pts.push(x0, y0);
    for (let i = 0; i < playerData.data.length; i++) {
      const x1 = x(i);
      const y1 = y(playerData.data[i]);

      pts.push(x1, y1)
    }

    const xn = x(playerData.data.length - 1) + xGap;
    const yn = y(playerData.data[playerData.data.length - 1]);
    pts.push(xn, yn);

    for (let i=0;i < pts.length - 4; i += 2) {
      cp.push(...getLineControlPoints(pts[i],pts[i+1],pts[i+2],pts[i+3],pts[i+4],pts[i+5], 0.35))
    }

    for(let i= 2; i < pts.length - 5; i += 2) {
      context.beginPath();
      context.moveTo(pts[i],pts[i+1]);
      context.bezierCurveTo(cp[2*i-2],cp[2*i-1],cp[2*i],cp[2*i+1],pts[i+2],pts[i+3]);
      context.stroke();
    }
  }

  // draw circles if less than 101 pugs
  if (playerData.data.length <= 101) {
    for (let i = 0; i < playerData.data.length; i++) {
      const xi = x(i)
      const yi = y(playerData.data[i]);

      context.beginPath();
      context.arc(xi, yi, 7, 0, 2 * Math.PI);
      context.fillStyle = baseColor;
      context.lineWidth = 3;
      context.strokeStyle = "#000000";
      context.fill();
      context.stroke();
    }
  }

  context.beginPath();
  context.restore();
}

function drawArea(
    context: CanvasRenderingContext2D,
    x: ScaleLinear<number, number>,
    y: ScaleLinear<number, number>,
    canvasWidth: number,
    canvasHeight: number,
    playerData: PlayerRatingHistoryDataItem,
    baseColor = 'rgba(255,0,0,1)'
) {
  context.save();
  const grad= context.createLinearGradient(0, margin.top, 0, canvasHeight - margin.bottom);
  const opacityReplacerRegex = /[^,]+(?=\))/;

  grad.addColorStop(0, baseColor);
  grad.addColorStop(0.5, baseColor.replace(opacityReplacerRegex, '0.3'));
  grad.addColorStop(1, baseColor.replace(opacityReplacerRegex, '0'));

  if (playerData.data.length === 1) {
    const [y1, y2] = y.range();
    const [x1, x2 ] = x.range();
    const pointY = y(playerData.data[0]);

    context.beginPath();
    context.moveTo(x1, y1);
    context.lineTo(x2, y1);
    context.lineTo(x2, pointY);
    context.lineTo(x1, pointY);
    context.closePath();
    context.fillStyle = grad;
    context.fill();
  } else if (playerData.data.length === 2) {
    const [y1, y2] = y.range();
    const [x1, x2 ] = x.range();
    const py1 = y(playerData.data[0]);
    const py2 = y(playerData.data[1]);

    context.beginPath();
    context.moveTo(x1, y1);
    context.lineTo(x2, y1);
    context.lineTo(x2, py2);
    context.lineTo(x1, py1);
    context.closePath();
    context.fillStyle = grad;
    context.fill();
  } else if (playerData.data.length >= 3) {
    // draw area borders line
    const pts: number[] = [];
    const cp: number[] = []

    const xGap = x(1) - x(0)
    const x0 = x(0) - xGap;
    const y0 = y(playerData.data[0]);
    pts.push(x0, y0);
    for (let i = 0; i < playerData.data.length; i++) {
      const x1 = x(i);
      const y1 = y(playerData.data[i]);

      pts.push(x1, y1)
    }

    const xn = x(playerData.data.length - 1) + xGap;
    const yn = y(playerData.data[playerData.data.length - 1]);
    pts.push(xn, yn);

    for (let i=0;i < pts.length - 4; i += 2) {
      cp.push(...getLineControlPoints(pts[i],pts[i+1],pts[i+2],pts[i+3],pts[i+4],pts[i+5], 0.35))
    }

    const [y1, y2] = y.range();
    const [x1, x2 ] = x.range();

    for(let i= 2; i < pts.length - 5; i += 2) {
      context.moveTo(pts[i],pts[i+1]);
      context.bezierCurveTo(cp[2*i-2],cp[2*i-1],cp[2*i],cp[2*i+1],pts[i+2],pts[i+3]);
      context.lineTo(pts[i + 2], y1);
      context.lineTo(pts[i], y1);
      context.lineTo(pts[i],pts[i+1])
    }

    context.fillStyle = grad;
    context.fill();

    context.closePath();
  }

  context.beginPath();
  context.restore();
}

function getLineControlPoints(
    x0: number,
    y0: number,
    x1: number,
    y1: number,
    x2: number,
    y2: number,
    t: number
) {
  const d01=Math.sqrt(Math.pow(x1-x0,2)+Math.pow(y1-y0,2));
  const d12=Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
  const fa=t*d01/(d01+d12);   // scaling factor for triangle Ta
  const fb=t*d12/(d01+d12);   // ditto for Tb, simplifies to fb=t-fa
  const p1x=x1-fa*(x2-x0);    // x2-x0 is the width of triangle T
  const p1y=y1-fa*(y2-y0);    // y2-y0 is the height of T
  const p2x=x1+fb*(x2-x0);
  const p2y=y1+fb*(y2-y0);
  return [p1x,p1y,p2x,p2y];
}

function drawYAxis(
    context: CanvasRenderingContext2D,
    yScale: ScaleLinear<number, number>,
    x: number,
    startY: number,
    endY: number
) {
  const tickPadding = 3;
  const tickSize = 6;

  const yTicks = yScale.ticks().filter((tick) => tick % 1 === 0);

  context.save();
  context.strokeStyle = axisFillColor;
  context.font = defaultFont;
  context.beginPath();
  yTicks.forEach(d => {
    context.moveTo(x, yScale(d));
    context.lineTo(x - tickSize, yScale(d));
  });
  context.stroke();

  context.beginPath();
  context.moveTo(x - tickSize, startY);
  context.lineTo(x, startY);
  context.lineTo(x, endY);
  context.lineTo(x - tickSize, endY);
  context.stroke();

  context.textAlign = "right";
  context.textBaseline = "middle";
  context.fillStyle = axisFillColor;
  context.font = `bold ${defaultFont}`;

  yTicks.forEach(d => {
    const textX = x - tickSize - tickPadding;
    const textY = yScale(d);
    const textXOffset = 0;

    context.fillStyle = axisFillColor;
    context.beginPath();
    context.fillText(d.toString(), textX + textXOffset, textY);
  });

  context.restore();
}

function drawChartGrid(
    context: CanvasRenderingContext2D,
    yScale: ScaleLinear<number, number>,
    startX: number,
    endX: number,
) {
  const yTicks = yScale.ticks();

  context.save();
  context.strokeStyle = 'rgba(256, 256, 256, 0.3)';

  yTicks.forEach(d => {
    context.beginPath();
    context.moveTo(endX, yScale(d) as number);
    context.lineTo(startX, yScale(d) as number);
    context.stroke();
  });

  context.restore();
}

function drawLegend(
    context: CanvasRenderingContext2D,
    data: { label: string, color: string}[],
    canvasWidth: number
) {
  let textWidth = 0;
  context.save();
  context.textAlign = "left";
  context.textBaseline = "top";
  context.fillStyle = axisFillColor;
  context.font = `bold 40px Helvetica Neue, Helvetica, Arial, sans-serif`;

  for (const [idx, item] of Object.entries(data)) {
    textWidth += 30 // indicator width
    textWidth += context.measureText(item.label).width;
    if (+idx < data.length - 1) {
      textWidth += 60; // margin
    }
  }

  let currentX = (canvasWidth - margin.left) / 2 - textWidth / 2 + margin.left;
  let y = margin.top / 2;
  for (const { color, label } of data) {
    context.beginPath();
    context.arc(currentX, y + 18, 10, 0, 2 * Math.PI);
    context.fillStyle = color;
    context.fill();
    currentX += 30;
    context.fillText(label, currentX, y);
    currentX += context.measureText(label).width;
    currentX += 60;
  }

  context.beginPath()
  context.restore();
}

// generateProgressImageForUser(['168031471548760065', '108850415717584896', '315881722375831553', '166286180470620161'])
