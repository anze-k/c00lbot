import fs from 'fs'
import {
    BaseCanvasWidth,
    BaseRowHeightPx,
    ChartSplitHeightThreshold,
    drawChartGrid,
    drawEloGroupLegend,
    drawEmptyMessage,
    drawPlayerBars,
    drawUpperLabel,
    drawXAxis,
    drawYAxis,
    getEloCategory,
    MapWinRateObject,
    PlayerMapWinRateChartMode,
    PlayerMapWinrateSorting,
    WinLossStat
} from "./generate_chart_common";
import {createCanvas, PNGStream} from "canvas";
import {map, max, scaleBand, scaleLinear, scaleOrdinal, stack} from "d3";
import {isNumber} from "lodash";
import {getAllPlayers} from "../../../db/dao/player";

export const generateMapWinrateGraphPerMap = async (
    targetMap: string,
    mode: PlayerMapWinRateChartMode = PlayerMapWinRateChartMode.Percent,
    minMaps = 20,
    sorting: PlayerMapWinrateSorting = PlayerMapWinrateSorting.Win
): Promise<PNGStream> => {
    const playerMapwinrate = fs.readFileSync('playerWinrateMap.json', 'utf8')
    const mapwinrateObj = JSON.parse(playerMapwinrate);

    const mapPlayerStats: WinLossStat[] = getMapStats(mapwinrateObj, targetMap, mode);
    const dbPlayers = await getAllPlayers();
    const nicknameToElo = dbPlayers.reduce((result, curr) => {
        result[curr.nickname] = curr.rating;
        return result;
    }, {} as Record<string, number>)

    const players = map(
        mapPlayerStats
            .filter((stat, idx) => stat.total >= minMaps && stat.player)
            .sort((a, b) => {
                if (sorting === PlayerMapWinrateSorting.Total) {
                    return a.total - b.total
                }

                if (sorting === PlayerMapWinrateSorting.Win) {
                    return b.win - a.win;
                }

                if (sorting === PlayerMapWinrateSorting.Draw) {
                    return b.draw - a.draw;
                }

                if (sorting === PlayerMapWinrateSorting.Loss) {
                    return b.loss - a.loss;
                }

                if (sorting === PlayerMapWinrateSorting.GroupByElo) {
                    const eloA = nicknameToElo[a.player as string] ?? -Infinity;
                    const eloB = nicknameToElo[b.player as string] ?? -Infinity;
                    const categoryDiff = getEloCategory(eloB) - getEloCategory(eloA);
                    if (categoryDiff === 0) {
                        return b.win - a.win;
                    } else {
                        return categoryDiff
                    }
                }

                return 0;
            }),
        (d) => d.player!
    );

    const series = stack<WinLossStat, 'win' | 'draw' | 'loss'>()
        .keys(['win', 'draw', 'loss'])
        .value((d, key) => d[key])

    const stackedData = series(mapPlayerStats);

    const margin = {
        top: 60,
        left: 600,
        right: 40,
        bottom: 130
    }

    let canvasWidth = BaseCanvasWidth;
    let canvasHeight = Math.max(margin.top + margin.bottom + players.length * BaseRowHeightPx, 540);

    const isSplit = canvasHeight > ChartSplitHeightThreshold;
    if (isSplit) {
        canvasWidth = canvasWidth * 2;
        canvasHeight = Math.max(Math.ceil(players.length / 2) * BaseRowHeightPx + margin.top + margin.bottom, 540);
    }

    const xDomainTopBound = mode === PlayerMapWinRateChartMode.MapCount ? max(mapPlayerStats, d => d.total) ?? 100 : 100;
    const xRangeTopBound = mode === PlayerMapWinRateChartMode.MapCount ? BaseCanvasWidth - margin.right : BaseCanvasWidth - margin.right - 35;
    const x = scaleLinear()
        .domain([0, xDomainTopBound])
        .range([margin.left, xRangeTopBound])
        .nice()

    const secondColumnX = scaleLinear()
        .domain([0, xDomainTopBound])
        .range([BaseCanvasWidth + margin.left, BaseCanvasWidth + xRangeTopBound])
        .nice()


    const firstColumnYLabels = players.slice(0, Math.ceil(players.length / 2));
    const secondColumnYLabels = players.slice(Math.ceil(players.length / 2), players.length);
    const firstColumnY = scaleBand()
        .domain(firstColumnYLabels)
        .range([margin.top, canvasHeight - margin.bottom]).padding(0.2);
    const secondColumnY = scaleBand()
        .domain(secondColumnYLabels)
        .range([margin.top, canvasHeight - margin.bottom]).padding(0.2);


    const y = scaleBand()
        .domain(players)
        .range([margin.top, canvasHeight - margin.bottom]).padding(0.2);

    const color = scaleOrdinal<string, string>()
        .domain(['win', 'draw', 'loss'])
        .range(['#78b159','#f4900c','#dd2e44'])

    const canvas = createCanvas(canvasWidth, canvasHeight);
    const context = canvas.getContext('2d');

    context.fillStyle = "black";
    context.fillRect(0, 0, canvasWidth, canvasHeight);

    const axisMargin = 5;

    if (players.length > 0) {
        if (isSplit) {
            // First column
            drawXAxis(context, x, canvasHeight - margin.bottom + axisMargin, BaseCanvasWidth - margin.right, margin.left, mode);
            drawYAxis(context, stackedData, color, mode, firstColumnY, margin.left - axisMargin, canvasHeight - margin.bottom, margin.top);
            drawPlayerBars(context, nicknameToElo, x, firstColumnY, color, stackedData, mode);
            drawChartGrid(context, x, firstColumnY, margin.left, xRangeTopBound, margin.top, canvasHeight - margin.bottom);

            // Second column
            drawXAxis(context, secondColumnX, canvasHeight - margin.bottom + axisMargin, BaseCanvasWidth * 2 - margin.right, BaseCanvasWidth + margin.left, mode);
            drawYAxis(context, stackedData, color, mode, secondColumnY, BaseCanvasWidth + margin.left - axisMargin, canvasHeight - margin.bottom, margin.top);
            drawPlayerBars(context, nicknameToElo, secondColumnX, secondColumnY, color, stackedData, mode);
            drawChartGrid(context, secondColumnX, secondColumnY, margin.left, xRangeTopBound, margin.top, canvasHeight - margin.bottom);

            const upperLabel = `${targetMap}${minMaps > 0 ? ` (at least ${minMaps} maps played)` : ''}`
            drawUpperLabel(context, upperLabel, margin.top, BaseCanvasWidth - margin.right, margin.left);
            drawUpperLabel(context, upperLabel, margin.top, BaseCanvasWidth + BaseCanvasWidth - margin.right, BaseCanvasWidth + margin.left);

            drawEloGroupLegend(context, [BaseCanvasWidth - margin.right, margin.left], canvasHeight - 50);
            drawEloGroupLegend(context, [BaseCanvasWidth * 2 - margin.right, BaseCanvasWidth + margin.left], canvasHeight - 50);
        } else {
            drawXAxis(context, x, canvasHeight - margin.bottom + axisMargin, canvasWidth - margin.right, margin.left, mode);
            drawYAxis(context, stackedData, color, mode, y, margin.left - axisMargin, canvasHeight - margin.bottom, margin.top);
            drawPlayerBars(context, nicknameToElo, x, y, color, stackedData, mode);
            drawChartGrid(context, x, y, margin.left, xRangeTopBound, margin.top, canvasHeight - margin.bottom);

            const upperLabel = `${targetMap}${minMaps > 0 ? ` (at least ${minMaps} maps played)` : ''}`
            drawUpperLabel(context, upperLabel, margin.top, BaseCanvasWidth - margin.right, margin.left);
            drawEloGroupLegend(context, [canvasWidth - margin.right, margin.left], canvasHeight - 50);
        }
    } else {
        drawEmptyMessage(context, `No Data Found For The Selected Map`, canvasWidth, canvasHeight);
    }

    // const testFileCreation: Promise<void> = new Promise((resolve, reject) => {
    //     const out = fs.createWriteStream(__dirname + '/test.png')
    //     const stream = canvas.createPNGStream()
    //     stream.pipe(out)
    //     out.on('finish', () =>  {
    //         console.log('The PNG file was created.');
    //         resolve();
    //     });
    //     out.on('error', (err) =>  reject(err));
    // })
    //
    // await testFileCreation;

    return canvas.createPNGStream();
}

function getMapStats(mapwinrateObj: unknown, map: string, mode: PlayerMapWinRateChartMode) {
    if (!isMapWinRateObj(mapwinrateObj)) {
        throw new Error('Player Map Winrate stats are broken');
    }

    const mapPlayerStats = mapwinrateObj[map];
    const playerMapStats: WinLossStat[] = [];
    for (const [player, data] of Object.entries(mapPlayerStats)) {
        const playerStat = data;
        if (!playerStat || playerStat.total === 0) { continue; }

        if (map === 'CTF-Anfractuous-LE106') {
            if (mapwinrateObj['CTF-Anfractuous-LE105'][player]) {
                const disabledVersionStats = mapwinrateObj['CTF-Anfractuous-LE105'][player];
                playerStat.total += disabledVersionStats.total
                playerStat.win += disabledVersionStats.win
                playerStat.draw += disabledVersionStats.draw
                playerStat.loss += disabledVersionStats.loss
            }
        }

        if (map === 'CTF-Azcanize-LE104') {
            if (mapwinrateObj['CTF-Azcanize-LE104R'][player]) {
                const disabledVersionStats = mapwinrateObj['CTF-Azcanize-LE104R'][player];
                playerStat.total += disabledVersionStats.total
                playerStat.win += disabledVersionStats.win
                playerStat.draw += disabledVersionStats.draw
                playerStat.loss += disabledVersionStats.loss
            }

            if (mapwinrateObj['CTF-Azcanize-LE104Rfix'][player]) {
                const disabledVersionStats = mapwinrateObj['CTF-Azcanize-LE104Rfix'][player];
                playerStat.total += disabledVersionStats.total
                playerStat.win += disabledVersionStats.win
                playerStat.draw += disabledVersionStats.draw
                playerStat.loss += disabledVersionStats.loss
            }
        }

        if (map === 'CTF-Gataka-SE105') {
            if (mapwinrateObj['CTF-Gataka-SE'][player]) {
                const disabledVersionStats = mapwinrateObj['CTF-Gataka-SE'][player];
                playerStat.total += disabledVersionStats.total
                playerStat.win += disabledVersionStats.win
                playerStat.draw += disabledVersionStats.draw
                playerStat.loss += disabledVersionStats.loss
            }
        }

        if (mode === PlayerMapWinRateChartMode.MapCount) {
            playerMapStats.push({ ...playerStat, player });
        }

        if (mode === PlayerMapWinRateChartMode.Percent) {
            const win = (playerStat.win / playerStat.total * 100);
            const draw = (playerStat.draw / playerStat.total * 100);
            const loss = (playerStat.loss / playerStat.total * 100);

            playerMapStats.push({ ...playerStat, player, win, draw, loss });
        }
    }

    return playerMapStats;
}

function isMapStat(obj: unknown): obj is { win: number, loss: number, draw: number, total: number } {
    return typeof obj === 'object' && obj !== null
        && 'win' in obj && isNumber(obj.win)
        && 'loss' in obj && isNumber(obj.loss)
        && 'draw' in obj && isNumber(obj.draw)
        && 'total' in obj && isNumber(obj.total)
}

function isMapWinRateObj(obj: unknown): obj is MapWinRateObject {
    if (typeof obj !== 'object' || obj === null) { return false; }
    for (const [, playerData] of Object.entries(obj)) {
        if (typeof playerData !== 'object' || playerData === null) { return false; }
        for (const [, mapStat] of Object.entries(playerData)) {
            if (!isMapStat(mapStat)) { return false; }
        }
    }

    return true;
}
