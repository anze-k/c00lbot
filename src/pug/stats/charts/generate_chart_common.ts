import {CanvasRenderingContext2D} from "canvas";
import {ScaleBand, ScaleLinear, ScaleOrdinal, Series} from "d3";

const axisFillColor = '#fff';
const defaultFontFamily = 'Helvetica Neue, Helvetica, Arial, sans-serif';
const defaultFont = `28px ${defaultFontFamily}`;

export type WinLossStat = {
    win: number;
    loss: number;
    draw: number;
    total: number;
    map?: string;
    player?: string;
}

export type MapWinRateObject = {
    [key: string]: {
        [key: string]: { win: number, loss: number, draw: number, total: number }
    }
}

export type TeammateWinRateObject = {
    [key: string]: {
        [key: string]: {
            wins: number;
            losses: number;
            draws: number;
            total: number;
            winRate: number;
            drawRate: number;
            lossRate: number;
        }
    }
}

export enum PlayerMapWinRateChartMode {
    Percent = 'Percent',
    MapCount = 'Map Count'
}

export enum PlayerMapWinrateSorting {
    Win = 'win',
    Draw = 'draw',
    Loss = 'loss',
    Total = 'total',
    GroupByElo = 'byelogroup'
}

export const enum EloGroup {
    Alarik,
    Low,
    Average,
    High,
    VeryHigh
}

export const EloRanges: [number, number][] = [];
EloRanges[EloGroup.Alarik] = [-Infinity, 249];
EloRanges[EloGroup.Low] = [250, 499];
EloRanges[EloGroup.Average] = [500, 699];
EloRanges[EloGroup.High] = [700, 899];
EloRanges[EloGroup.VeryHigh] = [900, +Infinity];

const EloGroupColors: string[] = [];
EloGroupColors[EloGroup.Alarik] = '#FF4500';
EloGroupColors[EloGroup.Low] = '#FFA500';
EloGroupColors[EloGroup.Average] = '#FFFF00';
EloGroupColors[EloGroup.High] = '#7CFC00';
EloGroupColors[EloGroup.VeryHigh] = '#00FFFF';

const EloGroupLabels: string[] = [];
EloGroupLabels[EloGroup.Alarik] = 'Alarik';
EloGroupLabels[EloGroup.Low] = '250+';
EloGroupLabels[EloGroup.Average] = '500+';
EloGroupLabels[EloGroup.High] = '700+';
EloGroupLabels[EloGroup.VeryHigh] = '900+';

export const BaseRowHeightPx = 50;
export const BaseCanvasWidth = 1920;
export const ChartSplitHeightThreshold = 2400;

export function drawUpperLabel(
    context: CanvasRenderingContext2D,
    label: string,
    y: number,
    startX: number,
    endX: number
) {
    context.textAlign = "center";
    context.textBaseline = "top";
    context.fillStyle = axisFillColor;
    context.font = `bold 32px Helvetica Neue, Helvetica, Arial, sans-serif`;

    const labelX = (startX + endX) / 2;
    const labelY = y - 36;

    context.beginPath();
    context.fillText(label, labelX, labelY);
}

export function drawXAxis(
    context: CanvasRenderingContext2D,
    xScale: ScaleLinear<number, number>,
    y: number,
    startX: number,
    endX: number,
    mode: PlayerMapWinRateChartMode
) {
    const tickSize = 6;
    const xTicks = xScale.ticks();
    const xTickFormat = xScale.tickFormat();

    context.strokeStyle = axisFillColor;
    context.beginPath();
    xTicks.forEach(d => {
        context.moveTo(xScale(d), y);
        context.lineTo(xScale(d), y + tickSize);
    });
    context.stroke();

    context.beginPath();
    context.moveTo(startX, y + tickSize);
    context.lineTo(startX, y);
    context.lineTo(endX, y);
    context.lineTo(endX, y + tickSize);
    context.stroke();

    context.textAlign = "center";
    context.textBaseline = "top";
    context.fillStyle = axisFillColor;
    context.font = defaultFont;
    xTicks.forEach(d => {
        context.beginPath();
        context.fillText(xTickFormat(d), xScale(d), y + tickSize);
    });

    const labelX = (startX + endX) / 2;
    const labelY = y + 36;

    context.font = `bold ${defaultFont}`;
    context.beginPath();

    let bottomLabel: string;
    switch (mode) {
        case PlayerMapWinRateChartMode.Percent: bottomLabel = '%'; break;
        case PlayerMapWinRateChartMode.MapCount: bottomLabel = 'Maps Played'; break;
    }
    const offset = PlayerMapWinRateChartMode.Percent ? -18 : 0;
    context.fillText(bottomLabel, labelX + offset, labelY);
}

export function drawYAxis(
    context: CanvasRenderingContext2D,
    data:  Series<WinLossStat, "win" | "draw" | "loss">[],
    color: ScaleOrdinal<string, string>,
    mode: PlayerMapWinRateChartMode,
    yScale: ScaleBand<string>,
    x: number,
    startY: number,
    endY: number
) {
    const tickPadding = 3;
    const tickSize = 6;
    const yTicks = yScale.domain();

    context.strokeStyle = axisFillColor;
    context.beginPath();
    yTicks.forEach(d => {
        context.moveTo(x, yScale(d) as number + yScale.bandwidth() / 2);
        context.lineTo(x - tickSize, yScale(d) as number + yScale.bandwidth() / 2);
    });
    context.stroke();

    context.beginPath();
    context.moveTo(x - tickSize, startY);
    context.lineTo(x, startY);
    context.lineTo(x, endY);
    context.lineTo(x - tickSize, endY);
    context.stroke();

    context.textAlign = "right";
    context.textBaseline = "middle";
    context.fillStyle = axisFillColor;
    context.font = `bold ${defaultFont}`;

    let decimalDigits = 0;
    if (mode === PlayerMapWinRateChartMode.Percent) {
        decimalDigits = 0;
    }

    yTicks.forEach(d => {
        const textX = x - tickSize - tickPadding;
        const textY = yScale(d) as number + yScale.bandwidth() / 2;
        let textXOffset = 0;
        const yValueData = data[0].find(item => item.data.map === d || item.data.player === d);

        // Draw win / draw / loss numbers
        if (yValueData) {
            const totalText = yValueData.data.total;
            context.beginPath();
            context.fillText(`${totalText}`, textX + textXOffset, textY);

            textXOffset -= context.measureText(`${totalText}`).width;

            context.fillStyle = axisFillColor;
            context.beginPath();
            context.fillText(` / `, textX + textXOffset, textY);

            textXOffset -= context.measureText(` / `).width;

            context.fillStyle = color('loss');
            const lossText = yValueData.data.loss.toFixed(decimalDigits);
            context.beginPath();
            context.fillText(`${lossText}`, textX + textXOffset, textY);

            textXOffset -= context.measureText(`${lossText}`).width;

            context.fillStyle = axisFillColor;
            context.beginPath();
            context.fillText(` / `, textX + textXOffset, textY);

            textXOffset -= context.measureText(` / `).width;

            context.fillStyle = color('draw');
            const drawText = yValueData.data.draw.toFixed(decimalDigits);
            context.beginPath();
            context.fillText(`${drawText}`, textX + textXOffset, textY);

            textXOffset -= context.measureText(`${drawText}`).width;

            context.fillStyle = axisFillColor;
            context.beginPath();
            context.fillText(` / `, textX + textXOffset, textY);

            textXOffset -= context.measureText(` / `).width;

            context.fillStyle = color('win');
            const winText = yValueData.data.win.toFixed(decimalDigits);
            context.beginPath();
            context.fillText(`${winText}`, textX + textXOffset, textY);

            textXOffset -= context.measureText(`${winText}`).width + 5;
        }

        context.fillStyle = axisFillColor;
        context.beginPath();
        context.fillText(d, textX + textXOffset, textY);
    });
}

export function drawChartGrid(
    context: CanvasRenderingContext2D,
    xScale: ScaleLinear<number, number>,
    yScale: ScaleBand<string>,
    startX: number,
    endX: number,
    startY: number,
    endY: number
) {
    const xTicks = xScale.ticks();
    const yTicks = yScale.domain();

    context.strokeStyle = 'rgba(256, 256, 256, 0.3)';

    context.beginPath();

    xTicks.forEach(d => {
        context.moveTo(xScale(d), startY);
        context.lineTo(xScale(d), endY);
    });

    context.stroke();

    const [xLow, xHigh] = xScale.domain();
    const xMiddle = xHigh / 2;

    const oldLineWidth = context.lineWidth;
    context.lineWidth = 3;
    context.strokeStyle = '#fff';

    context.beginPath();
    context.moveTo(xScale(xMiddle), startY);
    context.lineTo(xScale(xMiddle), endY);

    context.stroke();

    context.lineWidth = oldLineWidth;

    // yTicks.forEach(d => {
    //     context.moveTo(endX, yScale(d) as number + yScale.bandwidth() / 2);
    //     context.lineTo(startX, yScale(d) as number + yScale.bandwidth() / 2);
    // });
    //
    // context.stroke();
}

export function drawBars(
    context: CanvasRenderingContext2D,
    xScale: ScaleLinear<number, number>,
    yScale: ScaleBand<string>,
    color: ScaleOrdinal<string, string>,
    data:  Series<WinLossStat, "win" | "draw" | "loss">[],
    mode: PlayerMapWinRateChartMode
)  {
    for (const series of data) {
        const key = series.key;
        const fill = color(key);

        for (const point of series) {
            const yValue = point.data.map || point.data.player;
            if (!yValue) { continue; }

            const x = xScale(point[0]);
            const width = xScale(point[1]) - xScale(point[0]);
            const y = yScale(yValue) as number;
            const height = yScale.bandwidth();
            context.fillStyle = fill;
            context.fillRect(x, y, width, height);
        }
    }
}

export function drawPlayerBars(
    context: CanvasRenderingContext2D,
    nameToElo: Record<string, number>,
    xScale: ScaleLinear<number, number>,
    yScale: ScaleBand<string>,
    color: ScaleOrdinal<string, string>,
    data:  Series<WinLossStat, "win" | "draw" | "loss">[],
    mode: PlayerMapWinRateChartMode
)  {
    for (const series of data) {
        const key = series.key;
        const fill = color(key);

        const eloBadgeWidth = 15;
        const eloBadgeMargin = 3;

        for (const point of series) {
            const yValue = point.data.map || point.data.player;
            if (!yValue || !point.data.player) { continue; }

            const eloCategory = getEloCategory(nameToElo[point.data.player]);

            const x = xScale(point[0]);
            const width = xScale(point[1]) - xScale(point[0]);
            const y = yScale(yValue) as number;
            const height = yScale.bandwidth();
            context.fillStyle = fill;
            context.beginPath();

            if (x === xScale(0)) {
                context.fillRect(x + eloBadgeWidth + eloBadgeMargin, y, width - (eloBadgeWidth + eloBadgeMargin), height);
            } else {
                context.fillRect(x, y, width, height);
            }

            context.fillStyle = EloGroupColors[eloCategory];
            context.beginPath();
            context.fillRect(xScale(0), y, eloBadgeWidth, height);
        }
    }
}

export function drawEloGroupLegend(context: CanvasRenderingContext2D, xRange: [number, number], y: number) {
    const xCenter = xRange[1] + (xRange[0] - xRange[1]) / 2;

    context.textAlign = "left";
    context.textBaseline = "top";
    context.fillStyle = axisFillColor;
    context.font = `bold 30px ${defaultFontFamily}`;

    let textXOffset = 0;

    const circleRadius = 10;
    let legendWidth = (circleRadius + 10) * 5;
    for (const label of EloGroupLabels) {
        legendWidth += context.measureText(label).width + 50;
    }

    const textX = xCenter - legendWidth / 2;
    for (const [key, label] of Object.entries(EloGroupLabels)) {
        const keyIndex = parseInt(key);
        context.beginPath();
        context.save()
        context.arc(textX + textXOffset, y - 5 + circleRadius * 2, circleRadius, 0, 2 * Math.PI);
        context.fillStyle = EloGroupColors[keyIndex];
        context.fill();
        context.restore()
        textXOffset += circleRadius + 10;

        context.beginPath();
        context.fillText(label, textX + textXOffset, y);
        textXOffset += context.measureText(label).width + 50;
    }
}

export function drawEmptyMessage(context: CanvasRenderingContext2D, message: string, width: number, height: number) {
    const textX = width / 2;
    const textY = height / 2;
    context.textAlign = "center";
    context.textBaseline = "top";
    context.fillStyle = axisFillColor;
    context.font = `bold 48px ${defaultFontFamily}`;

    context.beginPath();
    context.fillText(message, textX, textY);
}

export function getEloCategory(elo: number): number {
    return EloRanges.findIndex((range) => {
        const [low, high] = range;
        return elo >= low && elo <= high;
    })
}