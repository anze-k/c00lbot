import fs from 'fs'
import {createCanvas, PNGStream} from "canvas";
import {map, max, scaleBand, scaleLinear, scaleOrdinal, stack} from "d3";
import {
    BaseCanvasWidth,
    BaseRowHeightPx,
    drawBars,
    drawChartGrid,
    drawEmptyMessage,
    drawUpperLabel,
    drawXAxis,
    drawYAxis,
    MapWinRateObject,
    PlayerMapWinRateChartMode,
    PlayerMapWinrateSorting,
    WinLossStat
} from "./generate_chart_common";
import {isNumber} from "lodash";

const excludedMaps = [
    'CTF-Anfractuous-LE105',
    'CTF-Azcanize-LE104R',
    'CTF-Azcanize-LE104Rfix',
    'CTF-Gataka-SE'
]

export const generateMapWinrateGraphPerPlayer = async (
    player: string,
    mode: PlayerMapWinRateChartMode = PlayerMapWinRateChartMode.Percent,
    minMaps = 5,
    sorting: PlayerMapWinrateSorting = PlayerMapWinrateSorting.Win
): Promise<PNGStream> => {
    const playerMapwinrate = fs.readFileSync('playerWinrateMap.json', 'utf8')
    const mapwinrateObj = JSON.parse(playerMapwinrate);

    const playerMapStats: WinLossStat[] = getMapStats(mapwinrateObj, player, mode);

    const maps = map(
        playerMapStats
            .filter((stat) => stat.total >= minMaps && stat.map && !excludedMaps.includes(stat.map))
            .sort((a, b) => {
                if (sorting === PlayerMapWinrateSorting.Total) {
                    return a.total - b.total
                }

                if (sorting === PlayerMapWinrateSorting.Win) {
                    return b.win - a.win;
                }

                if (sorting === PlayerMapWinrateSorting.Draw) {
                    return b.draw - a.draw;
                }

                if (sorting === PlayerMapWinrateSorting.Loss) {
                    return b.loss - a.loss;
                }

                return 0;
            }),
        (d) => d.map!
    );

    const series = stack<WinLossStat, 'win' | 'draw' | 'loss'>()
        .keys(['win', 'draw', 'loss'])
        .value((d, key) => d[key])

    const stackedData = series(playerMapStats);

    const margin = {
        top: 60,
        left: 700,
        right: 40,
        bottom: 80
    }

    const canvasWidth = BaseCanvasWidth;
    const canvasHeight = Math.max(margin.top + margin.bottom + maps.length * BaseRowHeightPx, 540);

    const xDomainTopBound = mode === PlayerMapWinRateChartMode.MapCount ? max(playerMapStats, d => d.total) ?? 100 : 100;
    const xRangeTopBound = mode === PlayerMapWinRateChartMode.MapCount ? canvasWidth - margin.right : canvasWidth - margin.right - 35;
    const x = scaleLinear()
        .domain([0, xDomainTopBound])
        .range([margin.left, xRangeTopBound])
        .nice()

    const y = scaleBand()
        .domain(maps)
        .range([margin.top, canvasHeight - margin.bottom]).padding(0.2);

    const color = scaleOrdinal<string, string>()
        .domain(['win', 'draw', 'loss'])
        .range(['#78b159','#f4900c','#dd2e44'])

    const canvas = createCanvas(canvasWidth, canvasHeight);
    const context = canvas.getContext('2d');

    context.fillStyle = "black";
    context.fillRect(0, 0, canvasWidth, canvasHeight);

    const axisMargin = 5;

    if (maps.length > 0) {
        drawXAxis(context, x, canvasHeight - margin.bottom + axisMargin, canvasWidth - margin.right, margin.left, mode);
        drawYAxis(context, stackedData, color, mode, y, margin.left - axisMargin, canvasHeight - margin.bottom, margin.top);
        drawBars(context, x, y, color, stackedData, mode);
        drawChartGrid(context, x, y, margin.left, xRangeTopBound, margin.top, canvasHeight - margin.bottom);

        const upperLabel = `${player}${minMaps > 0 ? ` (at least ${minMaps} maps played)` : ''}`
        drawUpperLabel(context, upperLabel, margin.top, canvasWidth - margin.right, margin.left);
    } else {
        drawEmptyMessage(
            context,
            `No Data Found For The Selected Player`,
            canvasWidth,
            canvasHeight
        );
    }

    // const testFileCreation: Promise<void> = new Promise((resolve, reject) => {
    //     const out = fs.createWriteStream(__dirname + '/test.png')
    //     const stream = canvas.createPNGStream()
    //     stream.pipe(out)
    //     out.on('finish', () =>  {
    //         console.log('The PNG file was created.');
    //         resolve();
    //     });
    //     out.on('error', (err) =>  reject(err));
    // })
    //
    // await testFileCreation;

    return canvas.createPNGStream();
}

function getMapStats(mapwinrateObj: unknown, player: string, mode: PlayerMapWinRateChartMode) {
    if (!isMapWinRateObj(mapwinrateObj)) {
        throw new Error('Player Map Winrate stats are broken');
    }

    const playerMapStats: WinLossStat[] = [];
    for (const [map, data] of Object.entries(mapwinrateObj)) {
        const mapStat = data[player];
        if (!mapStat || mapStat.total === 0) { continue; }

        if (map === 'CTF-Anfractuous-LE106') {
            if (mapwinrateObj['CTF-Anfractuous-LE105'][player]) {
                const disabledVersionStats = mapwinrateObj['CTF-Anfractuous-LE105'][player];
                mapStat.total += disabledVersionStats.total
                mapStat.win += disabledVersionStats.win
                mapStat.draw += disabledVersionStats.draw
                mapStat.loss += disabledVersionStats.loss
            }
        }

        if (map === 'CTF-Azcanize-LE104') {
            if (mapwinrateObj['CTF-Azcanize-LE104R'][player]) {
                const disabledVersionStats = mapwinrateObj['CTF-Azcanize-LE104R'][player];
                mapStat.total += disabledVersionStats.total
                mapStat.win += disabledVersionStats.win
                mapStat.draw += disabledVersionStats.draw
                mapStat.loss += disabledVersionStats.loss
            }

            if (mapwinrateObj['CTF-Azcanize-LE104Rfix'][player]) {
                const disabledVersionStats = mapwinrateObj['CTF-Azcanize-LE104Rfix'][player];
                mapStat.total += disabledVersionStats.total
                mapStat.win += disabledVersionStats.win
                mapStat.draw += disabledVersionStats.draw
                mapStat.loss += disabledVersionStats.loss
            }
        }

        if (map === 'CTF-Gataka-SE105') {
            if (mapwinrateObj['CTF-Gataka-SE'][player]) {
                const disabledVersionStats = mapwinrateObj['CTF-Gataka-SE'][player];
                mapStat.total += disabledVersionStats.total
                mapStat.win += disabledVersionStats.win
                mapStat.draw += disabledVersionStats.draw
                mapStat.loss += disabledVersionStats.loss
            }
        }

        if (mode === PlayerMapWinRateChartMode.MapCount) {
            playerMapStats.push({ ...mapStat, map });
        }

        if (mode === PlayerMapWinRateChartMode.Percent) {
            const win = (mapStat.win / mapStat.total * 100);
            const draw = (mapStat.draw / mapStat.total * 100);
            const loss = (mapStat.loss / mapStat.total * 100);

            playerMapStats.push({ ...mapStat, map, win, draw, loss });
        }
    }

    return playerMapStats;
}

function isMapStat(obj: unknown): obj is { win: number, loss: number, draw: number, total: number } {
    return typeof obj === 'object' && obj !== null
        && 'win' in obj && isNumber(obj.win)
        && 'loss' in obj && isNumber(obj.loss)
        && 'draw' in obj && isNumber(obj.draw)
        && 'total' in obj && isNumber(obj.total)
}

function isMapWinRateObj(obj: unknown): obj is MapWinRateObject {
    if (typeof obj !== 'object' || obj === null) { return false; }
    for (const [, playerData] of Object.entries(obj)) {
        if (typeof playerData !== 'object' || playerData === null) { return false; }
        for (const [, mapStat] of Object.entries(playerData)) {
            if (!isMapStat(mapStat)) { return false; }
        }
    }

    return true;
}
