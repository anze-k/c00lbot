import sqlite3 from 'better-sqlite3'
import config from 'config'

const db = sqlite3(config.get('dbConfig.dbName'))

interface Pug {
    id: number
    map_id: number
    team_red: string
    team_blue: string
}

interface Score {
    score_red: number
    score_blue: number
}

interface MetadataRow {
    value: string
}

function getLastProcessedPugId(): number {
    const row = db.prepare('SELECT value FROM script_metadata WHERE key = ?').get('last_processed_pug_id') as MetadataRow | undefined
    return row ? parseInt(row.value, 10) : 0
}

function setLastProcessedPugId(pugId: number): void {
    db.prepare('UPDATE script_metadata SET value = ? WHERE key = ?').run(pugId.toString(), 'last_processed_pug_id')
}

function updatePlayerStats(playerId: string, mapId: number, result: 'win' | 'draw' | 'loss') {
    const exists = db.prepare('SELECT * FROM map_stats WHERE player_id = ? AND map_id = ?').get(playerId, mapId)

    if (exists) {
        const columnToUpdate = result === 'win' ? 'games_won' : result === 'draw' ? 'games_drawn' : 'games_lost'
        db.prepare(`UPDATE map_stats SET games_played = games_played + 1, ${columnToUpdate} = ${columnToUpdate} + 1 WHERE player_id = ? AND map_id = ?`).run(playerId, mapId)
    } else {
        const wins = result === 'win' ? 1 : 0
        const draws = result === 'draw' ? 1 : 0
        const losses = result === 'loss' ? 1 : 0
        db.prepare(`INSERT INTO map_stats (player_id, map_id, games_played, games_won, games_drawn, games_lost) VALUES (?, ?, 1, ?, ?, ?)`).run(playerId, mapId, wins, draws, losses)
    }
}

type MapValidityCache = { [key: number]: boolean }
const mapValidityCache: MapValidityCache = {}

function isValidMap(mapId: number): boolean {
    if (mapValidityCache[mapId] === undefined) {
        const map = db.prepare('SELECT * FROM maps WHERE id = ?').get(mapId) as { id: number } | undefined
        mapValidityCache[mapId] = !!map
    }
    return mapValidityCache[mapId]
}

export function generateMapWinRates(): void {
    const lastPugId = getLastProcessedPugId()
    const pugs = db.prepare('SELECT * FROM all_pugs WHERE rating_change_red IS NOT NULL AND id > ?').all(lastPugId) as Pug[]
    let maxProcessedPugId = lastPugId
    let skippedCounter = 0
    const skippedMapIds: number[] = []

    const transaction = db.transaction(() => {
        for (const pug of pugs) {
            if (!isValidMap(pug.map_id)) {
                console.warn(`Skipped pug with ID ${pug.id} due to non-existing map with ID ${pug.map_id}.`)
                skippedCounter++
                if (!skippedMapIds.includes(pug.map_id)) {
                    skippedMapIds.push(pug.map_id)
                }
                continue
            }

            const teamRed = pug.team_red.split(',')
            const teamBlue = pug.team_blue.split(',')
            const score = db.prepare('SELECT * FROM all_scores WHERE pug_id = ?').get(pug.id) as Score | undefined
            if (!score) continue

            const isRedWinner = score.score_red > score.score_blue
            const isDraw = score.score_red === score.score_blue

            for (const playerId of teamRed) {
                updatePlayerStats(playerId, pug.map_id, isDraw ? 'draw' : (isRedWinner ? 'win' : 'loss'))
            }
            for (const playerId of teamBlue) {
                updatePlayerStats(playerId, pug.map_id, isDraw ? 'draw' : (isRedWinner ? 'loss' : 'win'))
            }

            maxProcessedPugId = Math.max(maxProcessedPugId, pug.id)
        }
        setLastProcessedPugId(maxProcessedPugId)
    })

    transaction()

    console.log(`Finished processing. Skipped ${skippedCounter} pugs due to non-existing maps. Skipped map IDs: ${skippedMapIds.join(', ')}`)
}
