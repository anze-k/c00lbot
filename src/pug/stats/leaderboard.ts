import { sendStatsMessage } from "../../bot/bot"
import {  getLeaderboardEmbedNoLimit } from "../../bot/embeds/user/stats/leaderboard_embed"
import { getCurrentSeason, setCurrentLeaderboardMsgId } from "../../db/dao/season"

export const generateLeaderboard = async () => {
    const season = await getCurrentSeason()
    if (!season) return
    
    const leaderboardEmbed = await getLeaderboardEmbedNoLimit()

    const msgId = await sendStatsMessage({ embeds: [leaderboardEmbed]}, season.leaderboard_msg_id)

    if (msgId) await setCurrentLeaderboardMsgId(msgId, season.id)
}
