import { BetOption } from "./bet/bet"

export const whoWon = (scoreRed: number, scoreBlue: number) => {
    if (scoreRed > scoreBlue) {
        return BetOption.Red
    } else if (scoreRed < scoreBlue) {
        return BetOption.Blue
    } else return BetOption.Draw
}
